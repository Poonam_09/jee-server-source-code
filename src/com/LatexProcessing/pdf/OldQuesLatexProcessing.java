/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing.pdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author admin
 */
public class OldQuesLatexProcessing {

    OldQuesLatexProcessing() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String mboxProcessing(String str,boolean isQuestion){
        String returnString = str;
        returnString = returnString.replace("\\hspace{0.1in}", " ");//1
        returnString = returnString.replace("matrix}\\\\", "matrix}");//2
        returnString = returnString.replace("} {", "}{");//3
        returnString = returnString.replace("\\Big(", "\\left(");//4
        returnString = returnString.replace("\\Big)", "\\right)");//5
        returnString = returnString.replace("\\Big[", "\\left[");//6
        returnString = returnString.replace("\\Big]", "\\right]");//7
        returnString = returnString.replace("\\left(", " $\\left($");//8
        returnString = returnString.replace("\\left[", " $\\left[$");//9
        returnString = returnString.replace("\\right]", "$\\right]$ ");//10
        returnString = returnString.replace("\\right)", "$\\right)$ ");//11
        returnString = returnString.replace("\\left|", " $\\left|$");//12
        returnString = returnString.replace("\\right|", "$\\right|$ ");//13
        returnString = returnString.replace("â€”", "{---}");//14
        returnString = returnString.replace("\\sqrt {", "\\sqrt{");//15
        returnString = returnString.replace("{\\minus}", "\\minus");//16
        returnString = returnString.replace("\\minus", "$\\minus$ ");//17
        //returnString = returnString.replace("\\ ", " ");
        returnString = returnString.replace(" \\ ", " ");//18
        returnString = returnString.replace("Â±", "$\\pm$");//19
        if(isQuestion)
            returnString = returnString.replace("^\\prime", "'");//20
        else
            returnString = returnString.replace("^\\prime", "^$\\prime$");
        returnString = returnString.replace("\\dfrac ", "\\dfrac");//21
        
        //Actual Processing Starts
        returnString = manageMboxSyntax(returnString);//1
        returnString = removeBeginEndArray(returnString);//2
        returnString = removeMbox(returnString);//3
        returnString = manageSlashBar(returnString);//4
        returnString = manageDfracSyntax(returnString);//5
        returnString = manageOverArrowSign(returnString);//6
            returnString = returnString.replace("\\{", " $\\{$ ");
            returnString = returnString.replace("\\}", " $\\}$ ");
        returnString = manageVecCommand(returnString);//7
        returnString = addDollerNew(returnString);//8
        returnString = managePercent(returnString);//9
        returnString = refineSqrtSign(returnString);//10
        returnString = manageRaiseToSign(returnString);//11
        returnString = manageUnderscoreSignFirst(returnString);//12
        returnString = manageUnderscoreSignSecond(returnString);//13
        returnString = fManagedFrac(returnString);//14
        returnString = manageBeginEndSpaceF(returnString);//15
        returnString = checkAll(returnString);//16
        returnString = manageBeginEnd(returnString);//17 (Check 15 & 17)
        returnString = manageLeftRigtCommand(returnString); //18
        returnString = manageRaisToDoller(returnString); //19
        returnString = fromPosition(returnString); //20
        returnString = manageLimitsF(returnString); //21
        returnString = manageDfrac(returnString); //22
        returnString = removeDollerSqrt(returnString); //23
            returnString = returnString.replace("$ $", " ");
            returnString = returnString.replace("$$", " ");
            returnString = returnString.replace("  ", " ");
            returnString = returnString.replace("  ", " ");
            returnString = returnString.replace("  ", " ");
        returnString = addDoller(returnString); //24
        
        char[] carray = returnString.toCharArray();
        int clen = carray.length;
        int coI=0;
        for (int l = 0; l< clen;l++) {
            if(carray[l]=='$') {
                coI++;
            }
        }
        if(coI%2==0) {
            System.out.println("11112222 String Ressult Doller Count : Pass");
        } else {
            System.out.println("11112222 String Ressult Doller Count : Fail");
        }
        
        returnString = manageDollerType(returnString); //25
            returnString = returnString.replace("$$", " ");
            returnString = returnString.trim();
        
        return returnString;
    }
    
    
    //******ProcessingMethods******//
    
    private String manageMboxSyntax(String str) { //1
        int closing=0, opening =0;
        str=" "+str+" ";
        char [] tarray;
        char [] carray = str.toCharArray();
        int clen=carray.length; 
        tarray = new char[str.length()+100];
        for(int i=0;i<clen;i++) {
            if(carray[i]=='{')opening++;
            if(carray[i]=='}')closing++;                
        }
        System.out.println("closing "+closing+"Openig "+opening);
        str=str.trim();
        if(opening!=closing) {
            int k=opening-closing;
            for (int i = 0; i < k; i++) {
                str=str+"}";
            }
        }
        System.out.println("Output "+str);
        return str;
    }
    
    private String removeBeginEndArray(String str) { // 2
        if(str.contains("\\begin{array}{l}")) {
//            str.replace("\\begin{array}{l}", "");
//            str.replace("\\end{array}", "");
//            System.out.println("OUTPUT "+str);
            str="  "+str+"  ";
            char [] carray = str.toCharArray();
            int clen=carray.length;
            char [] tarray = new char[str.length()+100];
            int flag;
            for(int i=0,j=0;i<clen;) {
                flag=0;   
                if(carray[i]=='\\'&&carray[i+1]=='b'&&carray[i+2]=='e'&&carray[i+3]=='g'&&carray[i+4]=='i'&&carray[i+5]=='n'&&carray[i+6]=='{'&&carray[i+7]=='a'&&carray[i+8]=='r'&&carray[i+9]=='r'&&carray[i+10]=='a'&&carray[i+11]=='y'&&carray[i+12]=='}'&&carray[i+13]=='{'&&carray[i+14]=='l'&&carray[i+15]=='}') {
                    flag=1;
                    i=i+16;
                }
                if(carray[i]=='\\'&&carray[i+1]=='e'&&carray[i+2]=='n'&&carray[i+3]=='d'&&carray[i+4]=='{'&&carray[i+5]=='a'&&carray[i+6]=='r'&&carray[i+7]=='r'&&carray[i+8]=='a'&&carray[i+9]=='y'&&carray[i+10]=='}') {
                    flag=1;
                     i=i+11;
                }
                if(flag==0) {
                    tarray[j] = carray[i];i++;j++;
                }

            }
            String s1 = new String(tarray);
            s1=s1.trim();
            System.out.println("OUTPUT"+s1);
            return s1;  
        }
        return str;
    }
    
    private String removeMbox(String str) { //3
//        Quest.stringcheck.NewClass ob = new Quest.stringcheck.NewClass();
//        str=ob.Managequotes(str);Managequotes
        str = str.replace("\\mbox {", "\\mbox{");
        str = removeDoubleSlash(str);
        str="  "+str+"  ";
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int k=0,j=0;int flag1=0;
        for(int i=0;i<clen;) {
            if(carray[i]=='\\'&&carray[i+1]=='m'&&carray[i+2]=='b'&&carray[i+3]=='o'&&carray[i+4]=='x'&&carray[i+5]=='{') {
                if(flag1==1&&carray[i+6]!=' ') {
                    tarray[j]=' ';j++;
                }
                i+=6;
                k=newSingleClosingBracketPosition(str, i-6);
                for(;i<k&&i<clen;) {
                    tarray[j]=carray[i];i++;j++;
                }
                i++;
                flag1=1;
            } else {
                tarray[j]=carray[i];i++;j++;
            }
        }
        String s1=new String(tarray);
        s1=s1.trim();
//        System.out.println(s1);
        return s1;
    }
    
    private String removeDoubleSlash(String str) {
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int j=0;
        for(int i=0;i<clen;) {
            if(carray[i]=='\\'&&carray[i+1]=='\\') {
                int beginIndex=str.indexOf("\\begin{", i);
                int endIndex=str.indexOf("\\end{", i);
                
                if(endIndex==-1) {
                    i+=2;                                    
                } else {
                    if(beginIndex==-1||beginIndex>endIndex) {
                        tarray[j]=carray[i];i++;j++;
                        tarray[j]=carray[i];i++;j++;                        
                    } else {
                        i+=2;
                    }
                }
            } else {
                tarray[j]=carray[i];
                i++;
                j++;
            }
        }
        String s1=new String(tarray);
        s1=s1.trim();
        return s1;
    }
    
    private int newSingleClosingBracketPosition(String str,int position) {
        char [] carray = str.toCharArray();
        int clen=carray.length;
        int count=0;
        int i=position;
        for(;carray[i]!='{';) {
            i++;
        } 
        for(;i<clen;i++) {
            if(carray[i]=='{'&&carray[i-1]!='\\') {
                count+=1;
            }
            if(carray[i]=='}'&&carray[i-1]!='\\') {
                count-=1;
            }
            if(count==0) {
//               System.out.println("Position   "+i+"Charecctor   "+carray[i]);
                return i;
            }
        }
        return 0;
    }
    
    private String manageSlashBar(String str) { //4
        if(str.contains("\\bar")) {
            char[] carray = str.toCharArray();
            int clen = carray.length;
            char[] tarray = new char[str.length() + 100];
            for (int i = 0, j = 0; i < clen;) {
                if (carray[i] == '\\' && carray[i + 1] == 'b' && carray[i + 2] == 'a' && carray[i+3] == 'r'&& carray[i+4] == ' ') {
                       tarray[j]='$';j++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]='$';j++;
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            
            String s=new String(tarray);
            s=s.trim();
            System.out.println(s);
            return s;
        }
        return str;
    }
    
    private String manageDfracSyntax(String str) { //5
        str=str.replace("\\frac", "\\dfrac");
        System.out.println(str);
        if(str.contains("\\dfrac")) {
            str="  "+str+"  ";
            int i = 0, j = 0, clen;
            char[] tarray;
            char[] carray;
            carray = str.toCharArray();
            clen = carray.length;
            int cposition = 0;
            tarray = new char[str.length() + 100];
            for(;i<clen;) {
                if(carray[i]=='\\'&&carray[i+1]=='d'&&carray[i+2]=='f'&&carray[i+3]=='r'&&carray[i+4]=='a'&&carray[i+5]=='c') {
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    if(carray[i]=='{') {
                        tarray[j]=carray[i];j++;i++;
                        int closingfirst=closingIndexFirst(str, i);
                        for (; i < closingfirst+1; i++) {
                            if(carray[i]!='$') {
                                tarray[j] = carray[i];
                                j++;
                            }
                        }
                        if(carray[i]!='{') {
                            tarray[j]='{';j++;
                            tarray[j]=carray[i];j++;i++;
                            tarray[j]='}';j++;
                        } else {
                            tarray[j]=carray[i];j++;i++;
                            int closingfirst1=closingIndexFirst(str, i);
                            for (; i < closingfirst1+1; i++) {
                                if(carray[i]!='$')
                                {
                                    tarray[j] = carray[i];
                                    j++;
                                }
                            }
                        }
                    } else {
                        tarray[j]='{';j++;
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]='}';j++;
                        tarray[j]='{';j++;
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]='}';j++;
                    }
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            String s1 = new String(tarray);
            s1=s1.trim();
            System.out.println("OUTPUT"+s1);
            return s1;  
        }
        return str;
    }
    
    private int finalcolsing1 = 0;
    private int closingIndexFirst(String str, int i) { 
        int closing=str.indexOf("}", i);
        int opening=str.indexOf("{", i);        
        if((opening==-1)||(closing<opening)) {
            finalcolsing1=closing;           
        } else {
            closingIndexFirst(str, closing+1);            
        }
        return finalcolsing1;
    }
    
    private String manageOverArrowSign(String str) { //6
        if(str.contains("\\overrightarrow ")) {
            char[] carray = str.toCharArray();
            int clen = carray.length;
            char[] tarray = new char[str.length() + 100];
            for (int i = 0, j = 0; i < clen;) {
                if (carray[i]=='\\'&&carray[i+1]=='o'&&carray[i+2]=='v'&&carray[i+3]=='e'&&carray[i+4]=='r'&&carray[i+5]=='r'&&carray[i+6]=='i'&&carray[i+7]=='g'&&carray[i+8]=='h'&&carray[i+9]=='t'&&carray[i+10] =='a'&&carray[i+11]=='r'&&carray[i+12]=='r'&&carray[i+13]=='o'&&carray[i+14]=='w'&&carray[i+15]==' ') {
                       tarray[j]='$';j++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++; 
                       tarray[j]=carray[i];j++;i++;
                       tarray[j]=carray[i];j++;i++;                       
                       tarray[j]='$';j++;
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            String s=new String(tarray);
            s=s.trim();
            System.out.println(s);
            return s;
        }
        return str;
    }
    
    private String manageVecCommand(String str) {  //7
        if(str.contains("\\vec")) {
            char[] carray = str.toCharArray();
            int clen = carray.length;
            int j = 0;
            char[] tarray = new char[str.length() + 100];
            for (int i = 0; i < clen;) {
                if(carray[i]=='\\'&&carray[i+1]=='v'&&carray[i+2]=='e'&&carray[i+3]=='c'&&carray[i+4]==' ') {
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]=carray[i];j++;i++;
                    tarray[j]='{';j++;
                    for(;carray[i]==' ';) {
                        i++;
                    }
                    if(carray[i]=='\\') {
                        int spaceindex=str.indexOf(" ", i);
                        for(;i<spaceindex;) {
                            tarray[j]=carray[i];j++;i++;                            
                        }
                        tarray[j]='}';j++;
                    } else {                        
                        tarray[j]=carray[i];j++;i++;                    
                        tarray[j]='}';j++;
                        tarray[j]=' ';j++;
                    }
                } else {
                    tarray[j]=carray[i];i++;
                    j++;
                } 
            }
            String s1 = new String(tarray);
            s1=s1.trim();
//            System.out.println(s1);
            return s1; 
        }
        return str;
    }
    
    private String addDollerNew(String str) { //8
        File file = new File("AllSymbol.txt");
        StringBuilder line = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String text = null;
            while ((text = reader.readLine()) != null) {
                line.append(text).append(System.getProperty("line.separator"));
//                System.out.println(text);
                if(str.contains(text)) {
                    if(text != "\\le") {
                        str=str.replace(text, "$"+text+"$");      
                    }
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return str;
    }
    
    private String managePercent(String str) { //9
        if(str.contains("%")) {
            str=" "+str+" ";
            char [] tarray;
            char [] carray = str.toCharArray();
            int clen=carray.length; 
            int j=0;
            tarray = new char[str.length()+100];
            for(int i=0;i<clen ;) {
                if(carray[i]=='%'&&carray[i-1]!='\\') {
                    tarray[j]='$';j++;
                    tarray[j]='\\';j++;
                    tarray[j]=carray[i];i++;j++;
                    tarray[j]='$';j++;
                } else {
                    tarray[j]=carray[i];i++;j++;
                }
            }
            String s1=new String(tarray);
            s1=s1.trim();
            return s1;
        }
        return str;
    }
    
    private String refineSqrtSign(String str) { //10
        if (str.contains("\\sqrt")) {
            str = " " + str;
            char[] tarray;
            char[] carray = str.toCharArray();
            int clen = carray.length;
            int flag = 0;
            int j = 0;
            tarray = new char[str.length() + 100];
            for (int i = 0; i < clen; i++) {
                int flag1=0;
                if (carray[i]=='\\' && carray[i+1] == 's' && carray[i + 2] == 'q' && carray[i + 3] == 'r' && carray[i + 4] == 't' && carray[i + 5]!='{'&&carray[i + 5]!='['&&carray[i-1]!='$') {
                    flag1=1;
                    tarray[j] = '$';
                    j++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    for (; carray[i] == ' ';) {
                        i++;
                    }
                    tarray[j] = '{';j++;
                    if (carray[i] == '\\') {
                        if (carray[i + 1] == 'd' && carray[i + 2] == 'f' && carray[i + 3] == 'r' && carray[i + 4] == 'a' && carray[i + 5] == 'c') {
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = carray[i]; j++; i++;
                            int closing = 1, k = 0, l = 0;
                            for (k = i; closing != 0; k++) {
                                if (carray[k] == '{') {
                                    closing++;
                                }
                                if (carray[k] == '}') {
                                    closing--;
                                }
                            }
                            int closing1=1;
                            k++;
                            for (l = k; closing1 != 0; l++) {
                                if (carray[l] == '{') {
                                    closing1++;
                                }
                                if (carray[l] == '}') {
                                    closing1--;
                                }
                            }
                            for (; i < l; ) {
                                tarray[j] = carray[i]; j++; i++;
                            }
                            i--;
                            tarray[j] = '}'; j++;
                            tarray[j] = '$'; j++;
                        } else {
                            tarray[j] = '{';
                            j++;
                            int spqceindex = str.indexOf(" ", i);
                            for (; i < spqceindex; i++) {
                                tarray[j] = carray[i]; j++;
                            }
                            i--;
                            tarray[j] = '}'; j++;
                            tarray[j] = '$'; j++;
                        }
                    } else {
                        tarray[j] = carray[i]; j++;
                        tarray[j] = '}'; j++;
                        tarray[j] = '$'; j++;
                    }
                }
                if (carray[i] == '\\' && carray[i + 1] == 's' && carray[i + 2] == 'q' && carray[i + 3] == 'r' && carray[i + 4] == 't' && carray[i + 5] == '[') {
                    flag1=1;
                    tarray[j] = '$';j++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    int recClosing=str.indexOf("]", i);
                    for(;i<recClosing+2;) {
                        tarray[j] = carray[i];j++;i++;
                    }
                    int closing = 1, k = 0, l = 0;
                    for (k = i; closing != 0; k++) {
                        if (carray[k] == '{') {
                            closing++;
                        }
                        if (carray[k] == '}') {
                            closing--;
                        }
                    }
                    for (; i < k; ) {
                        if(carray[i]=='$') {
                            i++;                                    
                        } else {
                            tarray[j] = carray[i];
                            j++; i++;
                        }
                    }
                        i--;
                    tarray[j] = '$';j++;        
                }
                if (carray[i] == '\\' && carray[i + 1] == 's' && carray[i + 2] == 'q' && carray[i + 3] == 'r' && carray[i + 4] == 't' && carray[i + 5] == '{') {
                    flag1=1;
                    tarray[j] = '$'; j++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    int closing = 1, k = 0, l = 0;
                    for (k = i; closing != 0; k++) {
                        if (carray[k] == '{') {
                            closing++;
                        }
                        if (carray[k] == '}') {
                            closing--;
                        }
                    }
                    for (; i < k; ) {
                        if(carray[i]=='$') {
                            i++;                                    
                        } else {
                            tarray[j] = carray[i];
                            j++; i++;
                        }
                    }
                    i--;
                    tarray[j] = '$';j++;        
                }
                if(flag1==0) {
                    tarray[j] = carray[i];
                    j++;
                }
            }
            String s1 = new String(tarray);
            s1 = s1.trim();
//            System.out.println(s1);
            return s1;
        }
        return str;
    }
    
    private String manageRaiseToSign(String str) { //11
        if(str.contains("^")) {          
            str="  "+str+"  ";
            char [] carray = str.toCharArray();
            int clen=carray.length;
            int cposition=0;
            char [] tarray = new char[str.length()+100];
            for(int i=0,j=0;i<clen;) {
                if(carray[i]=='^'&&dollerCount(str, i)) {
                    if(carray[i-1]=='\\') {
                        tarray[j-1]='$';
                        tarray[j]='\\';j++;
                        tarray[j]='h';i++;j++;
                        tarray[j]='a';j++;
                        tarray[j]='t';j++;
                        tarray[j]=' ';j++;
                        for(;carray[i]==' ';) {
                            i++;
                        }
                        tarray[j]=carray[i];i++;j++;
                        tarray[j]='$';j++;
                    } else {
                        tarray[j] = '$';
                        j++;
                        if (carray[i + 1] == '{') {
                            cposition = singleClosingBrackPosition(str, i + 1);
                            for (; i <= cposition;) {
                                if (carray[i] == '$') {
                                    i++;
                                } else {
                                    tarray[j] = carray[i]; i++; j++;
                                }
                            }
                            tarray[j] = '$';
                            j++;
                        } else {
    //                   System.out.println("m here");
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = carray[i]; j++; i++;
                            tarray[j] = '$'; j++;
                        }
                    }
                } else {
                    tarray[j]=carray[i]; j++; i++;
                }
            }
        String s=new String(tarray);
        s=s.trim();
        return s;
        }
        return str;
    }
    
    private boolean dollerCount(String str,int upto) {
        char []carray = str.toCharArray();
        int dollercount=0;
        boolean flag;
        for(int i=0;i<upto;i++) {
            if(carray[i]=='$') {
                dollercount++; 
            }
        }
        if((dollercount%2)==0||dollercount==0) {
            flag=true;
        } else {
            flag=false;
        }
        return flag;
    }
    
    private int singleClosingBrackPosition(String str,int position) {
        char [] carray = str.toCharArray();
        int clen=carray.length;
        int count=0;
        int flag=0,flag1=0;
        for(int i=position;i<clen;i++) {
            if(carray[i]=='{') {
                count++;
                flag=1;                
            }
            if(carray[i]=='}') {
                count--;
            }
            if(count==0&&flag1==1) {
                flag1=0;
                flag=0;
//                System.out.println("position "+(i)+"Charector "+carray[i]);
            }
            if(count==0&&flag==1) {
                System.out.println(i);
                System.out.println(carray[i]);
                flag=0;
                flag1=1;
                return i;
            }
        }
//       System.out.println("Out of Form");
        return 1;
    }
    
    private String manageUnderscoreSignFirst(String str) { //12
        if (str.contains("_")) {
            str=addSlashToUnderscore(str);
            str="  "+str+"  ";
            char[] carray = str.toCharArray();
            int clen = carray.length;
            int cposition = 0;
            char[] tarray = new char[str.length() + 100];
            for (int i = 0, j = 0; i < clen;) {
                if (carray[i] == '\\' && carray[i + 1] == '_' && carray[i - 1] != '$' && hashCount(str, i)) {
                    tarray[j] = '$';
                    j++;
                    if (carray[i + 2] == '{') {
                        cposition = singleClosingBrackPosition(str, i + 1);
//                   System.out.println("cposition"+cposition);
                        i++;
                        for (; i <= cposition;) {
                            if (carray[i] == '$') {
                                i++;
                            } else {
                                tarray[j] = carray[i]; i++; j++;
                            }
                        }
                        tarray[j] = '$';
                        j++;
                    } else {
                        i++;
//                   System.out.println("m here");
                        tarray[j] = carray[i]; j++; i++;
                        tarray[j] = carray[i]; j++; i++;
                        tarray[j] = '$';
                        j++;
                    }
                } else {
                    tarray[j] = carray[i]; j++; i++;
                }
            }
//       System.out.println(tarray);
            String s = new String(tarray);
            s = s.trim();
            return s;
        }
        return str;
    }
    
    private String addSlashToUnderscore(String str) {
        str="  "+str+"  ";
        char[] carray = str.toCharArray();
        int clen = carray.length;
        char[] tarray = new char[str.length() + 100];
        for (int i = 0, j = 0; i < clen;) {
            if(carray[i]=='_'&&carray[i-1]!='\\') {
                tarray[j]='\\';j++;
                tarray[j]=carray[i];i++;j++;
            } else {
                tarray[j]=carray[i];i++;j++;
            }                    
        }
        String s = new String(tarray);
        s = s.trim();
        return s;
    }
    
    private boolean hashCount(String str,int upto) {
        char []carray = str.toCharArray();
        int hashcount=0;
        boolean flag;
        for(int i=0;i<upto;i++) {
            if(carray[i]=='$') {
                hashcount++; 
            }
        }
        if((hashcount%2)==0||hashcount==0) {
            flag=true;
        } else {
            flag=false;
        }
        return flag;
    }
    
    private String manageUnderscoreSignSecond(String str) { //13
        if(str.contains("_")) {
            str="  "+str+"  ";
            char[] carray = str.toCharArray();
            int clen = carray.length;
            int j = 0;
            char[] tarray = new char[str.length() + 100];
            for (int i = 0; i < clen;) {
                if(carray[i]=='\\'&&carray[i+1]=='_') {
                    i+=1;
                    tarray[j]='$';j++;
                    tarray[j]=carray[i];j++;i++;
                    if(carray[i]=='{') {
                        tarray[j]=carray[i];j++;i++;
                        int closingindex = closingIndexSecond(str, i);
                        for (; i <=closingindex; i++) {
                            tarray[j] = carray[i];
                            j++;
                        }
                        tarray[j] = '$';
                        j++;
                    } else {
                        int spqceindex = str.indexOf(" ", i);
                        tarray[j]=carray[i];j++;i++;
                        
//                        for (; i < spqceindex; i++) {
//                            tarray[j] = carray[i];
//                            j++;
//                        }
//                        i--;
                        tarray[j] = '$';
                        j++;
                    }
                } else {
                    tarray[j]=carray[i];i++;
                    j++;
                }             
            }
            String s1 = new String(tarray);
            s1=s1.trim();
            System.out.println(s1);
            return s1;  
        }
        return str;
    }
    
    private int finalcolsing2=0;
    private int closingIndexSecond(String str, int i) {
        int closing=str.indexOf("}", i);
        int opening=str.indexOf("{", i);        
        if((opening==-1)||(closing<opening)) {
            finalcolsing2=closing;           
        } else {
            closingIndexSecond(str, closing+1);            
        }
        return finalcolsing2;
    }
    
    private String fManagedFrac(String str) {  //14
        if(str.contains("\\dfrac")) {
            str=" "+str;
            int i=0,j=0,clen;
            char [] tarray ;
            char [] carray;
            carray = str.toCharArray();
            clen=carray.length;int cposition=0;
            tarray= new char[str.length()+100];
            for(;i<clen;) {
                if(i!=0&&carray[i-1]!='$'&&carray[i]=='\\'&&carray[i+1]=='d'&&carray[i+2]=='f'&&carray[i+3]=='r'&&carray[i+4]=='a'&&carray[i+5]=='c'&& hashCount(str, i)) {
                    tarray[j]='$'; j++;
                    int k=doubleClosingBrackPosition(str, i);
                    for(;i<=k;) {
                        if(carray[i]=='$') {
                            i++;       
                        } else {
                            tarray[j]=carray[i]; j++; i++;
                        }
                    }
                    tarray[j]='$';j++;
                } else {
                    tarray[j]=carray[i]; j++; i++;
                }
            }
            String string1 =new String(tarray);
            string1=string1.trim();
            return string1;
//       System.out.println(tarray);
        }
        return str;
    }
    
    private int doubleClosingBrackPosition(String str,int position) {
        char [] carray = str.toCharArray();
        int clen=carray.length;
        int count=0;
        int flag=0,flag1=0;
//        System.out.println(str);
        for(int i=position;i<clen;i++) {
            if(carray[i]=='{') {
                count++;
                flag=1;                
            }
            if(carray[i]=='}') {
                count--;
            }
            if(count==0&&flag1==1) {
                flag1=0;
                flag=0;
                return i;
            }
            if(count==0&&flag==1) {
                flag=0;
                flag1=1;
            }
        }
        return 1;
    }
    
    private String manageBeginEndSpaceF(String str) {//15
        if(str.contains("\\begin{")) {
            str="  "+str+"  ";
            int i = 0, j = 0, clen;
            char[] tarray;
            char[] carray;
            carray = str.toCharArray();
            clen = carray.length;
            int cposition = 0;
            tarray = new char[str.length() + 100];
            for(;i<clen;) {
                if(carray[i]=='\\') {
                    if(carray[i]=='\\'&&carray[i+1]=='b'&&carray[i+2]=='e'&&carray[i+3]=='g'&&carray[i+4]=='i'&&carray[i+5]=='n'&&carray[i+6]=='{') {
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]=carray[i];j++;i++;
                        tarray[j]=carray[i];j++;i++;
                        int indexofend=str.indexOf("\\end{", i);
                        int newindexofend=str.indexOf("}", indexofend);
                        for(;i<newindexofend+1;) {
                            tarray[j]=carray[i];j++;i++;
                        }
                        tarray[j]=' ';j++;
                    } else {
                        tarray[j]=carray[i];j++;i++;
                    }
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            String s=new String(tarray);
            s=s.trim();
            System.out.println(s);
            return s;
        }
        return str;
    }
    
    private String checkAll(String str) { //16
        str="  "+str+"  ";
        char [] tarray;
        char [] carray = str.toCharArray();
        int clen=carray.length; 
        int flag=0;
        tarray = new char[str.length()+100];
        for(int i=0,j=0;i<clen;) {
            if(i!=0&&carray[i-1]!='$'&&carray[i]=='\\') {
                flag=0;
                tarray[j]='$';j++;
                for(;flag==0&&i<clen;) {
                    if(carray[i]=='$'||carray[i]==' '||carray[i]=='{'||carray[i]=='1'||carray[i]=='2'||carray[i]=='3'||carray[i]=='4'||carray[i]=='5'||carray[i]=='6'||carray[i]=='7'||carray[i]=='8'||carray[i]=='9'||carray[i]=='0'||i==clen-1) {
                            if(flag==0&&carray[i]==' ') {
                                tarray[j]='$';j++;
                                tarray[j]=carray[i];i++;j++;
                                flag=1;
                            }
                            if(flag==0&&carray[i]=='{') {
                                int m=singleClosingBrackPosition(str, i);
                                if(carray[m+1]=='{') {
                                    m=doubleClosingBrackPosition(str, i);
                                }
                                for(;i<=m;) {
                                    if(carray[i]=='$') {
                                        i++;       
                                    } else {
                                        tarray[j]=carray[i]; j++; i++;
                                    }
                                }               
//                                tarray[j]='$';j++;
                            }
                            if(flag==0&&carray[i]=='1'||carray[i]=='2'||carray[i]=='3'||carray[i]=='4'||carray[i]=='5'||carray[i]=='6'||carray[i]=='7'||carray[i]=='8'||carray[i]=='9'||carray[i]=='0') {
                                tarray[j]='$';j++;
                                tarray[j]=carray[i];i++;j++;
                                flag=1;                       
                            }
                            if(flag==0&&carray[i]=='$') {
                                tarray[j]='$';j++;
                                tarray[j]=carray[i];i++;j++;
                                flag=1;                       
                            }
                            if(flag==0&&i==clen-1) {
                                tarray[j]=carray[i];j++;
                                tarray[j]='$';j++;i++;
                                flag=1;
                            }
                    } else {
                        tarray[j]=carray[i]; j++; i++;
                    }
                }
            } else {
                tarray[j]=carray[i]; i++; j++;
            }
        }
//       System.out.println(tarray);
        String s1 = new String(tarray);
        s1=s1.trim();
        System.out.println("Fcheck func"+s1);
//       s1=removedfracerror(s1);
        return s1;
    }
    
    private String manageBeginEnd(String str) { //17
        if(str.contains("\\begin{")) {
            str="  "+str+"  ";
            int i = 0, j = 0, clen;
            char[] tarray;
            char[] carray;
            carray = str.toCharArray();
            clen = carray.length;
            tarray = new char[str.length() + 100];
            for(;i<clen;) {
                if(carray[i]=='\\') {
                    if(carray[i]=='\\'&&carray[i+1]=='b'&&carray[i+2]=='e'&&carray[i+3]=='g'&&carray[i+4]=='i'&&carray[i+5]=='n'&&carray[i+6]=='{') {
                        if(carray[i-1]!='$') {
                            tarray[j]='$';j++;
                        }
                        tarray[j]=carray[i]; j++; i++;
                        tarray[j]=carray[i]; j++; i++;
                        tarray[j]=carray[i]; j++; i++;
                        tarray[j]=carray[i]; j++; i++;
                        tarray[j]=carray[i]; j++; i++;
                        tarray[j]=carray[i]; j++; i++;
                        tarray[j]=carray[i]; j++; i++;
                        int indexofend=str.indexOf("\\end{", i);
                        int newindexofend=str.indexOf("}", indexofend);
                        for(;i<newindexofend+1;) {
                            if(carray[i]=='$') {
                                i++;
                            } else {
                                tarray[j]=carray[i];j++;i++;
                            }
                        }
                        if(carray[i]!='$') {
                            int asckii=(int)carray[i];
                            if((65>asckii&&asckii<90)||(97>asckii&&asckii<122)) {
                                tarray[j]=carray[i];j++;i++;
                                tarray[j]='$';j++;
                                tarray[j]=' ';j++;
                            } else {
                                tarray[j]='$';j++;
                                tarray[j]=' ';j++;                                
                            }
                        }
                    } else {
                        tarray[j]=carray[i];j++;i++;
                    }
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            String s=new String(tarray);
            s=s.trim();
            System.out.println(s);
            return s;
        }
        return str;
    }
    
    private String manageLeftRigtCommand(String str) { //18
        if(str.contains("\\left")) {
            str="  "+str+"  ";
            int i = 0, j = 0, clen;
            char[] tarray;
            char[] carray;
            carray = str.toCharArray();
            clen = carray.length;
            tarray = new char[str.length() + 100];
            for (; i < clen;) {
                if(carray[i]=='\\'&&carray[i+1]=='l'&&carray[i+2]=='e'&&carray[i+3]=='f'&&carray[i+4]=='t') {
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    int rightIndex=rightIndex(str, i);
                    for(;i<rightIndex;) {
                        if(carray[i]=='$') {
                            i++;
                        } else {
                            tarray[j]=carray[i];j++;i++;
                        }
                    }
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            String s=new String(tarray);
            s=s.trim();
//            System.out.println("OUTPUT "+s);
            return s;
        }
        return str;
    }
    
    private int rightIndex(String str,int i) {
        int leftIndex,rightIndex,Count=1,k=i,flag=0;
        for(;Count!=0;) {
            flag=0;
            leftIndex=str.indexOf("\\left", k);
            rightIndex=str.indexOf("\\right", k);
            if(leftIndex==-1&&flag==0) {
                Count--;
                flag=1;
                k=rightIndex;
            }
            if(leftIndex<rightIndex&&flag==0) {
                Count++;
                k=rightIndex+4;
                flag=1;
            }
            if(rightIndex<leftIndex&&flag==0) {
                Count--;
                k=rightIndex;
                flag=1;
            }
        }
        System.out.println("Right Index"+k);
        return k;
    }
    
    private String manageRaisToDoller(String str) { //19
        if(str.contains("^")){
            str="  "+str+"  ";
            char[] carray = str.toCharArray();
            int clen = carray.length;
            int j = 0;
            char[] tarray = new char[str.length() + 100];
            int k=0;
            for (int i = 0; i < clen;) {
                if(carray[i]=='^'&&carray[i+1]=='{') {
                    k=newSingleClosingBracketPosition(str, i);
                    for(;i<k&&i<clen;) {
                        if(carray[i]=='$') {
                            i++;
                        } else {
                            tarray[j]=carray[i];i++;j++;
                        }
                    }
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            String s=new String(tarray);
            s=s.trim();
    //      System.out.println("OUTPUT "+s);
            return s;
        }
        return str;
    }
    
    private String fromPosition(String str) { //20
        str=" "+str+" ";
        char [] tarray;
        char [] carray = str.toCharArray();
        int clen=carray.length; 
        int j=0;
        int dollercount=0;
        tarray = new char[str.length()+100];
        //and 1.217 $\\times$ 10$^{-4}$
        for(int i=0;i<clen;i++) {
            if(carray[i]=='$')
                dollercount++;
            if((carray[i]=='_'||carray[i]=='^')&&carray[i+1]=='{') {
                tarray[j]=carray[i];j++;i++;
                tarray[j]=carray[i];j++;i++;
                int closingbrac =1;
                for(;closingbrac!=0;i++) {
                    if(carray[i]=='{') {
                        closingbrac++;
                    }
                    if(carray[i]=='}') {
                        closingbrac--;
                    }
                    if(carray[i]!='$') {
                        tarray[j]=carray[i];
                        j++;
                        System.out.println(tarray[i]+" index"+i);
                    }
                    if(carray[i]=='$')
                        dollercount++;
                }
                if(carray[i]=='$')
                    dollercount++;
            }
            if(carray[i]=='$'&&dollercount%2==1) {
//               dollercount--;
                if(carray[i+1]=='\\'||carray[i+1]=='^'||carray[i+1]=='_') {
                    tarray[j]=carray[i];
                    System.out.println(tarray[i]+" index"+i);
                    j++;
                }                   
            } else {
                tarray[j]=carray[i];
                System.out.println(tarray[i]+" index"+i);
                j++;
            }
        }
        String s1 = new String(tarray);
        s1=s1.trim();
        System.out.println("1st processing"+s1);
        return s1;
    }
    
    private String manageLimitsF(String str) { //21
        if(str.contains(str)) {
            str="  "+str+"  ";
            int i = 0, j = 0, clen;
            char[] tarray;
            char[] carray;
            carray = str.toCharArray();
            clen = carray.length;
            tarray = new char[str.length() + 100];
            for (; i < clen;) {
                if(carray[i]=='\\'&&carray[i+1]=='l'&&carray[i+2]=='i'&&carray[i+3]=='m'&&carray[i+4]=='i'&&carray[i+5]=='t'&&carray[i+6]=='s') {
                    if (carray[i - 1] == '$') {
                        j--;
                    }
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    for(;carray[i]=='$';i++) {
                    }
                    if (carray[i] == '_') {
                        tarray[j] = carray[i]; j++; i++;
                        if (carray[i] == '{') {
                            tarray[j] = carray[i]; j++; i++;
                            int closingindex = closingIndexThird(str, i);
                            for (; i <=closingindex; i++) {
                                if (carray[i] == '$') {
                                    i++;
                                } else {
                                    tarray[j] = carray[i];
                                    j++;
                                }
                            }
                        } else {
                            int spqceindex = str.indexOf(" ", i);
                            tarray[j] = carray[i]; j++; i++;
//                          for (; i < spqceindex; i++) {
//                            tarray[j] = carray[i];
//                            j++;
//                        }
//                        i--;
                        }
                    }
                    for(;carray[i]=='$';i++) {
                    }
                    if (carray[i] == '^') {
                        tarray[j] = carray[i]; j++; i++;
                        if (carray[i] == '{') {
                            tarray[j] = carray[i]; j++; i++;
                            int closingindex = closingIndexThird(str, i);
                            for (; i <=closingindex; i++) {
                                if (carray[i] == '$') {
                                    i++;
                                } else {
                                    tarray[j] = carray[i];
                                    j++;
                                }
                            }
                        } else {
                            int spqceindex = str.indexOf(" ", i);
                            tarray[j] = carray[i]; j++; i++;
//                        for (; i < spqceindex; i++) {
//                            tarray[j] = carray[i];
//                            j++;
//                        }
//                        i--;
                        }
                    }
                    for(;carray[i]=='$';i++) {
                    }
                    if (carray[i] == '_') {
                        tarray[j] = carray[i]; j++; i++;
                        if (carray[i] == '{') {
                            tarray[j] = carray[i]; j++; i++;
                            int closingindex = closingIndexThird(str, i);
                            for (; i <=closingindex; i++) {
                                if (carray[i] == '$') {
                                    i++;
                                } else {
                                    tarray[j] = carray[i];
                                    j++;
                                }
                            }
                        } else {
                            int spqceindex = str.indexOf(" ", i);
                            tarray[j] = carray[i]; j++; i++;
//                        for (; i < spqceindex; i++) {
//                            tarray[j] = carray[i];
//                            j++;
//                        }
//                        i--;
                        }
                    }
                    if(carray[i]!='$')
                    {
                        tarray[j]='$';j++;
                    }
                } else {
                    tarray[j]=carray[i];j++;i++;
                }
            }
            String s1 = new String(tarray);
            s1=s1.trim();
            System.out.println("OUTPUT"+s1);
            return s1;  
        }
        return str;
    }
    
    private int closingIndexThird(String str, int i) {
        int finalcolsing=0;
        int closing=str.indexOf("}", i);
        int opening=str.indexOf("{", i);        
        if((opening==-1)||(closing<opening)) {
            finalcolsing=closing;           
        } else {
            closingIndexThird(str, closing+1);            
        }
        return finalcolsing;
    }
    
    private String manageDfrac(String str) { //22
        if(str.contains("\\dfrac")){
            char[] carray = str.toCharArray();
            int clen = carray.length;
            int j = 0;
            char[] tarray = new char[str.length() + 100];
            for (int i = 0; i < clen;) {
                if(carray[i]=='\\'&&carray[i+1]=='d'&&carray[i+2]=='f'&&carray[i+3]=='r'&&carray[i+4]=='a'&&carray[i+5]=='c') {
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    tarray[j]=carray[i]; j++; i++;
                    int closingindex=closingIndexFourth(str, i);
                    for (; i < closingindex+1; i++) {
                        if(carray[i]!='$') {
                            tarray[j] = carray[i];
                            j++;
                        }
                    }
                    tarray[j]=carray[i];j++;i++;
                    int closingindex1=closingIndexFourth(str, i);
                    for (; i < closingindex1; i++) {
                        if(carray[i]!='$') {
                            tarray[j] = carray[i];
                            j++;
                        }
                    }
//                        for (; i < spqceindex; i++) {
//                            tarray[j] = carray[i];
//                            j++;
//                        }
//                        i--;
                    
                } else {
                    tarray[j]=carray[i];i++;
                    j++;
                }             
            }
            String s1 = new String(tarray);
            s1=s1.trim();
            System.out.println(s1);
            return s1;  
        }
        return str;
    }

    int finalcolsing3=0;
    private int closingIndexFourth(String str, int i) {
        int closing=str.indexOf("}", i);
        int opening=str.indexOf("{", i);        
        if((opening==-1)||(closing<opening)) {
            finalcolsing3=closing;           
        } else {
            closingIndexFourth(str, closing+1);            
        }
        return finalcolsing3;
    }

    private String removeDollerSqrt(String str) { //23
        if(str.contains("\\sqrt{")) {          
            str=" "+str+" ";   
            char [] carray = str.toCharArray();
            int clen=carray.length;
            int cposition=0;
            char [] tarray = new char[str.length()+100];
            for(int i=0,j=0;i<clen;) {  
                if(carray[i] == '\\' && carray[i + 1] == 's' && carray[i + 2] == 'q' && carray[i + 3] == 'r' && carray[i + 4] == 't' && carray[i + 5]== '{') {
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    tarray[j] = carray[i]; j++; i++;
                    int closing = 1, k = 0, l = 0;
                        for (k = i; closing != 0; k++) {
                            if (carray[k] == '{') {
                                closing++;
                            }
                            if (carray[k] == '}') {
                                closing--;
                            }
                        }
                        for(;i<k;) {
                            if(carray[i]=='$') {
                                i++;
                            } else {
                                tarray[j] = carray[i]; j++; i++;
                            }
                        }
                            
                } else {
                    tarray[j] = carray[i];j++;i++;
                }
            }
            String s1 = new String(tarray);
            s1 = s1.trim();
            System.out.println(s1);
            return s1;
        }
        return str;
    }
    
    private String addDoller(String str) { //24
        str="  "+str+"  ";
        char[] tarray;
        int j =0;
        char[] carray = str.toCharArray();
        tarray = new char[str.length() + 100];
        int clen = carray.length;
        int coI=0;
        for (int l = 0; l< clen;l++) {
            if(carray[l]=='$') {
                coI++;                    
            }
        }
        if(coI%2!=0) {
            int lastclosing=str.lastIndexOf('}');
            int lastslash=str.lastIndexOf('\\');
            int lastraiseto=str.lastIndexOf('^');
            int lastUnderscore=str.lastIndexOf('_');
            int intlist[]={lastclosing,lastslash,lastraiseto,lastUnderscore};
            int k=0,last=intlist[k];
            for(;k<intlist.length-1;k++) {   
                if(last<intlist[k+1]) {
                    last=intlist[k+1];
                }  
            }
            System.out.println(last);
            if(carray[last]=='}') {
                int i=0;
                j=0;
                for(;i<=last;) {
                    tarray[j]=carray[i]; j++; i++;
                }
                tarray[j]='$'; j++;
                for(;i<clen;) {
                    tarray[j]=carray[i]; j++; i++;
                }
                String s1 = new String(tarray);
                s1=s1.trim();
                System.out.println("OUTPUT }"+s1);
                return s1; 
            }
            if(carray[last]=='\\') {
                int lastspace=str.indexOf(' ', last);
                int i=0;
                j=0;
                for(;i<lastspace;) {
                    tarray[j]=carray[i]; j++; i++;
                }
                tarray[j]='$';j++;
                for(;i<clen;) {
                    tarray[j]=carray[i]; j++; i++;
                }
                String s1 = new String(tarray);
                s1=s1.trim();
                System.out.println("OUTPUT \\"+s1);
                return s1;
            }
            if(carray[last]=='^') {
                int i=0;
                j=0;
                for(;i<=last+1;) {
                    tarray[j]=carray[i]; j++; i++;
                }
                tarray[j]='$';j++;
                for(;i<clen;) {
                    tarray[j]=carray[i]; j++; i++;
                }
                String s1 = new String(tarray);
                s1=s1.trim();
                System.out.println("OUTPUT ^"+s1);
                return s1;
            }
            if(carray[last]=='_') {
                int i=0;
                j=0;
                for(;i<=last+1;) {
                    tarray[j]=carray[i]; j++; i++;
                }
                tarray[j]='$';j++;
                for(;i<clen;) {
                    tarray[j]=carray[i];j++;i++;
                }
                String s1 = new String(tarray);
                s1=s1.trim();
                System.out.println("OUTPUT _"+s1);
                return s1;
            }
        }
        return str;
    }
    
    private String manageDollerType(String str) { //25
        str=" "+str+" ";
        int i=0,j=0,clen;
        int dollercount=0;
        char [] tarray ;
        char [] carray;
        carray = str.toCharArray();
        clen=carray.length;
        tarray= new char[str.length()+100];
        int lastdollerIndex = str.lastIndexOf('$');
        for (; i < clen; i++) {
            if (carray[i] == '$') {
                dollercount++;
            }
        }
        if(carray[lastdollerIndex+1]=='\\'&&carray[lastdollerIndex+2]=='d'&&carray[lastdollerIndex+3]=='f'&&carray[lastdollerIndex+4]=='r'&&carray[lastdollerIndex+5]=='a'&&carray[lastdollerIndex+6]=='c'&&carray[lastdollerIndex+7]=='{'&&(dollercount%2==0)) {
            int k=0;
            for (; k <lastdollerIndex; k++) {
                tarray[j]=carray[k];j++;
            }
            k++;
            tarray[j]=carray[k]; j++; k++;
            tarray[j]=carray[k]; j++; k++;
            tarray[j]=carray[k]; j++; k++;
            tarray[j]=carray[k]; j++; k++;
            tarray[j]=carray[k]; j++; k++;
            tarray[j]=carray[k]; j++; k++;
            tarray[j]=carray[k]; j++; k++;
            int closingindex=closingIndexFifth(str, k);
            for (; k < closingindex+1; k++) {
                tarray[j] = carray[k];j++;
            }
            tarray[j]=carray[k]; j++; k++;
            int closingindex1=closingIndexFifth(str, k);
            for (; k < closingindex1+1; k++) {
                tarray[j] = carray[k];j++;
            }
            tarray[j]='$'; j++;       
            for (; k < clen; k++) {
                tarray[j] = carray[k]; j++;
            }
            String s1 = new String(tarray);
            s1=s1.trim();
            System.out.println("OUTPUT "+s1);
            return s1;
        }
        return str;
    }
    
    int finalcolsing4=0;
    private int closingIndexFifth(String str, int i) {
        int closing=str.indexOf("}", i);
        int opening=str.indexOf("{", i);        
        if((opening==-1)||(closing<opening)) {
            finalcolsing4=closing;           
        } else {
            closingIndexFifth(str, closing+1);            
        }
        return finalcolsing4;
    }
}
