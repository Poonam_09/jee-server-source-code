/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ui.support.pages;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author Aniket
 */
public class ImageResizer {
       
    //Aniket
    public static BufferedImage resizeBufferImage(BufferedImage bufferedImage, int scaledWidth, int scaledHeight) {

        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, bufferedImage.getType());
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(bufferedImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
        return outputImage;
    }
    
//    public static void resize(String inputImagePath, String outputImagePath, int scaledWidth, int scaledHeight)
//            throws IOException {
//        // reads input image
//        File inputFile = new File(inputImagePath);
//        BufferedImage inputImage = ImageIO.read(inputFile);
//
//        // creates output image
//        BufferedImage outputImage = new BufferedImage(scaledWidth,
//                scaledHeight, inputImage.getType());
//
//        // scales the input image to the output image
//        Graphics2D g2d = outputImage.createGraphics();
//        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
//        g2d.dispose();
//
//        // extracts extension of output file
//        String formatName = outputImagePath.substring(outputImagePath
//                .lastIndexOf(".") + 1);
//
//        // writes to output file
//        ImageIO.write(outputImage, formatName, new File(outputImagePath));
//    }
//
//    public static void resize(String inputImagePath,
//            String outputImagePath, double percent) throws IOException {
//        File inputFile = new File(inputImagePath);
//        BufferedImage inputImage = ImageIO.read(inputFile);
//        int scaledWidth = (int) (inputImage.getWidth() * percent);
//        int scaledHeight = (int) (inputImage.getHeight() * percent);
//        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
//    }
//
//    /**
//     * Test resizing images
//     */
//    public static void main(String[] args) {
//        String inputImagePath = "D:/ee/22.png";
//        String outputImagePath1 = "D:/ee/out_Fixed.jpg";
//        String outputImagePath2 = "D:/ee/out_Smaller.jpg";
//        String outputImagePath3 = "D:/ee/out_Bigger.jpg";
//
//        try {
//            // resize to a fixed width (not proportional)
//            int scaledWidth = 1024;
//            int scaledHeight = 768;
//            ImageResizer.resize(inputImagePath, outputImagePath1, scaledWidth, scaledHeight);
//
//            // resize smaller by 50%
//            double percent = 0.5;
//            ImageResizer.resize(inputImagePath, outputImagePath2, percent);
//
//            // resize bigger by 50%
//            percent = 1.5;
//            ImageResizer.resize(inputImagePath, outputImagePath3, percent);
//
//        } catch (IOException ex) {
//            System.out.println("Error resizing the image.");
//            ex.printStackTrace();
//        }
//    }
}
