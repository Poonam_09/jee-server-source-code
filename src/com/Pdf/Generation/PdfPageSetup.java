/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Pdf.Generation;

import com.Model.FileChooser;
import com.Model.ProcessManager;
import com.pages.HomePage;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import com.bean.ChapterBean;
import com.bean.HeaderFooterTextBean;
import com.bean.PdfPageSetupBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.db.operations.PageSetupOperation;
import com.pages.MultipleChapterQuestionsSelection;
import com.pages.MultipleYearQuestionsSelection;
import com.pages.SingleChapterQuestionsSelection;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.apache.commons.io.FileUtils;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import com.Model.TitleInfo;
import org.jdesktop.swingx.prompt.PromptSupport;

/**
 *
 * @author SAGAR
 */
public class PdfPageSetup extends javax.swing.JFrame {
    private ArrayList<QuestionBean> selectedQuestionsList;
    private ArrayList<QuestionBean> questionsList;
    private ArrayList<ChapterBean> chaptersList;
    private ArrayList<SubjectBean> selectedSubjectList;
    private CardLayout pageSetupLayout;
    private PdfPageSetupBean pdfPageSetupBean;
    private CardLayout footerLeftLayout;
    private CardLayout footerCenterLayout;
    private CardLayout footerRightLayout;
    private CardLayout headerLeftLayout;
    private CardLayout headerCenterLayout;
    private CardLayout headerRightLayout;
    private CardLayout waterMarkLayout;
    private int pageCardValue;
    private String processPath;
    private ArrayList<Integer> pageCardList;
    private File sourceLogo;
    private String waterMarkImagePath;
    private File waterMarkImageFile;
    private String printingPaperType;
    private Object quesPageObject;
    
    private String sourceLogoString;
    

    public PdfPageSetup() {
        initComponents();
        setLocation(0, 0);
        buttonGroup1.add(ChkFirstFormat);
        buttonGroup1.add(ChkSecondFormat);
        
        setLocationRelativeTo(null);
    }
    
    //Aniket All
    public PdfPageSetup(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chaptersList,Object quesPageObject,ArrayList<QuestionBean> questionsList,String printingPaperType) {
        initComponents();
        this.selectedQuestionsList = selectedQuestionsList;
        this.chaptersList = chaptersList;
        this.printingPaperType = printingPaperType;
        this.questionsList = questionsList;
        this.selectedSubjectList = selectedSubjectList;
//        this.quesFrame = quesFrame;
        this.quesPageObject = quesPageObject;
        setInitialValues();
    }
            
            
//    //Aniket ChapterWise
//    public PdfPageSetup(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> selectedSubjectList,ArrayList<ChapterBean> chaptersList,SingleChapterQuestionsSelection selectQuestionsChapterWise,ArrayList<QuestionBean> questionsList,String printingPaperType) {
//        initComponents();
//        this.selectedQuestionsList = selectedQuestionsList;
//        this.chaptersList = chaptersList;
//        this.printingPaperType = printingPaperType;
//        this.questionsList = questionsList;
//        this.selectQuestionsChapterWise = selectQuestionsChapterWise;
//        this.selectedSubjectList = selectedSubjectList;
//        selectQuestionsUnitsWise = null;
//        homePage = null;
//        selectQuestionsYearWise = null; 
//        setInitialValues();
//    }
    
//    //Aniket UnitsWise
//    public PdfPageSetup(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> selectedSubjectList, ArrayList<ChapterBean> chaptersList,MultipleChapterQuestionsSelection selectQuestionsUnitsWise,ArrayList<QuestionBean> questionsList,String printingPaperType) {
//        initComponents();
//        this.selectedQuestionsList = selectedQuestionsList;
//        this.questionsList = questionsList;
//        this.chaptersList = chaptersList;
//        this.printingPaperType = printingPaperType;
//        selectQuestionsChapterWise = null;
//        this.selectedSubjectList = selectedSubjectList;
//        this.selectQuestionsUnitsWise = selectQuestionsUnitsWise;
//        homePage = null;
//        selectQuestionsYearWise = null;
//        setInitialValues();
//    }
    
//    //Aniket GroupWise
//    public PdfPageSetup(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<ChapterBean> chaptersList,HomePage homePage,ArrayList<QuestionBean> questionsList,String printingPaperType) {
//        initComponents();
//        this.selectedQuestionsList = selectedQuestionsList;
//        this.questionsList = questionsList;
//        this.chaptersList = chaptersList;
//        this.printingPaperType = printingPaperType;
//        selectQuestionsChapterWise = null;
//        selectQuestionsUnitsWise = null;
//        this.homePage = homePage;
//        selectQuestionsYearWise = null;
//        setInitialValues();
//    }
    
//    //Aniket Previous Question Paper
//    public PdfPageSetup(ArrayList<QuestionBean> selectedQuestionsList,ArrayList<SubjectBean> subjectList,MultipleYearQuestionsSelection selectQuestionsYearWise,ArrayList<QuestionBean> questionsList,String printingPaperType) {
//        initComponents();
//        this.selectedQuestionsList = selectedQuestionsList;
//        this.questionsList = questionsList;
//        this.chaptersList = null;
//        this.printingPaperType = printingPaperType;
//        selectQuestionsChapterWise = null;
//        selectQuestionsUnitsWise = null;
//        homePage = null;
//        this.selectQuestionsYearWise = selectQuestionsYearWise;
//        setInitialValues();
//    }
    
//    //Aniket
//    public PdfPageSetup(ArrayList<QuestionBean> selectedQuestionsList, ArrayList<ChapterBean> chaptersList,SingleChapterQuestionsSelection selectQuestionsChapterWise, int QuestionPaperTypeFlag) {
//        initComponents();
//        this.selectedQuestionsList = selectedQuestionsList;
//        this.questionsList = questionsList;
//        this.chaptersList = chaptersList;
//        this.selectQuestionsChapterWise = selectQuestionsChapterWise;
//        setInitialValues();
//    }
    
    //Aniket
    private void setInitialValues() {
        setLocation(0, 0);
        setLocationRelativeTo(null);
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        buttonGroup1.add(ChkFirstFormat);
        buttonGroup1.add(ChkSecondFormat);
        buttonGroup2.add(ChkA4);
        buttonGroup2.add(ChkLegal);
        buttonGroup2.add(ChkLetter);
        pdfPageSetupBean = new PageSetupOperation().getPdfPagesSetup();
        setValues();
    }
    
    //Aniket
     private void setValues() {
        pageCardList = null;
        sourceLogoString = null;
        sourceLogo = null;
        waterMarkImagePath = null;
        waterMarkImageFile = null;
        LblLogoView.setText("Institute Name");
        processPath = new ProcessManager().getProcessPath();
        
        pageSetupLayout = (CardLayout) RightBodyPanel.getLayout();

//        pageSetupLayout.show(RightBodyPanel, "FirstCard");
        pageCardValue = 1;
        getUpdateNextStatus();
//        pageCardValue ++;
        footerLeftLayout = (CardLayout) PanelFooterOne.getLayout();
        footerCenterLayout = (CardLayout) PanelFooterTwo.getLayout();
        footerRightLayout = (CardLayout) PanelFooterThree.getLayout();
        headerLeftLayout = (CardLayout) PanelHeaderOne.getLayout();
        headerCenterLayout = (CardLayout) PanelHeaderTwo.getLayout();
        headerRightLayout = (CardLayout) PanelHeaderThree.getLayout();
        waterMarkLayout = (CardLayout) WaterMarkCardPanel.getLayout();
        footerLeftLayout.show(PanelFooterOne,"FooterLeftFirstCard");
        footerCenterLayout.show(PanelFooterTwo,"FooterCenterFirstCard");
        footerRightLayout.show(PanelFooterThree,"FooterRightFirstCard");
        headerLeftLayout.show(PanelHeaderOne,"HeaderLeftFirstCard");
        headerCenterLayout.show(PanelHeaderTwo,"HeaderCenterFirstCard");
        headerRightLayout.show(PanelHeaderThree,"HeaderRightFirstCard");
        this.getContentPane().setBackground(new Color(212, 208, 200));
        LblPaperTypeDimention.setText("<html><table bgcolor=\"#FFFFFF\"  border=\"2\" style=\"width:100%\">\n"
                + "\n"
                + "<tr>\n"
                + "<th>Size</th>\n"
                + "<th>Width x Height (mm)</th>\n"
                + "<th>Width x Height (in)</th>\n"
                + "<th>Area sq m</th>	<th>Area sq yd</th>\n"
                + "</tr>\n"
                + "  \n"
                + "  \n"
                + "<tr>\n"
                + "<th>A4</th>\n"
                + "<td align=\"center\"> 210 x 297 mm		\n"
                + "<td align=\"center\"> 8.3 x 11.7 in\n"
                + "<td align=\"center\"> 0.062 sq m\n"
                + "<td align=\"center\"> 0.074 sq yd\n"
                + "</tr>\n"
                + "				\n"
                + "				\n"
                + "<tr>   \n"
                + "<th>Letter</th>\n"
                + "<td align=\"center\"> 216 x 279 mm		\n"
                + "<td align=\"center\"> 8.5 x 11.0 in\n"
                + "<td align=\"center\"> 0.060 sq m\n"
                + "<td align=\"center\"> 0.072 sq yd   \n"
                + "</tr>\n"
                + "\n"
                + "\n"
                + "<tr>  \n"
                + "<th> Legal </th>\n"
                + "<td align=\"center\"> 216 x 356 mm		\n"
                + "<td align=\"center\"> 8.5 x 14.0 in\n"
                + "<td align=\"center\"> 0.077 sq m\n"
                + "<td align=\"center\"> 0.092 sq yd  \n"
                + "</tr>\n"
                + "\n"
                + "\n"
                + "\n"
                + "\n"
                + "</table><html>");
//        setButtonBorder(BtnPageFormat);
        BorderPanel.setVisible(false);
        WaterMarkSettingsPanel.setVisible(false);
        BtnBrowse.setVisible(false);
        WaterMarkAngleSlider.setMinorTickSpacing(45);
        WaterMarkAngleSlider.setMajorTickSpacing(45);
        WaterMarkAngleSlider.setPaintTicks(true);
        WaterMarkAngleSlider.setPaintLabels(true);
        WaterMarkAngleSlider.setLabelTable(WaterMarkAngleSlider.createStandardLabels(45));
        PromptSupport.setPrompt("Enter Text", TxtHeaderLeft);
        PromptSupport.setPrompt("Enter Text", TxtHeaderCenter);
        PromptSupport.setPrompt("Enter Text", TxtHeaderRight);
        PromptSupport.setPrompt("Enter Text", TxtFooterLeft);
        PromptSupport.setPrompt("Enter Text", TxtFooterCenter);
        PromptSupport.setPrompt("Enter Text", TxtFooterRight);
        LblHeaderFooterNote.setText("<html>(Note: Once you set attributes of header and footer, while creating question paper, header footer settings will be auto-filled that you set hear)<html>");
        if(printingPaperType.equalsIgnoreCase("UnitWise")) {
            ChkChapterList.setVisible(true);
            ChkChapterList.setSelected(true);
        } else {
            ChkChapterList.setVisible(false);
        }
         System.out.println(""+pdfPageSetupBean);
        if(pdfPageSetupBean != null)
            loadsSettings();
    }
     
    private void loadsSettings() {
        if(pdfPageSetupBean.getPageFormat() == 0)//Page Format
            ChkFirstFormat.setSelected(true);
        else 
            ChkSecondFormat.setSelected(true);

        ChkColumnType.setSelected(pdfPageSetupBean.isTwoColumn());//Column Type
        ChkYear.setSelected(pdfPageSetupBean.isPrintYear());//Print Year
        
        if((pdfPageSetupBean.getPaperType().trim()).equals("a4paper")){ //Paper Type
            ChkA4.setSelected(true);
        } else if((pdfPageSetupBean.getPaperType().trim()).equals("legalpaper")){
            ChkLegal.setSelected(true);
        } else {
            ChkLetter.setSelected(true);
        }
        setPaperTypePanel();
        
        if(pdfPageSetupBean.isSetLogo()) { //Set Logo
            ChkSetLogo.setSelected(true);
            setLogosPanel(pdfPageSetupBean.getLogoPath());
//            setLogoPanel(pdfPageSetupBean.getLogoPath());
            ChkImageActualSize.setSelected(pdfPageSetupBean.isSetImageActualsize()); // Actual Image Logo Size
        } else {
            ChkSetLogo.setSelected(false);
        }
        
        if(pdfPageSetupBean.getLogoPosition() == 0) { //Logo Position
            ChkLeft.setSelected(true);
            ChkRight.setSelected(false);
            ChkTop.setSelected(false);
            ChkBottom.setSelected(false);
        } else if(pdfPageSetupBean.getLogoPosition() == 1) {
            ChkLeft.setSelected(false);
            ChkRight.setSelected(true);
            ChkTop.setSelected(false);
            ChkBottom.setSelected(false);
        } else if(pdfPageSetupBean.getLogoPosition() == 2) {
            ChkLeft.setSelected(false);
            ChkRight.setSelected(false);
            ChkTop.setSelected(true);
            ChkBottom.setSelected(false);
        } else {
            ChkLeft.setSelected(false);
            ChkRight.setSelected(false);
            ChkTop.setSelected(false);
            ChkBottom.setSelected(true);
        }
        setLogoPosition();
        if(pdfPageSetupBean.getWaterMarkInfo() == 0) { //Watermark
            WaterMarkSettingsPanel.setVisible(false);
        } else if(pdfPageSetupBean.getWaterMarkInfo() == 1) {
            waterMarkLayout.show(WaterMarkCardPanel, "TextGrayCard");
            WaterMarkSettingsPanel.setVisible(true);
            ChkWaterMarkAsInstiName.setSelected(true);
            ChkWaterMarkAsInstiLogo.setSelected(false);
            WaterMarkAngleSlider.setValue(pdfPageSetupBean.getWaterMarkAngle());
            CmbWaterMarkTextGrayScale.setSelectedIndex(pdfPageSetupBean.getWaterMarkTextGrayScale());
            CmbWaterMarkScale.setSelectedIndex(pdfPageSetupBean.getWaterMarkScale() - 1);
            rotateWatermarkText(WaterMarkAngleSlider.getValue(), "\\text{Your Institute Name}");
        } else {
            waterMarkImagePath = pdfPageSetupBean.getWaterMarkLogoPath();
            waterMarkLayout.show(WaterMarkCardPanel, "WaterLogoCard");
            LblWaterLogoPath.setText("<html><font size=2>\n" +
                                    getSubString(pdfPageSetupBean.getWaterMarkLogoPath().trim())+"\n" +
                                    "</font></html>");
//            LblWaterMarkTextGrayScale
//            CmbWaterMarkTextGrayScale
//            WaterMarkTextGrayScalePanel
            WaterMarkSettingsPanel.setVisible(true);
            ChkWaterMarkAsInstiName.setSelected(false);
            ChkWaterMarkAsInstiLogo.setSelected(true);
            WaterMarkScalePanel.setVisible(true);
//            WaterMarkTextGrayScalePanel.setVisible(false);
            BtnBrowse.setVisible(true);
            WaterMarkAngleSlider.setValue(pdfPageSetupBean.getWaterMarkAngle());
            CmbWaterMarkScale.setSelectedIndex(pdfPageSetupBean.getWaterMarkScale() - 1);
            rotateWatermarkText(WaterMarkAngleSlider.getValue(), "\\includegraphics{"+processPath + "/watermarklogo.png}");
        }
        
        HeaderFooterTextBean headerFooterTextBean = null;
        headerFooterTextBean = pdfPageSetupBean.getHeaderLeftBean();
        if(headerFooterTextBean.isCheckBoxSelected()) {
            ChkHeaderLeft.setSelected(true);
            TxtHeaderLeft.setText(headerFooterTextBean.getTextValue().trim());
        } else {
            CmbHeaderLeft.setSelectedIndex(getHFComboIndex(headerFooterTextBean.getTextValue().trim()));
        }
        
        headerFooterTextBean = pdfPageSetupBean.getHeaderCenterBean();
        if(headerFooterTextBean.isCheckBoxSelected()) {
            ChkHeaderCenter.setSelected(true);
            TxtHeaderCenter.setText(headerFooterTextBean.getTextValue().trim());
        } else {
            CmbHeaderCenter.setSelectedIndex(getHFComboIndex(headerFooterTextBean.getTextValue().trim()));
        }
        
        headerFooterTextBean = pdfPageSetupBean.getHeaderRightBean();
        if(headerFooterTextBean.isCheckBoxSelected()) {
            ChkHeaderRight.setSelected(true);
            TxtHeaderRight.setText(headerFooterTextBean.getTextValue().trim());
        } else {
            CmbHeaderRight.setSelectedIndex(getHFComboIndex(headerFooterTextBean.getTextValue().trim()));
        }
        
        headerFooterTextBean = pdfPageSetupBean.getFooterLeftBean();
        if(headerFooterTextBean.isCheckBoxSelected()) {
            ChkFooterLeft.setSelected(true);
            TxtFooterLeft.setText(headerFooterTextBean.getTextValue().trim());
        } else {
            CmbFooterLeft.setSelectedIndex(getHFComboIndex(headerFooterTextBean.getTextValue().trim()));
        }
        
        headerFooterTextBean = pdfPageSetupBean.getFooterCenterBean();
        if(headerFooterTextBean.isCheckBoxSelected()) {
            ChkFooterCenter.setSelected(true);
            TxtFooterCenter.setText(headerFooterTextBean.getTextValue().trim());
        } else {
            CmbFooterCenter.setSelectedIndex(getHFComboIndex(headerFooterTextBean.getTextValue().trim()));
        }
        
        headerFooterTextBean = pdfPageSetupBean.getFooterRightBean();
        if(headerFooterTextBean.isCheckBoxSelected()) {
            ChkFooterRight.setSelected(true);
            TxtFooterRight.setText(headerFooterTextBean.getTextValue().trim());
        } else {
            CmbFooterRight.setSelectedIndex(getHFComboIndex(headerFooterTextBean.getTextValue().trim()));
        }

        ChkFirstPageHeaderFooter.setSelected(pdfPageSetupBean.isHeaderFooterOnFirstPage());//First Page Header
        TxtQuestionStart.setText(""+pdfPageSetupBean.getQuestionStartNo());//Question Start Number
        ChkQuestionBold.setSelected(pdfPageSetupBean.isQuestionBold());//Question Bold
        ChkOptionBold.setSelected(pdfPageSetupBean.isOptionBold()); //Option Bold
        ChkSolutionBold.setSelected(pdfPageSetupBean.isSolutionBold()); //Solution Bold
        ChkOptimizeLineSpace.setSelected(pdfPageSetupBean.isOptimizeLineSpace());//Optimize Line Space
        CmbFontSize.setSelectedIndex(pdfPageSetupBean.getFontSize()-1);//Font Size
        if(pdfPageSetupBean.isPageBorder()) { //Page Border
            BorderPanel.setVisible(true);
            ChkPageBorder.setSelected(true);
            CmbBorderLineWidth.setSelectedIndex(pdfPageSetupBean.getBorderWidth());//Border Width
        } else {
            BorderPanel.setVisible(false);
            ChkPageBorder.setSelected(false);
        }
        
       
        
        if(pdfPageSetupBean.isChapterList()) { //ChapterList
            ChkChapterList.setSelected(true);
        } else {
            ChkChapterList.setSelected(false);
        }
    }
     
    private String getSubString(String str) {
        if(str.length() > 30)
            return str.substring(0, 30) + "-<br/>" + str.substring(30, str.length());
        else
            return str;
    }
    
    private boolean getUpdateNextStatuss() {
        boolean returnValue = false;
        if(!checkHeaderFooterText()) {
            pageCardValue = 5;
            JOptionPane.showMessageDialog(this, "Please Enter Data Or\nUn-Select Selected Check Box.");
        }
        
        if(ChkSetLogo.isSelected() && sourceLogo == null) {
            pageCardValue = 3;
            JOptionPane.showMessageDialog(this, "Please Select Logo Image Or\nUn-Select Selected Check Box.");
        }
        addCardList();
        if(pageCardValue == 1) {
            pageSetupLayout.show(RightBodyPanel, "FirstCard");
            setButtonBorder(BtnPageFormat);
        } else if(pageCardValue == 2) {
            pageSetupLayout.show(RightBodyPanel, "SecondCard");
            setButtonBorder(BtnPageType);
        } else if(pageCardValue == 3) {
            pageSetupLayout.show(RightBodyPanel, "ThirdCard");
            setButtonBorder(BtnPageLogo);
        } else if(pageCardValue == 4) {
            pageSetupLayout.show(RightBodyPanel, "FourthCard");
            setButtonBorder(BtnPageWatermark);
        } else if(pageCardValue == 5) {
            pageSetupLayout.show(RightBodyPanel, "FifthCard");
            setButtonBorder(BtnPageHeaderFooter);
        } else if(pageCardValue == 6) {
            pageSetupLayout.show(RightBodyPanel, "SixthCard");
            setButtonBorder(BtnPageOther);
        }
        
        if(pageCardValue == 6 && pageCardList.size() == 6)
            returnValue = true;
         
        if(pageCardValue != 6)
            pageCardValue++;
        else
            pageCardValue = 1;
        
        return returnValue;
    } 
    
    private boolean getUpdateNextStatus() {
        boolean returnValue = true;
        if(!checkHeaderFooterText()) {
            pageCardValue = 5;
            returnValue = false;
            JOptionPane.showMessageDialog(this, "Please Enter Data Or\nUn-Select Selected Check Box.");
        }
        
        if(ChkSetLogo.isSelected() && (sourceLogoString == null && pdfPageSetupBean != null)) {
            if((pdfPageSetupBean.getLogoPath() != null && pdfPageSetupBean.getLogoPath().equalsIgnoreCase("")) || pdfPageSetupBean.getLogoPath() == null) {
                pageCardValue = 3;
                returnValue = false;
                JOptionPane.showMessageDialog(this, "Please Select Logo Image Or\nUn-Select Selected Check Box.");
            }
            
        }
//        addCardList();
        if(pageCardValue == 1) {
            pageSetupLayout.show(RightBodyPanel, "FirstCard");
            setButtonBorder(BtnPageFormat);
        } else if(pageCardValue == 2) {
            pageSetupLayout.show(RightBodyPanel, "SecondCard");
            setButtonBorder(BtnPageType);
        } else if(pageCardValue == 3) {
            pageSetupLayout.show(RightBodyPanel, "ThirdCard");
            setButtonBorder(BtnPageLogo);
        } else if(pageCardValue == 4) {
            pageSetupLayout.show(RightBodyPanel, "FourthCard");
            setButtonBorder(BtnPageWatermark);
        } else if(pageCardValue == 5) {
            pageSetupLayout.show(RightBodyPanel, "FifthCard");
            setButtonBorder(BtnPageHeaderFooter);
        } else if(pageCardValue == 6) {
            pageSetupLayout.show(RightBodyPanel, "SixthCard");
            setButtonBorder(BtnPageOther);
        }
        
//        if(pageCardValue == 6 && pageCardList.size() == 6)
//            returnValue = true;
//         
//        if(pageCardValue != 6)
//            pageCardValue++;
//        else
//            pageCardValue = 1;
        
        return returnValue;
    } 
    
    private void addCardList() {
        if(pageCardList == null)
            pageCardList = new ArrayList<Integer>();
        
        if(!pageCardList.contains(pageCardValue))
            pageCardList.add(pageCardValue);
        
    }
    
    private PdfPageSetupBean setsPageSetupBean() {
        PdfPageSetupBean returnBean = new PdfPageSetupBean();
        if(ChkFirstFormat.isSelected())
            returnBean.setPageFormat(0);
        else
        returnBean.setPageFormat(1);
        returnBean.setTwoColumn(ChkColumnType.isSelected());
        returnBean.setPrintYear(ChkYear.isSelected());
        if(ChkA4.isSelected()) 
            returnBean.setPaperType("a4paper");
        else if(ChkLegal.isSelected())
            returnBean.setPaperType("legalpaper");
        else 
            returnBean.setPaperType("letterpaper");
        
        returnBean.setSetImageActualsize(ChkImageActualSize.isSelected());
        
        if(ChkSetLogo.isSelected() && sourceLogoString != null) {
            returnBean.setSetLogo(true);
            returnBean.setLogoPath(sourceLogoString);
            
            if(pdfPageSetupBean != null && pdfPageSetupBean.isSetLogo()) {
                if(!sourceLogoString.equalsIgnoreCase(pdfPageSetupBean.getLogoPath().trim())) {
                    try {
                        new File(pdfPageSetupBean.getLogoPath().trim()).delete();
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                    try {
                       FileUtils.copyFile(sourceLogo, new File(sourceLogoString));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            } else if(pdfPageSetupBean != null && !pdfPageSetupBean.isSetLogo()) {
                try {
                   FileUtils.copyFile(sourceLogo, new File(sourceLogoString));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                try {
                   FileUtils.copyFile(sourceLogo, new File(sourceLogoString));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        } else if(ChkSetLogo.isSelected() && sourceLogoString == null) {
            returnBean.setLogoPath(pdfPageSetupBean.getLogoPath().trim());
            returnBean.setSetLogo(true);
        } else if(!ChkSetLogo.isSelected()) {
            returnBean.setLogoPath("");
            returnBean.setSetLogo(false);
            ChkImageActualSize.setSelected(false);
            ChkLeft.setSelected(true);
            
            if(pdfPageSetupBean != null && pdfPageSetupBean.isSetLogo()) {
                try {
                    new File(pdfPageSetupBean.getLogoPath().trim()).delete();
                } catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        } else {
            returnBean.setLogoPath("");
            returnBean.setSetLogo(false);
        }
        
        if (ChkLeft.isSelected()) {
            returnBean.setLogoPosition(0);
        }
        if (ChkRight.isSelected()) {
            returnBean.setLogoPosition(1);
        }
        if (ChkTop.isSelected()) {
            returnBean.setLogoPosition(2);
        }
        if (ChkBottom.isSelected()) {
            returnBean.setLogoPosition(3);
        }

        int WaterMarkInfo = 0;
        int WaterMarkTextGrayScale = 0;
        int WaterMarkScale = 0;
        int WaterMarkAngle = 0;
        String WaterMarkLogoPath = "";
        if (ChkWaterMarkAsInstiName.isSelected()) {
            if(waterMarkImagePath != null) {
                new File(processPath + "/WaterMark-Logo/WaterMark.png").delete();
            }
            WaterMarkInfo = 1;
            WaterMarkTextGrayScale = CmbWaterMarkTextGrayScale.getSelectedIndex();
            String str = CmbWaterMarkScale.getSelectedItem().toString();
            WaterMarkScale = Integer.parseInt(str);
            WaterMarkAngle = WaterMarkAngleSlider.getValue();
        } else if (ChkWaterMarkAsInstiLogo.isSelected()) {
            WaterMarkInfo = 2;
            String str = CmbWaterMarkScale.getSelectedItem().toString();
            WaterMarkScale = Integer.parseInt(str);
            WaterMarkAngle = WaterMarkAngleSlider.getValue();
            WaterMarkLogoPath = processPath + "/WaterMark-Logo/WaterMark.png";
            if(waterMarkImagePath != null && waterMarkImageFile != null) {
                File dest = new File(WaterMarkLogoPath);
                dest.delete();
                try {
                    FileUtils.copyFile(waterMarkImageFile, dest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if(waterMarkImageFile != null) {
                try {
                    FileUtils.copyFile(waterMarkImageFile, new File(WaterMarkLogoPath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(waterMarkImageFile == null && waterMarkImagePath == null) {
                WaterMarkLogoPath = "";
                WaterMarkInfo = 0;
                WaterMarkTextGrayScale = 0;
                WaterMarkScale = 0;
                WaterMarkAngle = 0;
            }
        } else {
            if(waterMarkImagePath != null) {
                new File(processPath + "/WaterMark-Logo/WaterMark.png").delete();
            }
        } 
        returnBean.setWaterMarkInfo(WaterMarkInfo);
        returnBean.setWaterMarkTextGrayScale(WaterMarkTextGrayScale);
        returnBean.setWaterMarkLogoPath(WaterMarkLogoPath);
        returnBean.setWaterMarkScale(WaterMarkScale);
        returnBean.setWaterMarkAngle(WaterMarkAngle);
        
        HeaderFooterTextBean headerFooterTextBean = null;
        
        headerFooterTextBean = new HeaderFooterTextBean();
        if(ChkHeaderLeft.isSelected()) {
            headerFooterTextBean.setCheckBoxSelected(true);
            headerFooterTextBean.setTextValue(TxtHeaderLeft.getText().trim());
        } else {
            headerFooterTextBean.setCheckBoxSelected(false);
            headerFooterTextBean.setTextValue(CmbHeaderLeft.getSelectedItem().toString().trim());
        }
        returnBean.setHeaderLeftBean(headerFooterTextBean);
        
        headerFooterTextBean = new HeaderFooterTextBean();
        if(ChkHeaderCenter.isSelected()) {
            headerFooterTextBean.setCheckBoxSelected(true);
            headerFooterTextBean.setTextValue(TxtHeaderCenter.getText().trim());
        } else {
            headerFooterTextBean.setCheckBoxSelected(false);
            headerFooterTextBean.setTextValue(CmbHeaderCenter.getSelectedItem().toString().trim());
        }
        returnBean.setHeaderCenterBean(headerFooterTextBean);
        
        headerFooterTextBean = new HeaderFooterTextBean();
        if(ChkHeaderRight.isSelected()) {
            headerFooterTextBean.setCheckBoxSelected(true);
            headerFooterTextBean.setTextValue(TxtHeaderRight.getText().trim());
        } else {
            headerFooterTextBean.setCheckBoxSelected(false);
            headerFooterTextBean.setTextValue(CmbHeaderRight.getSelectedItem().toString().trim());
        }
        returnBean.setHeaderRightBean(headerFooterTextBean);
        
        headerFooterTextBean = new HeaderFooterTextBean();
        if(ChkFooterLeft.isSelected()) {
            headerFooterTextBean.setCheckBoxSelected(true);
            headerFooterTextBean.setTextValue(TxtFooterLeft.getText().trim());
        } else {
            headerFooterTextBean.setCheckBoxSelected(false);
            headerFooterTextBean.setTextValue(CmbFooterLeft.getSelectedItem().toString().trim());
        }
        returnBean.setFooterLeftBean(headerFooterTextBean);
        
        headerFooterTextBean = new HeaderFooterTextBean();
        if(ChkFooterCenter.isSelected()) {
            headerFooterTextBean.setCheckBoxSelected(true);
            headerFooterTextBean.setTextValue(TxtFooterCenter.getText().trim());
        } else {
            headerFooterTextBean.setCheckBoxSelected(false);
            headerFooterTextBean.setTextValue(CmbFooterCenter.getSelectedItem().toString().trim());
        }
        returnBean.setFooterCenterBean(headerFooterTextBean);
        
        headerFooterTextBean = new HeaderFooterTextBean();
        if(ChkFooterRight.isSelected()) {
            headerFooterTextBean.setCheckBoxSelected(true);
            headerFooterTextBean.setTextValue(TxtFooterRight.getText().trim());
        } else {
            headerFooterTextBean.setCheckBoxSelected(false);
            headerFooterTextBean.setTextValue(CmbFooterRight.getSelectedItem().toString().trim());
        }
        returnBean.setFooterRightBean(headerFooterTextBean);
        returnBean.setHeaderFooterOnFirstPage(ChkFirstPageHeaderFooter.isSelected());
        if(!TxtQuestionStart.getText().isEmpty())
            returnBean.setQuestionStartNo(Integer.parseInt(TxtQuestionStart.getText()));
        else
            returnBean.setQuestionStartNo(1);
        returnBean.setQuestionBold(ChkQuestionBold.isSelected());
        returnBean.setOptionBold(ChkOptionBold.isSelected());
        returnBean.setSolutionBold(ChkSolutionBold.isSelected());
        returnBean.setPageBorder(ChkPageBorder.isSelected());       
        returnBean.setBorderWidth(CmbBorderLineWidth.getSelectedIndex());
        returnBean.setChapterList(ChkChapterList.isSelected());
        returnBean.setOptimizeLineSpace(ChkOptimizeLineSpace.isSelected());
        returnBean.setFontSize(CmbFontSize.getSelectedIndex()+1);
        return returnBean;
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        HeaderPanel = new javax.swing.JPanel();
        LblBodyHeader = new javax.swing.JLabel();
        LeftBodyPanel = new javax.swing.JPanel();
        BtnPageFormat = new javax.swing.JButton();
        BtnPageType = new javax.swing.JButton();
        BtnPageLogo = new javax.swing.JButton();
        BtnPageWatermark = new javax.swing.JButton();
        BtnPageHeaderFooter = new javax.swing.JButton();
        BtnPageOther = new javax.swing.JButton();
        RightBodyPanel = new javax.swing.JPanel();
        FormatPanel = new javax.swing.JPanel();
        ChkFirstFormat = new javax.swing.JCheckBox();
        ChkSecondFormat = new javax.swing.JCheckBox();
        LblFirstFormat = new javax.swing.JLabel();
        LblSecondFormat = new javax.swing.JLabel();
        ChkColumnType = new javax.swing.JCheckBox();
        ChkYear = new javax.swing.JCheckBox();
        PaperTypePanel = new javax.swing.JPanel();
        LblPaperTypeFirstHeader = new javax.swing.JLabel();
        LblPaperTypeDimention = new javax.swing.JLabel();
        LblPaperTypeSecondHeader = new javax.swing.JLabel();
        ChkA4 = new javax.swing.JCheckBox();
        LblA4 = new javax.swing.JLabel();
        ChkLegal = new javax.swing.JCheckBox();
        LblLegal = new javax.swing.JLabel();
        ChkLetter = new javax.swing.JCheckBox();
        LblLetter = new javax.swing.JLabel();
        LogoPanel = new javax.swing.JPanel();
        ChkSetLogo = new javax.swing.JCheckBox();
        LogoSelectPanel = new javax.swing.JPanel();
        BtnSelectLogo = new javax.swing.JButton();
        LblLogoPath = new javax.swing.JLabel();
        LblLogoWidth = new javax.swing.JLabel();
        LblLogoNote = new javax.swing.JLabel();
        LblLogoView = new javax.swing.JLabel();
        LogoPositionPanel = new javax.swing.JPanel();
        ChkImageActualSize = new javax.swing.JCheckBox();
        LblLogoPosition = new javax.swing.JLabel();
        ChkLeft = new javax.swing.JCheckBox();
        ChkRight = new javax.swing.JCheckBox();
        ChkTop = new javax.swing.JCheckBox();
        ChkBottom = new javax.swing.JCheckBox();
        WaterMarkPanel = new javax.swing.JPanel();
        ChkWaterMarkAsInstiName = new javax.swing.JCheckBox();
        ChkWaterMarkAsInstiLogo = new javax.swing.JCheckBox();
        WaterMarkSettingsPanel = new javax.swing.JPanel();
        WaterMarkScalePanel = new javax.swing.JPanel();
        BtnBrowse = new javax.swing.JButton();
        LblWaterMarkScale = new javax.swing.JLabel();
        CmbWaterMarkScale = new javax.swing.JComboBox();
        LblWaterMarkAngel = new javax.swing.JLabel();
        WaterMarkAngleSlider = new javax.swing.JSlider();
        LblWaterMarkView = new javax.swing.JLabel();
        WaterMarkCardPanel = new javax.swing.JPanel();
        WaterMarkTextGrayScalePanel = new javax.swing.JPanel();
        LblWaterMarkTextGrayScale = new javax.swing.JLabel();
        CmbWaterMarkTextGrayScale = new javax.swing.JComboBox();
        WaterLogoPathPanel = new javax.swing.JPanel();
        LblWaterLogoPath = new javax.swing.JLabel();
        HeaderFooterPanel = new javax.swing.JPanel();
        LblHeaderFooterNote = new javax.swing.JLabel();
        LblHeader = new javax.swing.JLabel();
        LblFooter = new javax.swing.JLabel();
        ChkFirstPageHeaderFooter = new javax.swing.JCheckBox();
        BodyHeaderPanel = new javax.swing.JPanel();
        ChkHeaderLeft = new javax.swing.JCheckBox();
        PanelHeaderOne = new javax.swing.JPanel();
        HeaderLeftAutoPanel = new javax.swing.JPanel();
        CmbHeaderLeft = new javax.swing.JComboBox();
        HeaderLeftManualPanel = new javax.swing.JPanel();
        TxtHeaderLeft = new javax.swing.JTextField();
        ChkHeaderCenter = new javax.swing.JCheckBox();
        PanelHeaderTwo = new javax.swing.JPanel();
        HeaderCenterAutoPanel = new javax.swing.JPanel();
        CmbHeaderCenter = new javax.swing.JComboBox();
        HeaderCenterManualPanel = new javax.swing.JPanel();
        TxtHeaderCenter = new javax.swing.JTextField();
        ChkHeaderRight = new javax.swing.JCheckBox();
        PanelHeaderThree = new javax.swing.JPanel();
        HeaderRightAutoPanel = new javax.swing.JPanel();
        CmbHeaderRight = new javax.swing.JComboBox();
        HeaderRightManualPanel = new javax.swing.JPanel();
        TxtHeaderRight = new javax.swing.JTextField();
        BodyFooterPanel = new javax.swing.JPanel();
        ChkFooterLeft = new javax.swing.JCheckBox();
        PanelFooterOne = new javax.swing.JPanel();
        FooterLeftAutoPanel = new javax.swing.JPanel();
        CmbFooterLeft = new javax.swing.JComboBox();
        FooterLeftManualPanel = new javax.swing.JPanel();
        TxtFooterLeft = new javax.swing.JTextField();
        ChkFooterCenter = new javax.swing.JCheckBox();
        PanelFooterTwo = new javax.swing.JPanel();
        FooterCenterAutoPanel = new javax.swing.JPanel();
        CmbFooterCenter = new javax.swing.JComboBox();
        FooterCenterManualPanel = new javax.swing.JPanel();
        TxtFooterCenter = new javax.swing.JTextField();
        ChkFooterRight = new javax.swing.JCheckBox();
        PanelFooterThree = new javax.swing.JPanel();
        FooterRightAutoPanel = new javax.swing.JPanel();
        CmbFooterRight = new javax.swing.JComboBox();
        FooterRightManualPanel = new javax.swing.JPanel();
        TxtFooterRight = new javax.swing.JTextField();
        OtherSettingsPanel = new javax.swing.JPanel();
        LblQuestionStart = new javax.swing.JLabel();
        TxtQuestionStart = new javax.swing.JTextField();
        ChkQuestionBold = new javax.swing.JCheckBox();
        ChkOptionBold = new javax.swing.JCheckBox();
        ChkPageBorder = new javax.swing.JCheckBox();
        BorderPanel = new javax.swing.JPanel();
        LblBorderWidth = new javax.swing.JLabel();
        CmbBorderLineWidth = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        ChkSolutionBold = new javax.swing.JCheckBox();
        ChkChapterList = new javax.swing.JCheckBox();
        ChkOptimizeLineSpace = new javax.swing.JCheckBox();
        FontPanel = new javax.swing.JPanel();
        LblFontSize = new javax.swing.JLabel();
        CmbFontSize = new javax.swing.JComboBox();
        LblQuestionStart1 = new javax.swing.JLabel();
        FooterPanel = new javax.swing.JPanel();
        BtnCancel = new javax.swing.JButton();
        BtnUpdateSettings = new javax.swing.JButton();
        BtnPreviousSettings = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        HeaderPanel.setBackground(new java.awt.Color(11, 45, 55));

        LblBodyHeader.setBackground(new java.awt.Color(0, 102, 102));
        LblBodyHeader.setFont(new java.awt.Font("Calibri", 0, 30)); // NOI18N
        LblBodyHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblBodyHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBodyHeader.setText("Page Settings");
        LblBodyHeader.setOpaque(true);

        javax.swing.GroupLayout HeaderPanelLayout = new javax.swing.GroupLayout(HeaderPanel);
        HeaderPanel.setLayout(HeaderPanelLayout);
        HeaderPanelLayout.setHorizontalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblBodyHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        HeaderPanelLayout.setVerticalGroup(
            HeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblBodyHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
        );

        LeftBodyPanel.setBackground(new java.awt.Color(11, 45, 55));

        BtnPageFormat.setBackground(new java.awt.Color(11, 45, 55));
        BtnPageFormat.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnPageFormat.setForeground(new java.awt.Color(255, 255, 255));
        BtnPageFormat.setText("Set Paper Format");
        BtnPageFormat.setBorderPainted(false);
        BtnPageFormat.setFocusPainted(false);
        BtnPageFormat.setMaximumSize(new java.awt.Dimension(76, 20));
        BtnPageFormat.setMinimumSize(new java.awt.Dimension(76, 20));
        BtnPageFormat.setPreferredSize(new java.awt.Dimension(76, 20));
        BtnPageFormat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPageFormatMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPageFormatMouseExited(evt);
            }
        });
        BtnPageFormat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPageFormatActionPerformed(evt);
            }
        });

        BtnPageType.setBackground(new java.awt.Color(11, 45, 55));
        BtnPageType.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnPageType.setForeground(new java.awt.Color(255, 255, 255));
        BtnPageType.setText("Paper Type");
        BtnPageType.setBorder(null);
        BtnPageType.setFocusPainted(false);
        BtnPageType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPageTypeActionPerformed(evt);
            }
        });

        BtnPageLogo.setBackground(new java.awt.Color(11, 45, 55));
        BtnPageLogo.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnPageLogo.setForeground(new java.awt.Color(255, 255, 255));
        BtnPageLogo.setText("Set Your Logo");
        BtnPageLogo.setBorderPainted(false);
        BtnPageLogo.setFocusPainted(false);
        BtnPageLogo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPageLogoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPageLogoMouseExited(evt);
            }
        });
        BtnPageLogo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPageLogoActionPerformed(evt);
            }
        });

        BtnPageWatermark.setBackground(new java.awt.Color(11, 45, 55));
        BtnPageWatermark.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnPageWatermark.setForeground(new java.awt.Color(255, 255, 255));
        BtnPageWatermark.setText("Watermark");
        BtnPageWatermark.setBorderPainted(false);
        BtnPageWatermark.setFocusPainted(false);
        BtnPageWatermark.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPageWatermarkMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPageWatermarkMouseExited(evt);
            }
        });
        BtnPageWatermark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPageWatermarkActionPerformed(evt);
            }
        });

        BtnPageHeaderFooter.setBackground(new java.awt.Color(11, 45, 55));
        BtnPageHeaderFooter.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnPageHeaderFooter.setForeground(new java.awt.Color(255, 255, 255));
        BtnPageHeaderFooter.setText("Header Footer");
        BtnPageHeaderFooter.setBorderPainted(false);
        BtnPageHeaderFooter.setFocusPainted(false);
        BtnPageHeaderFooter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPageHeaderFooterMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPageHeaderFooterMouseExited(evt);
            }
        });
        BtnPageHeaderFooter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPageHeaderFooterActionPerformed(evt);
            }
        });

        BtnPageOther.setBackground(new java.awt.Color(11, 45, 55));
        BtnPageOther.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        BtnPageOther.setForeground(new java.awt.Color(255, 255, 255));
        BtnPageOther.setText("Other");
        BtnPageOther.setBorderPainted(false);
        BtnPageOther.setFocusPainted(false);
        BtnPageOther.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BtnPageOtherMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                BtnPageOtherMouseExited(evt);
            }
        });
        BtnPageOther.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPageOtherActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LeftBodyPanelLayout = new javax.swing.GroupLayout(LeftBodyPanel);
        LeftBodyPanel.setLayout(LeftBodyPanelLayout);
        LeftBodyPanelLayout.setHorizontalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BtnPageLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BtnPageType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BtnPageWatermark, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
            .addComponent(BtnPageHeaderFooter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BtnPageOther, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BtnPageFormat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        LeftBodyPanelLayout.setVerticalGroup(
            LeftBodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LeftBodyPanelLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(BtnPageFormat, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(BtnPageType, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(BtnPageLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnPageWatermark, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(BtnPageHeaderFooter, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(BtnPageOther, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17))
        );

        RightBodyPanel.setPreferredSize(new java.awt.Dimension(665, 480));
        RightBodyPanel.setLayout(new java.awt.CardLayout());

        FormatPanel.setBackground(new java.awt.Color(0, 102, 102));

        ChkFirstFormat.setBackground(new java.awt.Color(11, 45, 55));
        ChkFirstFormat.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkFirstFormat.setForeground(new java.awt.Color(255, 255, 255));
        ChkFirstFormat.setSelected(true);
        ChkFirstFormat.setText("Format 1 - (Standard)");
        ChkFirstFormat.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkFirstFormatItemStateChanged(evt);
            }
        });

        ChkSecondFormat.setBackground(new java.awt.Color(11, 45, 55));
        ChkSecondFormat.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkSecondFormat.setForeground(new java.awt.Color(255, 255, 255));
        ChkSecondFormat.setText("Format 2 – (Basic)");
        ChkSecondFormat.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkSecondFormatItemStateChanged(evt);
            }
        });

        LblFirstFormat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/FormatFirst.png"))); // NOI18N
        LblFirstFormat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        LblSecondFormat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/FormatSecond.png"))); // NOI18N
        LblSecondFormat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        LblSecondFormat.setEnabled(false);

        ChkColumnType.setBackground(new java.awt.Color(11, 45, 55));
        ChkColumnType.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkColumnType.setForeground(new java.awt.Color(255, 255, 255));
        ChkColumnType.setSelected(true);
        ChkColumnType.setText("Two Column Type Paper");

        ChkYear.setBackground(new java.awt.Color(11, 45, 55));
        ChkYear.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        ChkYear.setForeground(new java.awt.Color(255, 255, 255));
        ChkYear.setText("Print Year On Question Paper");

        javax.swing.GroupLayout FormatPanelLayout = new javax.swing.GroupLayout(FormatPanel);
        FormatPanel.setLayout(FormatPanelLayout);
        FormatPanelLayout.setHorizontalGroup(
            FormatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormatPanelLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(FormatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblFirstFormat)
                    .addGroup(FormatPanelLayout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(ChkFirstFormat)))
                .addGap(31, 31, 31)
                .addGroup(FormatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormatPanelLayout.createSequentialGroup()
                        .addComponent(LblSecondFormat)
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormatPanelLayout.createSequentialGroup()
                        .addComponent(ChkSecondFormat)
                        .addGap(96, 96, 96))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormatPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ChkColumnType)
                .addGap(98, 98, 98)
                .addComponent(ChkYear)
                .addGap(68, 68, 68))
        );
        FormatPanelLayout.setVerticalGroup(
            FormatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FormatPanelLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(FormatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ChkFirstFormat)
                    .addComponent(ChkSecondFormat))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(FormatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LblFirstFormat, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                    .addComponent(LblSecondFormat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26)
                .addGroup(FormatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ChkColumnType)
                    .addComponent(ChkYear))
                .addGap(47, 47, 47))
        );

        RightBodyPanel.add(FormatPanel, "FirstCard");

        PaperTypePanel.setBackground(new java.awt.Color(0, 102, 102));
        PaperTypePanel.setPreferredSize(new java.awt.Dimension(665, 480));

        LblPaperTypeFirstHeader.setBackground(new java.awt.Color(11, 45, 55));
        LblPaperTypeFirstHeader.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        LblPaperTypeFirstHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblPaperTypeFirstHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPaperTypeFirstHeader.setText("Paper Type with Dimenstions");
        LblPaperTypeFirstHeader.setOpaque(true);

        LblPaperTypeDimention.setBackground(new java.awt.Color(255, 255, 255));
        LblPaperTypeDimention.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        LblPaperTypeDimention.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        LblPaperTypeSecondHeader.setBackground(new java.awt.Color(11, 45, 55));
        LblPaperTypeSecondHeader.setFont(new java.awt.Font("Corbel", 0, 16)); // NOI18N
        LblPaperTypeSecondHeader.setForeground(new java.awt.Color(255, 255, 255));
        LblPaperTypeSecondHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPaperTypeSecondHeader.setText("Select Question Paper Type");
        LblPaperTypeSecondHeader.setOpaque(true);

        ChkA4.setBackground(new java.awt.Color(0, 102, 102));
        ChkA4.setSelected(true);
        ChkA4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkA4.setIconTextGap(5);
        ChkA4.setInheritsPopupMenu(true);
        ChkA4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ChkA4.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ChkA4StateChanged(evt);
            }
        });
        ChkA4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkA4ActionPerformed(evt);
            }
        });

        LblA4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/a4.png"))); // NOI18N
        LblA4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblA4MouseClicked(evt);
            }
        });

        ChkLegal.setBackground(new java.awt.Color(0, 102, 102));
        ChkLegal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkLegal.setIconTextGap(5);
        ChkLegal.setInheritsPopupMenu(true);
        ChkLegal.setOpaque(false);
        ChkLegal.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ChkLegal.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ChkLegalStateChanged(evt);
            }
        });

        LblLegal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/legal.png"))); // NOI18N
        LblLegal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblLegalMouseClicked(evt);
            }
        });

        ChkLetter.setBackground(new java.awt.Color(0, 102, 102));
        ChkLetter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkLetter.setIconTextGap(5);
        ChkLetter.setInheritsPopupMenu(true);
        ChkLetter.setOpaque(false);
        ChkLetter.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ChkLetter.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                ChkLetterStateChanged(evt);
            }
        });

        LblLetter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/letter.png"))); // NOI18N
        LblLetter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                LblLetterMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout PaperTypePanelLayout = new javax.swing.GroupLayout(PaperTypePanel);
        PaperTypePanel.setLayout(PaperTypePanelLayout);
        PaperTypePanelLayout.setHorizontalGroup(
            PaperTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LblPaperTypeFirstHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(LblPaperTypeDimention, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(LblPaperTypeSecondHeader, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PaperTypePanelLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(ChkA4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblA4)
                .addGap(36, 36, 36)
                .addComponent(ChkLegal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblLegal)
                .addGap(39, 39, 39)
                .addComponent(ChkLetter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblLetter)
                .addGap(33, 33, 33))
        );
        PaperTypePanelLayout.setVerticalGroup(
            PaperTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PaperTypePanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(LblPaperTypeFirstHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(LblPaperTypeDimention, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(LblPaperTypeSecondHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(PaperTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PaperTypePanelLayout.createSequentialGroup()
                        .addGap(111, 111, 111)
                        .addGroup(PaperTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(ChkLegal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ChkA4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ChkLetter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(98, 98, 98))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PaperTypePanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PaperTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblLegal)
                            .addComponent(LblA4)
                            .addComponent(LblLetter))
                        .addContainerGap())))
        );

        RightBodyPanel.add(PaperTypePanel, "SecondCard");

        LogoPanel.setBackground(new java.awt.Color(0, 102, 102));
        LogoPanel.setPreferredSize(new java.awt.Dimension(665, 480));
        LogoPanel.setRequestFocusEnabled(false);

        ChkSetLogo.setBackground(new java.awt.Color(208, 87, 96));
        ChkSetLogo.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkSetLogo.setForeground(new java.awt.Color(255, 255, 255));
        ChkSetLogo.setText("SetLogo");
        ChkSetLogo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkSetLogoItemStateChanged(evt);
            }
        });

        LogoSelectPanel.setBackground(new java.awt.Color(0, 102, 102));

        BtnSelectLogo.setBackground(new java.awt.Color(208, 87, 96));
        BtnSelectLogo.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        BtnSelectLogo.setForeground(new java.awt.Color(255, 255, 255));
        BtnSelectLogo.setText("Select image");
        BtnSelectLogo.setBorderPainted(false);
        BtnSelectLogo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSelectLogoActionPerformed(evt);
            }
        });

        LblLogoPath.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        LblLogoPath.setForeground(new java.awt.Color(255, 255, 255));

        LblLogoWidth.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        LblLogoWidth.setForeground(new java.awt.Color(255, 255, 255));

        LblLogoNote.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        LblLogoNote.setForeground(new java.awt.Color(255, 255, 255));

        LblLogoView.setBackground(new java.awt.Color(0, 102, 102));
        LblLogoView.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblLogoView.setForeground(new java.awt.Color(128, 128, 255));
        LblLogoView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblLogoView.setOpaque(true);

        LogoPositionPanel.setBackground(new java.awt.Color(0, 102, 102));

        ChkImageActualSize.setBackground(new java.awt.Color(208, 87, 96));
        ChkImageActualSize.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkImageActualSize.setForeground(new java.awt.Color(255, 255, 255));
        ChkImageActualSize.setText("Set Image Actual Size");
        ChkImageActualSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkImageActualSizeActionPerformed(evt);
            }
        });

        LblLogoPosition.setBackground(new java.awt.Color(208, 87, 96));
        LblLogoPosition.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblLogoPosition.setForeground(new java.awt.Color(255, 255, 255));
        LblLogoPosition.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblLogoPosition.setText("Set Logo Position");
        LblLogoPosition.setOpaque(true);

        ChkLeft.setBackground(new java.awt.Color(208, 87, 96));
        ChkLeft.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkLeft.setForeground(new java.awt.Color(255, 255, 255));
        ChkLeft.setText("Left");
        ChkLeft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkLeftActionPerformed(evt);
            }
        });

        ChkRight.setBackground(new java.awt.Color(208, 87, 96));
        ChkRight.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkRight.setForeground(new java.awt.Color(255, 255, 255));
        ChkRight.setText("Right");
        ChkRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkRightActionPerformed(evt);
            }
        });

        ChkTop.setBackground(new java.awt.Color(208, 87, 96));
        ChkTop.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkTop.setForeground(new java.awt.Color(255, 255, 255));
        ChkTop.setText("Top");
        ChkTop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkTopActionPerformed(evt);
            }
        });

        ChkBottom.setBackground(new java.awt.Color(208, 87, 96));
        ChkBottom.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkBottom.setForeground(new java.awt.Color(255, 255, 255));
        ChkBottom.setText("Bottom");
        ChkBottom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkBottomActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LogoPositionPanelLayout = new javax.swing.GroupLayout(LogoPositionPanel);
        LogoPositionPanel.setLayout(LogoPositionPanelLayout);
        LogoPositionPanelLayout.setHorizontalGroup(
            LogoPositionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogoPositionPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(LogoPositionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ChkImageActualSize)
                    .addComponent(LblLogoPosition, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkRight, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkTop, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(ChkBottom, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        LogoPositionPanelLayout.setVerticalGroup(
            LogoPositionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogoPositionPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(ChkImageActualSize)
                .addGap(10, 10, 10)
                .addGroup(LogoPositionPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblLogoPosition, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkLeft)
                    .addComponent(ChkRight)
                    .addComponent(ChkTop)
                    .addComponent(ChkBottom))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout LogoSelectPanelLayout = new javax.swing.GroupLayout(LogoSelectPanel);
        LogoSelectPanel.setLayout(LogoSelectPanelLayout);
        LogoSelectPanelLayout.setHorizontalGroup(
            LogoSelectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogoSelectPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LogoSelectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LogoPositionPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblLogoView, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LogoSelectPanelLayout.createSequentialGroup()
                        .addComponent(BtnSelectLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(LogoSelectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblLogoNote, javax.swing.GroupLayout.DEFAULT_SIZE, 446, Short.MAX_VALUE)
                            .addComponent(LblLogoWidth, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LblLogoPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        LogoSelectPanelLayout.setVerticalGroup(
            LogoSelectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogoSelectPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LogoSelectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BtnSelectLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblLogoPath, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(LblLogoWidth, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(LblLogoNote, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(LogoPositionPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblLogoView, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout LogoPanelLayout = new javax.swing.GroupLayout(LogoPanel);
        LogoPanel.setLayout(LogoPanelLayout);
        LogoPanelLayout.setHorizontalGroup(
            LogoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogoPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(LogoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LogoSelectPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ChkSetLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        LogoPanelLayout.setVerticalGroup(
            LogoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogoPanelLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(ChkSetLogo)
                .addGap(10, 10, 10)
                .addComponent(LogoSelectPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        RightBodyPanel.add(LogoPanel, "ThirdCard");

        WaterMarkPanel.setBackground(new java.awt.Color(0, 102, 102));
        WaterMarkPanel.setPreferredSize(new java.awt.Dimension(665, 480));

        ChkWaterMarkAsInstiName.setBackground(new java.awt.Color(208, 87, 96));
        ChkWaterMarkAsInstiName.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkWaterMarkAsInstiName.setForeground(new java.awt.Color(255, 255, 255));
        ChkWaterMarkAsInstiName.setText("WaterMark As Institute Name");
        ChkWaterMarkAsInstiName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ChkWaterMarkAsInstiNameMouseClicked(evt);
            }
        });

        ChkWaterMarkAsInstiLogo.setBackground(new java.awt.Color(208, 87, 96));
        ChkWaterMarkAsInstiLogo.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkWaterMarkAsInstiLogo.setForeground(new java.awt.Color(255, 255, 255));
        ChkWaterMarkAsInstiLogo.setText("WaterMark As Institute Logo");
        ChkWaterMarkAsInstiLogo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ChkWaterMarkAsInstiLogoMouseClicked(evt);
            }
        });

        WaterMarkSettingsPanel.setBackground(new java.awt.Color(0, 102, 102));

        WaterMarkScalePanel.setMaximumSize(new java.awt.Dimension(276, 47));
        WaterMarkScalePanel.setMinimumSize(new java.awt.Dimension(276, 47));

        BtnBrowse.setBackground(new java.awt.Color(208, 87, 96));
        BtnBrowse.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        BtnBrowse.setForeground(new java.awt.Color(255, 255, 255));
        BtnBrowse.setText("Browse");
        BtnBrowse.setBorderPainted(false);
        BtnBrowse.setMaximumSize(new java.awt.Dimension(81, 23));
        BtnBrowse.setMinimumSize(new java.awt.Dimension(81, 23));
        BtnBrowse.setPreferredSize(new java.awt.Dimension(81, 24));
        BtnBrowse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnBrowseActionPerformed(evt);
            }
        });

        LblWaterMarkScale.setBackground(new java.awt.Color(208, 87, 96));
        LblWaterMarkScale.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblWaterMarkScale.setForeground(new java.awt.Color(255, 255, 255));
        LblWaterMarkScale.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblWaterMarkScale.setText("WaterMark Scale");
        LblWaterMarkScale.setMaximumSize(new java.awt.Dimension(121, 25));
        LblWaterMarkScale.setMinimumSize(new java.awt.Dimension(121, 25));
        LblWaterMarkScale.setOpaque(true);
        LblWaterMarkScale.setPreferredSize(new java.awt.Dimension(121, 25));

        CmbWaterMarkScale.setBackground(new java.awt.Color(208, 87, 96));
        CmbWaterMarkScale.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbWaterMarkScale.setForeground(new java.awt.Color(255, 255, 255));
        CmbWaterMarkScale.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        CmbWaterMarkScale.setName(""); // NOI18N

        javax.swing.GroupLayout WaterMarkScalePanelLayout = new javax.swing.GroupLayout(WaterMarkScalePanel);
        WaterMarkScalePanel.setLayout(WaterMarkScalePanelLayout);
        WaterMarkScalePanelLayout.setHorizontalGroup(
            WaterMarkScalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, WaterMarkScalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BtnBrowse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblWaterMarkScale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbWaterMarkScale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        WaterMarkScalePanelLayout.setVerticalGroup(
            WaterMarkScalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WaterMarkScalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WaterMarkScalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblWaterMarkScale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbWaterMarkScale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(WaterMarkScalePanelLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(BtnBrowse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        LblWaterMarkAngel.setBackground(new java.awt.Color(208, 87, 96));
        LblWaterMarkAngel.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblWaterMarkAngel.setForeground(new java.awt.Color(255, 255, 255));
        LblWaterMarkAngel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblWaterMarkAngel.setText("Set WaterMark Angle");
        LblWaterMarkAngel.setOpaque(true);

        WaterMarkAngleSlider.setMajorTickSpacing(45);
        WaterMarkAngleSlider.setMaximum(360);
        WaterMarkAngleSlider.setMinorTickSpacing(45);
        WaterMarkAngleSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        WaterMarkAngleSlider.setPaintLabels(true);
        WaterMarkAngleSlider.setValue(0);
        WaterMarkAngleSlider.setBackground(new java.awt.Color(0, 102, 102));
        WaterMarkAngleSlider.setForeground(new java.awt.Color(255, 255, 255));
        WaterMarkAngleSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                WaterMarkAngleSliderStateChanged(evt);
            }
        });

        LblWaterMarkView.setFont(new java.awt.Font("Serif", 1, 24)); // NOI18N
        LblWaterMarkView.setForeground(new java.awt.Color(255, 255, 255));
        LblWaterMarkView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        WaterMarkCardPanel.setBackground(new java.awt.Color(0, 102, 102));
        WaterMarkCardPanel.setLayout(new java.awt.CardLayout());

        LblWaterMarkTextGrayScale.setBackground(new java.awt.Color(208, 87, 96));
        LblWaterMarkTextGrayScale.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblWaterMarkTextGrayScale.setForeground(new java.awt.Color(255, 255, 255));
        LblWaterMarkTextGrayScale.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblWaterMarkTextGrayScale.setText("WaterMark Text Gray Scale");
        LblWaterMarkTextGrayScale.setOpaque(true);

        CmbWaterMarkTextGrayScale.setBackground(new java.awt.Color(208, 87, 96));
        CmbWaterMarkTextGrayScale.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbWaterMarkTextGrayScale.setForeground(new java.awt.Color(255, 255, 255));
        CmbWaterMarkTextGrayScale.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0" }));
        CmbWaterMarkTextGrayScale.setSelectedIndex(6);
        CmbWaterMarkTextGrayScale.setToolTipText("");
        CmbWaterMarkTextGrayScale.setName(""); // NOI18N

        javax.swing.GroupLayout WaterMarkTextGrayScalePanelLayout = new javax.swing.GroupLayout(WaterMarkTextGrayScalePanel);
        WaterMarkTextGrayScalePanel.setLayout(WaterMarkTextGrayScalePanelLayout);
        WaterMarkTextGrayScalePanelLayout.setHorizontalGroup(
            WaterMarkTextGrayScalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WaterMarkTextGrayScalePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblWaterMarkTextGrayScale, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CmbWaterMarkTextGrayScale, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        WaterMarkTextGrayScalePanelLayout.setVerticalGroup(
            WaterMarkTextGrayScalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, WaterMarkTextGrayScalePanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(WaterMarkTextGrayScalePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblWaterMarkTextGrayScale, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbWaterMarkTextGrayScale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        WaterMarkCardPanel.add(WaterMarkTextGrayScalePanel, "TextGrayCard");

        WaterLogoPathPanel.setBackground(new java.awt.Color(0, 102, 102));

        LblWaterLogoPath.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblWaterLogoPath.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout WaterLogoPathPanelLayout = new javax.swing.GroupLayout(WaterLogoPathPanel);
        WaterLogoPathPanel.setLayout(WaterLogoPathPanelLayout);
        WaterLogoPathPanelLayout.setHorizontalGroup(
            WaterLogoPathPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 241, Short.MAX_VALUE)
            .addGroup(WaterLogoPathPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(WaterLogoPathPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(LblWaterLogoPath, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        WaterLogoPathPanelLayout.setVerticalGroup(
            WaterLogoPathPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 47, Short.MAX_VALUE)
            .addGroup(WaterLogoPathPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(WaterLogoPathPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(LblWaterLogoPath, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        WaterMarkCardPanel.add(WaterLogoPathPanel, "WaterLogoCard");

        javax.swing.GroupLayout WaterMarkSettingsPanelLayout = new javax.swing.GroupLayout(WaterMarkSettingsPanel);
        WaterMarkSettingsPanel.setLayout(WaterMarkSettingsPanelLayout);
        WaterMarkSettingsPanelLayout.setHorizontalGroup(
            WaterMarkSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WaterMarkSettingsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WaterMarkSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(WaterMarkSettingsPanelLayout.createSequentialGroup()
                        .addComponent(WaterMarkAngleSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LblWaterMarkView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(WaterMarkSettingsPanelLayout.createSequentialGroup()
                        .addGroup(WaterMarkSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblWaterMarkAngel, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(WaterMarkSettingsPanelLayout.createSequentialGroup()
                                .addComponent(WaterMarkScalePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(WaterMarkCardPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(53, 53, 53))
        );
        WaterMarkSettingsPanelLayout.setVerticalGroup(
            WaterMarkSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WaterMarkSettingsPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(WaterMarkSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(WaterMarkScalePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(WaterMarkCardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblWaterMarkAngel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(WaterMarkSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(WaterMarkAngleSlider, javax.swing.GroupLayout.DEFAULT_SIZE, 332, Short.MAX_VALUE)
                    .addComponent(LblWaterMarkView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout WaterMarkPanelLayout = new javax.swing.GroupLayout(WaterMarkPanel);
        WaterMarkPanel.setLayout(WaterMarkPanelLayout);
        WaterMarkPanelLayout.setHorizontalGroup(
            WaterMarkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WaterMarkPanelLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(WaterMarkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(WaterMarkSettingsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(WaterMarkPanelLayout.createSequentialGroup()
                        .addComponent(ChkWaterMarkAsInstiName, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(69, 69, 69)
                        .addComponent(ChkWaterMarkAsInstiLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(32, 32, 32))
        );
        WaterMarkPanelLayout.setVerticalGroup(
            WaterMarkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, WaterMarkPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(WaterMarkPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ChkWaterMarkAsInstiLogo, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ChkWaterMarkAsInstiName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(WaterMarkSettingsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        RightBodyPanel.add(WaterMarkPanel, "FourthCard");
        WaterMarkPanel.getAccessibleContext().setAccessibleDescription("");

        HeaderFooterPanel.setBackground(new java.awt.Color(0, 102, 102));
        HeaderFooterPanel.setPreferredSize(new java.awt.Dimension(665, 480));

        LblHeaderFooterNote.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        LblHeaderFooterNote.setForeground(new java.awt.Color(255, 255, 255));

        LblHeader.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Header.png"))); // NOI18N

        LblFooter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/Footer.png"))); // NOI18N

        ChkFirstPageHeaderFooter.setBackground(new java.awt.Color(208, 87, 96));
        ChkFirstPageHeaderFooter.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkFirstPageHeaderFooter.setForeground(new java.awt.Color(255, 255, 255));
        ChkFirstPageHeaderFooter.setText("Set header footer to first page");

        BodyHeaderPanel.setBackground(new java.awt.Color(0, 102, 102));

        ChkHeaderLeft.setBackground(new java.awt.Color(0, 102, 102));
        ChkHeaderLeft.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkHeaderLeft.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkHeaderLeft.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkHeaderLeftItemStateChanged(evt);
            }
        });

        PanelHeaderOne.setBackground(new java.awt.Color(0, 102, 102));
        PanelHeaderOne.setLayout(new java.awt.CardLayout());

        HeaderLeftAutoPanel.setBackground(new java.awt.Color(0, 102, 102));

        CmbHeaderLeft.setBackground(new java.awt.Color(208, 87, 96));
        CmbHeaderLeft.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbHeaderLeft.setForeground(new java.awt.Color(255, 255, 255));
        CmbHeaderLeft.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Chapter/Subject", "Subject/Group", "Division", "Date", "Institute Name", "Page Number" }));

        javax.swing.GroupLayout HeaderLeftAutoPanelLayout = new javax.swing.GroupLayout(HeaderLeftAutoPanel);
        HeaderLeftAutoPanel.setLayout(HeaderLeftAutoPanelLayout);
        HeaderLeftAutoPanelLayout.setHorizontalGroup(
            HeaderLeftAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderLeftAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbHeaderLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );
        HeaderLeftAutoPanelLayout.setVerticalGroup(
            HeaderLeftAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderLeftAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbHeaderLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelHeaderOne.add(HeaderLeftAutoPanel, "HeaderLeftFirstCard");

        HeaderLeftManualPanel.setBackground(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout HeaderLeftManualPanelLayout = new javax.swing.GroupLayout(HeaderLeftManualPanel);
        HeaderLeftManualPanel.setLayout(HeaderLeftManualPanelLayout);
        HeaderLeftManualPanelLayout.setHorizontalGroup(
            HeaderLeftManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderLeftManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtHeaderLeft, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                .addContainerGap())
        );
        HeaderLeftManualPanelLayout.setVerticalGroup(
            HeaderLeftManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderLeftManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtHeaderLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelHeaderOne.add(HeaderLeftManualPanel, "HeaderLeftSecondCard");

        ChkHeaderCenter.setBackground(new java.awt.Color(0, 102, 102));
        ChkHeaderCenter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkHeaderCenter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkHeaderCenter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkHeaderCenterItemStateChanged(evt);
            }
        });

        PanelHeaderTwo.setBackground(new java.awt.Color(0, 102, 102));
        PanelHeaderTwo.setLayout(new java.awt.CardLayout());

        HeaderCenterAutoPanel.setBackground(new java.awt.Color(0, 102, 102));

        CmbHeaderCenter.setBackground(new java.awt.Color(208, 87, 96));
        CmbHeaderCenter.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbHeaderCenter.setForeground(new java.awt.Color(255, 255, 255));
        CmbHeaderCenter.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Chapter/Subject", "Subject/Group", "Division", "Date", "Institute Name", "Page Number" }));

        javax.swing.GroupLayout HeaderCenterAutoPanelLayout = new javax.swing.GroupLayout(HeaderCenterAutoPanel);
        HeaderCenterAutoPanel.setLayout(HeaderCenterAutoPanelLayout);
        HeaderCenterAutoPanelLayout.setHorizontalGroup(
            HeaderCenterAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderCenterAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbHeaderCenter, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );
        HeaderCenterAutoPanelLayout.setVerticalGroup(
            HeaderCenterAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HeaderCenterAutoPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CmbHeaderCenter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        PanelHeaderTwo.add(HeaderCenterAutoPanel, "HeaderCenterFirstCard");

        HeaderCenterManualPanel.setBackground(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout HeaderCenterManualPanelLayout = new javax.swing.GroupLayout(HeaderCenterManualPanel);
        HeaderCenterManualPanel.setLayout(HeaderCenterManualPanelLayout);
        HeaderCenterManualPanelLayout.setHorizontalGroup(
            HeaderCenterManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderCenterManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtHeaderCenter, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        HeaderCenterManualPanelLayout.setVerticalGroup(
            HeaderCenterManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderCenterManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtHeaderCenter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelHeaderTwo.add(HeaderCenterManualPanel, "HeaderCenterSecondCard");

        ChkHeaderRight.setBackground(new java.awt.Color(0, 102, 102));
        ChkHeaderRight.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkHeaderRight.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkHeaderRight.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkHeaderRightItemStateChanged(evt);
            }
        });

        PanelHeaderThree.setBackground(new java.awt.Color(0, 102, 102));
        PanelHeaderThree.setLayout(new java.awt.CardLayout());

        HeaderRightAutoPanel.setBackground(new java.awt.Color(0, 102, 102));

        CmbHeaderRight.setBackground(new java.awt.Color(208, 87, 96));
        CmbHeaderRight.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbHeaderRight.setForeground(new java.awt.Color(255, 255, 255));
        CmbHeaderRight.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Chapter/Subject", "Subject/Group", "Division", "Date", "Institute Name", "Page Number" }));

        javax.swing.GroupLayout HeaderRightAutoPanelLayout = new javax.swing.GroupLayout(HeaderRightAutoPanel);
        HeaderRightAutoPanel.setLayout(HeaderRightAutoPanelLayout);
        HeaderRightAutoPanelLayout.setHorizontalGroup(
            HeaderRightAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderRightAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbHeaderRight, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        HeaderRightAutoPanelLayout.setVerticalGroup(
            HeaderRightAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HeaderRightAutoPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CmbHeaderRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        PanelHeaderThree.add(HeaderRightAutoPanel, "HeaderRightFirstCard");

        HeaderRightManualPanel.setBackground(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout HeaderRightManualPanelLayout = new javax.swing.GroupLayout(HeaderRightManualPanel);
        HeaderRightManualPanel.setLayout(HeaderRightManualPanelLayout);
        HeaderRightManualPanelLayout.setHorizontalGroup(
            HeaderRightManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderRightManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtHeaderRight, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        HeaderRightManualPanelLayout.setVerticalGroup(
            HeaderRightManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderRightManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtHeaderRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelHeaderThree.add(HeaderRightManualPanel, "HeaderRightSecondCard");

        javax.swing.GroupLayout BodyHeaderPanelLayout = new javax.swing.GroupLayout(BodyHeaderPanel);
        BodyHeaderPanel.setLayout(BodyHeaderPanelLayout);
        BodyHeaderPanelLayout.setHorizontalGroup(
            BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ChkHeaderLeft)
                .addGap(175, 175, 175)
                .addComponent(ChkHeaderCenter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ChkHeaderRight)
                .addGap(161, 161, 161))
            .addGroup(BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                    .addGap(31, 31, 31)
                    .addComponent(PanelHeaderOne, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(405, Short.MAX_VALUE)))
            .addGroup(BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                    .addGap(227, 227, 227)
                    .addComponent(PanelHeaderTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(205, Short.MAX_VALUE)))
            .addGroup(BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                    .addGap(438, 438, 438)
                    .addComponent(PanelHeaderThree, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        BodyHeaderPanelLayout.setVerticalGroup(
            BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ChkHeaderRight)
                    .addComponent(ChkHeaderCenter)
                    .addComponent(ChkHeaderLeft))
                .addContainerGap(22, Short.MAX_VALUE))
            .addGroup(BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(PanelHeaderOne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(PanelHeaderTwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(BodyHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyHeaderPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(PanelHeaderThree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        BodyFooterPanel.setBackground(new java.awt.Color(0, 102, 102));

        ChkFooterLeft.setBackground(new java.awt.Color(0, 102, 102));
        ChkFooterLeft.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkFooterLeft.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkFooterLeft.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkFooterLeftItemStateChanged(evt);
            }
        });

        PanelFooterOne.setBackground(new java.awt.Color(0, 102, 102));
        PanelFooterOne.setLayout(new java.awt.CardLayout());

        FooterLeftAutoPanel.setBackground(new java.awt.Color(0, 102, 102));

        CmbFooterLeft.setBackground(new java.awt.Color(208, 87, 96));
        CmbFooterLeft.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbFooterLeft.setForeground(new java.awt.Color(255, 255, 255));
        CmbFooterLeft.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Chapter/Subject", "Subject/Group", "Division", "Date", "Institute Name", "Page Number" }));

        javax.swing.GroupLayout FooterLeftAutoPanelLayout = new javax.swing.GroupLayout(FooterLeftAutoPanel);
        FooterLeftAutoPanel.setLayout(FooterLeftAutoPanelLayout);
        FooterLeftAutoPanelLayout.setHorizontalGroup(
            FooterLeftAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterLeftAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbFooterLeft, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );
        FooterLeftAutoPanelLayout.setVerticalGroup(
            FooterLeftAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterLeftAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbFooterLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelFooterOne.add(FooterLeftAutoPanel, "FooterLeftFirstCard");

        FooterLeftManualPanel.setBackground(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout FooterLeftManualPanelLayout = new javax.swing.GroupLayout(FooterLeftManualPanel);
        FooterLeftManualPanel.setLayout(FooterLeftManualPanelLayout);
        FooterLeftManualPanelLayout.setHorizontalGroup(
            FooterLeftManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterLeftManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtFooterLeft, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                .addContainerGap())
        );
        FooterLeftManualPanelLayout.setVerticalGroup(
            FooterLeftManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterLeftManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtFooterLeft, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelFooterOne.add(FooterLeftManualPanel, "FooterLeftSecondCard");

        ChkFooterCenter.setBackground(new java.awt.Color(0, 102, 102));
        ChkFooterCenter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkFooterCenter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkFooterCenter.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkFooterCenterItemStateChanged(evt);
            }
        });

        PanelFooterTwo.setBackground(new java.awt.Color(0, 102, 102));
        PanelFooterTwo.setLayout(new java.awt.CardLayout());

        FooterCenterAutoPanel.setBackground(new java.awt.Color(0, 102, 102));

        CmbFooterCenter.setBackground(new java.awt.Color(208, 87, 96));
        CmbFooterCenter.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbFooterCenter.setForeground(new java.awt.Color(255, 255, 255));
        CmbFooterCenter.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Chapter/Subject", "Subject/Group", "Division", "Date", "Institute Name", "Page Number" }));

        javax.swing.GroupLayout FooterCenterAutoPanelLayout = new javax.swing.GroupLayout(FooterCenterAutoPanel);
        FooterCenterAutoPanel.setLayout(FooterCenterAutoPanelLayout);
        FooterCenterAutoPanelLayout.setHorizontalGroup(
            FooterCenterAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterCenterAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbFooterCenter, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );
        FooterCenterAutoPanelLayout.setVerticalGroup(
            FooterCenterAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FooterCenterAutoPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CmbFooterCenter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        PanelFooterTwo.add(FooterCenterAutoPanel, "FooterCenterFirstCard");

        FooterCenterManualPanel.setBackground(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout FooterCenterManualPanelLayout = new javax.swing.GroupLayout(FooterCenterManualPanel);
        FooterCenterManualPanel.setLayout(FooterCenterManualPanelLayout);
        FooterCenterManualPanelLayout.setHorizontalGroup(
            FooterCenterManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterCenterManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtFooterCenter, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FooterCenterManualPanelLayout.setVerticalGroup(
            FooterCenterManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterCenterManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtFooterCenter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelFooterTwo.add(FooterCenterManualPanel, "FooterCenterSecondCard");

        ChkFooterRight.setBackground(new java.awt.Color(0, 102, 102));
        ChkFooterRight.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ChkFooterRight.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ChkFooterRight.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkFooterRightItemStateChanged(evt);
            }
        });

        PanelFooterThree.setBackground(new java.awt.Color(0, 102, 102));
        PanelFooterThree.setLayout(new java.awt.CardLayout());

        FooterRightAutoPanel.setBackground(new java.awt.Color(0, 102, 102));

        CmbFooterRight.setBackground(new java.awt.Color(208, 87, 96));
        CmbFooterRight.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbFooterRight.setForeground(new java.awt.Color(255, 255, 255));
        CmbFooterRight.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "None", "Chapter/Subject", "Subject/Group", "Division", "Date", "Institute Name", "Page Number" }));

        javax.swing.GroupLayout FooterRightAutoPanelLayout = new javax.swing.GroupLayout(FooterRightAutoPanel);
        FooterRightAutoPanel.setLayout(FooterRightAutoPanelLayout);
        FooterRightAutoPanelLayout.setHorizontalGroup(
            FooterRightAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterRightAutoPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CmbFooterRight, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FooterRightAutoPanelLayout.setVerticalGroup(
            FooterRightAutoPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FooterRightAutoPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CmbFooterRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        PanelFooterThree.add(FooterRightAutoPanel, "FooterRightFirstCard");

        FooterRightManualPanel.setBackground(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout FooterRightManualPanelLayout = new javax.swing.GroupLayout(FooterRightManualPanel);
        FooterRightManualPanel.setLayout(FooterRightManualPanelLayout);
        FooterRightManualPanelLayout.setHorizontalGroup(
            FooterRightManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterRightManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtFooterRight, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        FooterRightManualPanelLayout.setVerticalGroup(
            FooterRightManualPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterRightManualPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(TxtFooterRight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PanelFooterThree.add(FooterRightManualPanel, "FooterRightSecondCard");

        javax.swing.GroupLayout BodyFooterPanelLayout = new javax.swing.GroupLayout(BodyFooterPanel);
        BodyFooterPanel.setLayout(BodyFooterPanelLayout);
        BodyFooterPanelLayout.setHorizontalGroup(
            BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ChkFooterLeft)
                .addGap(175, 175, 175)
                .addComponent(ChkFooterCenter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ChkFooterRight)
                .addGap(161, 161, 161))
            .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                    .addGap(31, 31, 31)
                    .addComponent(PanelFooterOne, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(405, Short.MAX_VALUE)))
            .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                    .addGap(227, 227, 227)
                    .addComponent(PanelFooterTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(205, Short.MAX_VALUE)))
            .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                    .addGap(438, 438, 438)
                    .addComponent(PanelFooterThree, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        BodyFooterPanelLayout.setVerticalGroup(
            BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ChkFooterRight)
                    .addComponent(ChkFooterCenter)
                    .addComponent(ChkFooterLeft))
                .addContainerGap(22, Short.MAX_VALUE))
            .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(PanelFooterOne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(PanelFooterTwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(BodyFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(BodyFooterPanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(PanelFooterThree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout HeaderFooterPanelLayout = new javax.swing.GroupLayout(HeaderFooterPanel);
        HeaderFooterPanel.setLayout(HeaderFooterPanelLayout);
        HeaderFooterPanelLayout.setHorizontalGroup(
            HeaderFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderFooterPanelLayout.createSequentialGroup()
                .addGroup(HeaderFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HeaderFooterPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(LblHeaderFooterNote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(HeaderFooterPanelLayout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(HeaderFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LblFooter, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BodyFooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BodyHeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ChkFirstPageHeaderFooter, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(0, 28, Short.MAX_VALUE)))
                .addContainerGap())
        );
        HeaderFooterPanelLayout.setVerticalGroup(
            HeaderFooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HeaderFooterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblHeaderFooterNote, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyHeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblFooter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BodyFooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ChkFirstPageHeaderFooter)
                .addGap(39, 39, 39))
        );

        RightBodyPanel.add(HeaderFooterPanel, "FifthCard");

        OtherSettingsPanel.setBackground(new java.awt.Color(0, 102, 102));

        LblQuestionStart.setBackground(new java.awt.Color(208, 87, 96));
        LblQuestionStart.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblQuestionStart.setForeground(new java.awt.Color(255, 255, 255));
        LblQuestionStart.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblQuestionStart.setText("Question Starting Number");
        LblQuestionStart.setOpaque(true);

        TxtQuestionStart.setFont(new java.awt.Font("Microsoft JhengHei", 1, 12)); // NOI18N
        TxtQuestionStart.setText("1");
        TxtQuestionStart.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TxtQuestionStartKeyPressed(evt);
            }
        });

        ChkQuestionBold.setBackground(new java.awt.Color(208, 87, 96));
        ChkQuestionBold.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkQuestionBold.setForeground(new java.awt.Color(255, 255, 255));
        ChkQuestionBold.setSelected(true);
        ChkQuestionBold.setText("Make Question Bold");

        ChkOptionBold.setBackground(new java.awt.Color(208, 87, 96));
        ChkOptionBold.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkOptionBold.setForeground(new java.awt.Color(255, 255, 255));
        ChkOptionBold.setSelected(true);
        ChkOptionBold.setText("Make Option Bold");

        ChkPageBorder.setBackground(new java.awt.Color(208, 87, 96));
        ChkPageBorder.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkPageBorder.setForeground(new java.awt.Color(255, 255, 255));
        ChkPageBorder.setSelected(true);
        ChkPageBorder.setText("Set Page Border");
        ChkPageBorder.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkPageBorderItemStateChanged(evt);
            }
        });

        BorderPanel.setBackground(new java.awt.Color(0, 102, 102));

        LblBorderWidth.setBackground(new java.awt.Color(208, 87, 96));
        LblBorderWidth.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblBorderWidth.setForeground(new java.awt.Color(255, 255, 255));
        LblBorderWidth.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblBorderWidth.setText("Set border line width (in pt)");
        LblBorderWidth.setOpaque(true);

        CmbBorderLineWidth.setBackground(new java.awt.Color(208, 87, 96));
        CmbBorderLineWidth.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbBorderLineWidth.setForeground(new java.awt.Color(255, 255, 255));
        CmbBorderLineWidth.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1pt", "2pt", "3pt", "4pt", "5pt", "6pt", "7pt", "8pt", "9pt", "10pt" }));

        javax.swing.GroupLayout BorderPanelLayout = new javax.swing.GroupLayout(BorderPanel);
        BorderPanel.setLayout(BorderPanelLayout);
        BorderPanelLayout.setHorizontalGroup(
            BorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BorderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblBorderWidth, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(CmbBorderLineWidth, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        BorderPanelLayout.setVerticalGroup(
            BorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BorderPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(BorderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblBorderWidth, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbBorderLineWidth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 192, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 45, Short.MAX_VALUE)
        );

        ChkSolutionBold.setBackground(new java.awt.Color(208, 87, 96));
        ChkSolutionBold.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkSolutionBold.setForeground(new java.awt.Color(255, 255, 255));
        ChkSolutionBold.setSelected(true);
        ChkSolutionBold.setText("Make Solution Bold");

        ChkChapterList.setBackground(new java.awt.Color(208, 87, 96));
        ChkChapterList.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkChapterList.setForeground(new java.awt.Color(255, 255, 255));
        ChkChapterList.setSelected(true);
        ChkChapterList.setText("Show Chapter List");

        ChkOptimizeLineSpace.setBackground(new java.awt.Color(208, 87, 96));
        ChkOptimizeLineSpace.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        ChkOptimizeLineSpace.setForeground(new java.awt.Color(255, 255, 255));
        ChkOptimizeLineSpace.setText("Set Optimize Line Space");
        ChkOptimizeLineSpace.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ChkOptimizeLineSpaceItemStateChanged(evt);
            }
        });
        ChkOptimizeLineSpace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChkOptimizeLineSpaceActionPerformed(evt);
            }
        });

        FontPanel.setBackground(new java.awt.Color(0, 102, 102));

        LblFontSize.setBackground(new java.awt.Color(208, 87, 96));
        LblFontSize.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblFontSize.setForeground(new java.awt.Color(255, 255, 255));
        LblFontSize.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblFontSize.setText("Set Font Size");
        LblFontSize.setOpaque(true);

        CmbFontSize.setBackground(new java.awt.Color(208, 87, 96));
        CmbFontSize.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        CmbFontSize.setForeground(new java.awt.Color(255, 255, 255));
        CmbFontSize.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25" }));
        CmbFontSize.setSelectedIndex(10);

        LblQuestionStart1.setBackground(new java.awt.Color(208, 87, 96));
        LblQuestionStart1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        LblQuestionStart1.setForeground(new java.awt.Color(255, 255, 255));
        LblQuestionStart1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblQuestionStart1.setText("Note:Default Font Size 11");
        LblQuestionStart1.setOpaque(true);

        javax.swing.GroupLayout FontPanelLayout = new javax.swing.GroupLayout(FontPanel);
        FontPanel.setLayout(FontPanelLayout);
        FontPanelLayout.setHorizontalGroup(
            FontPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FontPanelLayout.createSequentialGroup()
                .addComponent(LblFontSize, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(80, 80, 80)
                .addComponent(CmbFontSize, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblQuestionStart1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
        );
        FontPanelLayout.setVerticalGroup(
            FontPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, FontPanelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(FontPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblFontSize, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CmbFontSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblQuestionStart1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout OtherSettingsPanelLayout = new javax.swing.GroupLayout(OtherSettingsPanel);
        OtherSettingsPanel.setLayout(OtherSettingsPanelLayout);
        OtherSettingsPanelLayout.setHorizontalGroup(
            OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, OtherSettingsPanelLayout.createSequentialGroup()
                .addGroup(OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(ChkChapterList, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ChkQuestionBold)
                            .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                                .addComponent(LblQuestionStart, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(54, 54, 54)
                                .addComponent(TxtQuestionStart, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ChkSolutionBold, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ChkOptionBold, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ChkPageBorder, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(ChkOptimizeLineSpace))
                            .addComponent(BorderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(FontPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91))
        );
        OtherSettingsPanelLayout.setVerticalGroup(
            OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, OtherSettingsPanelLayout.createSequentialGroup()
                .addContainerGap(49, Short.MAX_VALUE)
                .addGroup(OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblQuestionStart, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtQuestionStart, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(OtherSettingsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                        .addGap(289, 289, 289)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(OtherSettingsPanelLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(ChkQuestionBold)
                        .addGap(18, 18, 18)
                        .addComponent(ChkOptionBold)
                        .addGap(18, 18, 18)
                        .addComponent(ChkSolutionBold)
                        .addGap(18, 18, 18)
                        .addComponent(ChkPageBorder)
                        .addGap(18, 18, 18)
                        .addComponent(BorderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ChkOptimizeLineSpace)
                        .addGap(16, 16, 16)
                        .addComponent(ChkChapterList)
                        .addGap(27, 27, 27)
                        .addComponent(FontPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38))
        );

        RightBodyPanel.add(OtherSettingsPanel, "SixthCard");

        FooterPanel.setBackground(new java.awt.Color(0, 102, 102));

        BtnCancel.setBackground(new java.awt.Color(208, 87, 96));
        BtnCancel.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        BtnCancel.setForeground(new java.awt.Color(255, 255, 255));
        BtnCancel.setText("Cancel");
        BtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelActionPerformed(evt);
            }
        });

        BtnUpdateSettings.setBackground(new java.awt.Color(208, 87, 96));
        BtnUpdateSettings.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        BtnUpdateSettings.setForeground(new java.awt.Color(255, 255, 255));
        BtnUpdateSettings.setText("Update Settings");
        BtnUpdateSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnUpdateSettingsActionPerformed(evt);
            }
        });

        BtnPreviousSettings.setBackground(new java.awt.Color(208, 87, 96));
        BtnPreviousSettings.setFont(new java.awt.Font("Calibri", 0, 16)); // NOI18N
        BtnPreviousSettings.setForeground(new java.awt.Color(255, 255, 255));
        BtnPreviousSettings.setText("Use Previous Settings");
        BtnPreviousSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPreviousSettingsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FooterPanelLayout = new javax.swing.GroupLayout(FooterPanel);
        FooterPanel.setLayout(FooterPanelLayout);
        FooterPanelLayout.setHorizontalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(89, 89, 89)
                .addComponent(BtnUpdateSettings, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addComponent(BtnPreviousSettings)
                .addGap(24, 24, 24))
        );
        FooterPanelLayout.setVerticalGroup(
            FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FooterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FooterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnCancel, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(BtnUpdateSettings, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(BtnPreviousSettings, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(LeftBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(RightBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 665, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(HeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(FooterPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(HeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(RightBodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LeftBodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addComponent(FooterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnPageLogoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageLogoMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageLogoMouseEntered

    private void BtnPageLogoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageLogoMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageLogoMouseExited

    private void BtnPageWatermarkMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageWatermarkMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageWatermarkMouseEntered

    private void BtnPageWatermarkMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageWatermarkMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageWatermarkMouseExited

    private void BtnPageWatermarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPageWatermarkActionPerformed
        // TODO add your handling code here:
        pageCardValue = 4;
        getUpdateNextStatus();
//        setButtonBorder(BtnPageWatermark);
//        pageSetupLayout.show(RightBodyPanel, "FourthCard");

    }//GEN-LAST:event_BtnPageWatermarkActionPerformed

    private void BtnPageHeaderFooterMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageHeaderFooterMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageHeaderFooterMouseEntered

    private void BtnPageHeaderFooterMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageHeaderFooterMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageHeaderFooterMouseExited

    private void BtnPageHeaderFooterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPageHeaderFooterActionPerformed
        // TODO add your handling code here:
        pageCardValue = 5;
        getUpdateNextStatus();
//        setButtonBorder(BtnPageHeaderFooter);
//        pageSetupLayout.show(RightBodyPanel, "FifthCard");
    }//GEN-LAST:event_BtnPageHeaderFooterActionPerformed

    private void BtnPageOtherMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageOtherMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageOtherMouseEntered

    private void BtnPageOtherMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageOtherMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageOtherMouseExited

    private void BtnPageOtherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPageOtherActionPerformed
        // TODO add your handling code here:
        pageCardValue = 6;
        getUpdateNextStatus();
//        setButtonBorder(BtnPageOther);
//        pageSetupLayout.show(RightBodyPanel, "SixthCard");

    }//GEN-LAST:event_BtnPageOtherActionPerformed

    private void BtnPageFormatMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageFormatMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageFormatMouseEntered

    private void BtnPageFormatMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BtnPageFormatMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPageFormatMouseExited

    private void BtnPageFormatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPageFormatActionPerformed
        // TODO add your handling code here:
        pageCardValue = 1;
        getUpdateNextStatus();
//        setButtonBorder(BtnPageFormat);
//        pageSetupLayout.show(RightBodyPanel, "FirstCard");
    }//GEN-LAST:event_BtnPageFormatActionPerformed

    private void BtnPageTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPageTypeActionPerformed
        // TODO add your handling code here:
        pageCardValue = 2;
        getUpdateNextStatus();
//        setButtonBorder(BtnPageType);
        
//        pageSetupLayout.show(RightBodyPanel, "SecondCard");

    }//GEN-LAST:event_BtnPageTypeActionPerformed

    private void BtnPageLogoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPageLogoActionPerformed
        // TODO add your handling code here:
        LblLogoView.setText("Institute Name");
        pageCardValue = 3;
        getUpdateNextStatus();
//        setButtonBorder(BtnPageLogo);
//        pageSetupLayout.show(RightBodyPanel, "ThirdCard");
        
    }//GEN-LAST:event_BtnPageLogoActionPerformed

    private void setLogosPanel(String filePath) {
        LblLogoPath.setText(filePath);
        ImageIcon icon = new ImageIcon(filePath);
        int width = icon.getIconWidth();
        int height = icon.getIconHeight();
        LblLogoWidth.setText("Width=" + width + "   Height=" + height);
        LblLogoNote.setText("(Note : By default Image Width & Height is set to 90 * 90)");
        icon.getImage().flush();
        LblLogoView.setIcon(icon);
    }
    
    private void setLogoPanel(String filePath) {
        sourceLogo = new File(filePath);
        LblLogoPath.setText(filePath);
        ImageIcon icon = new ImageIcon(filePath);
        int width = icon.getIconWidth();
        int height = icon.getIconHeight();
        LblLogoWidth.setText("Width=" + width + "   Height=" + height);
        LblLogoNote.setText("(Note : By default Image Width & Height is set to 90 * 90)");
        icon.getImage().flush();
        LblLogoView.setIcon(icon);
    }
    
    private void setLogoPosition() {
        if (ChkImageActualSize.isSelected()) {
            ChkLeft.setSelected(false);
            ChkRight.setSelected(false);
            if(!ChkTop.isSelected() &&  !ChkBottom.isSelected())
                ChkTop.setSelected(true);
        }
        
        if (ChkLeft.isSelected()) {
            LblLogoView.setVerticalTextPosition(SwingConstants.CENTER);
            LblLogoView.setHorizontalTextPosition(SwingConstants.RIGHT);
        } else if (ChkRight.isSelected()) {
            LblLogoView.setVerticalTextPosition(SwingConstants.CENTER);
            LblLogoView.setHorizontalTextPosition(SwingConstants.LEFT);
            LblLogoView.validate();
            LblLogoView.repaint();
        } else if(ChkTop.isSelected()) {
            LblLogoView.setVerticalTextPosition(SwingConstants.BOTTOM);
            LblLogoView.setHorizontalTextPosition(SwingConstants.CENTER);
        } else if(ChkBottom.isSelected()) {
            LblLogoView.setVerticalTextPosition(SwingConstants.TOP);
            LblLogoView.setHorizontalTextPosition(SwingConstants.CENTER);
        }
    }
    
    
    private void BtnSelectLogoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSelectLogoActionPerformed
        // TODO add your handling code here:
        String fileLocation = new FileChooser().getPngFileLocation(this);
        /*LblLogoPath.setText("");
        LblLogoView.setIcon(null);
        LblLogoWidth.setText("");
        LblLogoNote.setText("");
        sourceLogo = null;
        headerLogoImagePath = null;
        if(fileLocation != null) {
            String[] strArray = fileLocation.split(",");
            headerLogoImagePath = processPath + "/WaterMark-Logo/"+strArray[0].trim();
//            sourceLogo = new File(strArray[1].trim());
            setLogoPanel(strArray[1].trim());
        } */ //4-11-2017
        
        LblLogoPath.setText("");
        LblLogoView.setIcon(null);
        LblLogoWidth.setText("");
        LblLogoNote.setText("");
        sourceLogoString = null;
        sourceLogo = null;
        if(fileLocation != null) {
            String[] strArray = fileLocation.split(",");
            sourceLogoString = processPath + "/WaterMark-Logo/"+strArray[0].trim();
            setLogosPanel(strArray[1].trim());
            try {
                sourceLogo = new File(strArray[1].trim());
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        } else if(pdfPageSetupBean != null) {
            if(pdfPageSetupBean.getLogoPath() != null && !pdfPageSetupBean.getLogoPath().equalsIgnoreCase("")) {
                setLogosPanel(pdfPageSetupBean.getLogoPath());
            }
        }
        
      
       /* try {
            JFileChooser saveFile = new JFileChooser();//new save dialog
            saveFile.resetChoosableFileFilters();

            saveFile.setFileFilter(new FileFilter()//adds new filter into list
            {
                String description = "Image Files(*.png)";//the filter you see
                String extension = ".png";//the filter passed to program

                @Override
                public String getDescription() {
                    return description;
                }

                @Override
                public boolean accept(File f) {
                    if (f == null) {
                        return false;
                    }
                    if (f.isDirectory()) {
                        return true;
                    }
                    return f.getName().toLowerCase().endsWith(extension);
                }
            });
            saveFile.setDialogTitle("Select Logo");
            //  UIManager.put("FileChooser.saveButtonText", "Open");
            saveFile.setCurrentDirectory(new File("*.png"));
            saveFile.setCurrentDirectory(new File("*.jpg"));
            //  saveFile.setCurrentDirectory(new File("*.png"));
            int result = saveFile.showDialog(LblLogoView, "Select");
            // strSaveFileMenuItem is the parent Component which calls this method, ex: a toolbar, button or a menu item.
            if(saveFile.getSelectedFile() != null) {
                String strFileName = saveFile.getSelectedFile().getName();
                String imagepath = saveFile.getSelectedFile().getPath();
                LblLogoPath.setText(imagepath);

                ImageIcon icon = new ImageIcon(imagepath);
                int width = icon.getIconWidth();
                int height = icon.getIconHeight();
                LblLogoWidth.setText("Width=" + width + "   Height=" + height);
                LblLogoNote.setText("(Note : By default Image Width & Height is set to 90 * 90)");

                icon.getImage().flush();
                LblLogoView.setIcon(icon);
                LogoSelectPanel.setVisible(false);
//                headerLogoImagePath = "C:\\InvoicesJEE\\Type1\\" + strFileName;
//                sourceLogo = new File(imagepath);
//                destLogo = new File(headerLogoImagePath);
//                
                


//            watermarkLogoImagePath = "C:\\InvoicesJEE\\Type1\\watermarklogo.png";
//            newwatermark_info.setWatermarklogopath(watermarkLogoImagePath);
//            File dest1 = new File(headerLogoImagePath);
//            try {
//                FileUtils.copyFile(source, dest1);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

                LblLogoView.setVerticalTextPosition(SwingConstants.CENTER);
                LblLogoView.setHorizontalTextPosition(SwingConstants.RIGHT);

              //  newPaperFormat_Type_Logo.setLogoPath(headerLogoImagePath);
            }

            //
            //                ImageIcon ii = new ImageIcon(getClass().getResource(imagepath));
            //                jLabel1.setIcon(ii);
            //                jLabel1.repaint();
            // the file name selected by the user is now in the string 'strFilename'.
            //    saveFile.setApproveButtonText("Open");
        } catch (Exception ex) {
//                  statusBar.setText("Error Saving File:" + ex.getMessage()+"\n");
            ex.printStackTrace();
        }*/
    }//GEN-LAST:event_BtnSelectLogoActionPerformed
    
    private void WaterMarkAngleSliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_WaterMarkAngleSliderStateChanged
        // TODO add your handling code here:
        if (ChkWaterMarkAsInstiLogo.isSelected()) {
            rotateWatermarkText(WaterMarkAngleSlider.getValue(), "\\includegraphics{"+processPath + "/watermarklogo.png}");
        }
        if (ChkWaterMarkAsInstiName.isSelected()) {
            rotateWatermarkText(WaterMarkAngleSlider.getValue(), "\\text{Your Institute Name}");
        }
    }//GEN-LAST:event_WaterMarkAngleSliderStateChanged

    private void ChkLegalStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ChkLegalStateChanged
        // TODO add your handling code here:
        if (ChkLegal.isSelected()) {
            ChkA4.setSelected(false);
            ChkLetter.setSelected(false);
            LblLegal.setEnabled(true);
        } else {
            LblLegal.setEnabled(false);
        }
    }//GEN-LAST:event_ChkLegalStateChanged

    private void ChkLetterStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ChkLetterStateChanged
        // TODO add your handling code here:
        if (ChkLetter.isSelected()) {
            ChkA4.setSelected(false);
            ChkLegal.setSelected(false);
            LblLetter.setEnabled(true);
        } else {
            LblLetter.setEnabled(false);
        }
    }//GEN-LAST:event_ChkLetterStateChanged

    private void ChkSecondFormatItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkSecondFormatItemStateChanged
        // TODO add your handling code here:
        paperFormatItemChanged();
        /*if (ChkSecondFormat.isSelected()) {
            ChkFirstFormat.setSelected(false);
            LblSecondFormat.setEnabled(true);
        } else {
            LblSecondFormat.setEnabled(false);
        }*/
    }//GEN-LAST:event_ChkSecondFormatItemStateChanged

    private void ChkFirstFormatItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkFirstFormatItemStateChanged
        // TODO add your handling code here:
        paperFormatItemChanged();
        /*if (ChkFirstFormat.isSelected()) {
            ChkSecondFormat.setSelected(false);
            LblFirstFormat.setEnabled(true);
        } else {
            LblFirstFormat.setEnabled(false);
        }*/
    }//GEN-LAST:event_ChkFirstFormatItemStateChanged

    private void paperFormatItemChanged() {
        LblFirstFormat.setEnabled(ChkFirstFormat.isSelected());
            LblSecondFormat.setEnabled(!ChkFirstFormat.isSelected());
    }
    private void ChkPageBorderItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkPageBorderItemStateChanged
        // TODO add your handling code here:
        if (ChkPageBorder.isSelected()) {
            BorderPanel.setVisible(true);
        } else {
            BorderPanel.setVisible(false);
        }
    }//GEN-LAST:event_ChkPageBorderItemStateChanged

    private void keyPressedNumbers(KeyEvent evt, JTextField text,int length) {
        String str = text.getText();
        if(str.length() < length) {
            if ((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46) {
                text.setEditable(true);
            } else {
                text.setEditable(false);
//                JOptionPane.showMessageDialog(this, "Please Enter Numbers Only.");
            }
        } else if(!(evt.getKeyCode() == 8 || evt.getKeyCode() == 9 || evt.getKeyCode() == 13 || evt.getKeyCode() == 16 || evt.getKeyCode() == 17 || evt.getKeyCode() == 20 || evt.getKeyCode() == 27 || evt.getKeyCode() == 37 || evt.getKeyCode() == 38 || evt.getKeyCode() == 39 || evt.getKeyCode() == 40 || evt.getKeyCode() == 46)) {
            text.setEditable(false);
//            JOptionPane.showMessageDialog(this, "Please Enter Only "+length+" Numbers.");
        } else {
            text.setEditable(true);
        }
    }
    
    private void BtnUpdateSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnUpdateSettingsActionPerformed
        // TODO add your handling code here:
        String message = "";
        if(getUpdateNextStatus()) {
            message = "Are You Sure to Update Settings?";
            Object[] options = {"YES", "CANCEL"};
            int i = JOptionPane.showOptionDialog(this,message, "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
            if (i == 0) {
                pdfPageSetupBean = setsPageSetupBean();
                if(new PageSetupOperation().insertPdfPagesSetup(pdfPageSetupBean)) {
                    nextCall();
                }
            }
        }
        
//        String message = "";
//        if(pdfPageSetupBean == null) {
//            if(getUpdateNextStatus()) {
//                message = "Are You Sure to Continue?";
//                Object[] options = {"YES", "CANCEL"};
//                int i = JOptionPane.showOptionDialog(this,message, "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
//                if (i == 0) {
//                    pdfPageSetupBean = setsPageSetupBean();
//                    if(new PageSetupOperation().insertPdfPagesSetup(pdfPageSetupBean)) {
//                        nextCall();
//                    }
//                }          
//            }
//        } else {
//            message = "Are You Sure to Continue With Default Page Settings?";
//            Object[] options = {"YES", "CANCEL"};
//            int i = JOptionPane.showOptionDialog(this,message, "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
//            if (i == 0) {
//                nextCall();
//            } else {
//                if(pdfPageSetupBean.isSetLogo()) {
//                    destLogo = new File(pdfPageSetupBean.getLogoPath());
//                }
//                pdfPageSetupBean = null;
//            }
//        }    
    }//GEN-LAST:event_BtnUpdateSettingsActionPerformed

    private void BtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelActionPerformed
        // TODO add your handling code here:
        if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
            SingleChapterQuestionsSelection frm = (SingleChapterQuestionsSelection)quesPageObject;
            frm.setEnabled(true);
        } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
            MultipleChapterQuestionsSelection frm = (MultipleChapterQuestionsSelection)quesPageObject;
            frm.setEnabled(true);
        } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
            MultipleYearQuestionsSelection frm = (MultipleYearQuestionsSelection)quesPageObject;
            frm.setEnabled(true);
        } else if(HomePage.class.isInstance(quesPageObject)) {
            HomePage frm = (HomePage)quesPageObject;
            frm.setEnabled(true);
        }
        this.dispose();
    }//GEN-LAST:event_BtnCancelActionPerformed
    
    private void nextCall() {
        if(pdfPageSetupBean.getPageFormat() == 0) {
            new PdfFormatFirst(selectedQuestionsList,selectedSubjectList,chaptersList,pdfPageSetupBean,questionsList,printingPaperType,quesPageObject,this).setVisible(true);
            this.setVisible(false);
        } else {
            new PdfFormatSecond(selectedQuestionsList,selectedSubjectList,chaptersList,pdfPageSetupBean,questionsList,printingPaperType,quesPageObject,this).setVisible(true);
            this.setVisible(false);
        }
    }

    private boolean checkHeaderFooterText() {
        boolean returnValue = true;
        if(ChkHeaderLeft.isSelected()) {
            if(!TxtHeaderLeft.getText().trim().isEmpty())
                returnValue = true;
            else
                return false;
        }
        
        if(ChkHeaderCenter.isSelected()) {
            if(!TxtHeaderCenter.getText().trim().isEmpty())
                returnValue = true;
            else
                return false;
        }
        
        if(ChkHeaderRight.isSelected()) {
            if(!TxtHeaderRight.getText().trim().isEmpty())
                returnValue = true;
            else
                return false;
        }
        
        if(ChkFooterLeft.isSelected()) {
            if(!TxtFooterLeft.getText().trim().isEmpty())
                returnValue = true;
            else
                return false;
        }
        
        if(ChkFooterCenter.isSelected()) {
            if(!TxtFooterCenter.getText().trim().isEmpty())
                returnValue = true;
            else
                return false;
        }
        
        if(ChkFooterRight.isSelected()) {
            if(!TxtFooterRight.getText().trim().isEmpty())
                returnValue = true;
            else
                return false;
        }
        
        return returnValue;
    }
        int set = 0;
    private void BtnBrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnBrowseActionPerformed
        // TODO add your handling code here:
        String tempPath = new FileChooser().getPngFileLocation(this);
        
        //System.out.println("tempPath:="+tempPath);
        
        waterMarkImageFile = null;
        LblWaterLogoPath.setText("");
        if(tempPath != null) {
            String[] fileArray = tempPath.split(",");
          //  System.out.println("fileArray:="+fileArray);
          //  System.out.println("fileArray[0]:="+fileArray[0]);
          //   System.out.println("fileArray[1]:="+fileArray[1]);
             
            waterMarkImageFile = new File(fileArray[1].trim());
            
            LblWaterLogoPath.setText("<html><font size=2>\n" +
                                    getSubString(fileArray[1].trim())+"\n" +
                                    "</font></html>");
            JOptionPane.showMessageDialog(rootPane, "Image Loaded Successfully");
        } else if(pdfPageSetupBean != null) {
            if(pdfPageSetupBean.getWaterMarkInfo() == 2)
                LblWaterLogoPath.setText("<html><font size=2>\n" +
                                    getSubString(pdfPageSetupBean.getWaterMarkLogoPath().trim())+"\n" +
                                    "</font></html>");
        }
    }//GEN-LAST:event_BtnBrowseActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(rootPane, "Are You Sure go to Home Page?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
//            quesFrame.dispose();
            if(SingleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                SingleChapterQuestionsSelection frm = (SingleChapterQuestionsSelection)quesPageObject;
                frm.dispose();
            } else if(MultipleChapterQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleChapterQuestionsSelection frm = (MultipleChapterQuestionsSelection)quesPageObject;
                frm.dispose();
            } else if(MultipleYearQuestionsSelection.class.isInstance(quesPageObject)) {
                MultipleYearQuestionsSelection frm = (MultipleYearQuestionsSelection)quesPageObject;
                frm.dispose();
            } else if(HomePage.class.isInstance(quesPageObject)) {
                HomePage frm = (HomePage)quesPageObject;
                frm.dispose();
            }
            new HomePage().setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void ChkA4StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_ChkA4StateChanged
        // TODO add your handling code here:
        if (ChkA4.isSelected()) {
            LblA4.setEnabled(true);
            ChkLegal.setSelected(false);
            ChkLetter.setSelected(false);
        } else {
            LblA4.setEnabled(false);
        }
    }//GEN-LAST:event_ChkA4StateChanged

    private void ChkA4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkA4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ChkA4ActionPerformed

    private void LblA4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblA4MouseClicked
        if (ChkA4.isSelected()) {
            ChkA4.setSelected(false);
            LblA4.setEnabled(false);
        } else {
            ChkA4.setSelected(true);
            LblA4.setEnabled(true);
        }
        ChkLegal.setSelected(false);
        ChkLetter.setSelected(false);
    }//GEN-LAST:event_LblA4MouseClicked

    private void LblLegalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblLegalMouseClicked
        if (ChkLegal.isSelected()) {
            ChkLegal.setSelected(false);
            LblLegal.setEnabled(false);
        } else {
            ChkLegal.setSelected(true);
            LblLegal.setEnabled(true);
        }
        ChkA4.setSelected(false);
        ChkLetter.setSelected(false);
    }//GEN-LAST:event_LblLegalMouseClicked

    private void LblLetterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LblLetterMouseClicked
        if (ChkLetter.isSelected()) {
            ChkLetter.setSelected(false);
            LblLetter.setEnabled(false);
        } else {
            ChkLetter.setSelected(true);
            LblLetter.setEnabled(true);
        }
        ChkLegal.setSelected(false);
        ChkA4.setSelected(false);
    }//GEN-LAST:event_LblLetterMouseClicked

    private void ChkFooterLeftItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkFooterLeftItemStateChanged
        // TODO add your handling code here:
        if(!ChkFooterLeft.isSelected()){
            footerLeftLayout.show(PanelFooterOne,"FooterLeftFirstCard");
        } else {
            footerLeftLayout.show(PanelFooterOne,"FooterLeftSecondCard");
        }
    }//GEN-LAST:event_ChkFooterLeftItemStateChanged

    private void ChkFooterCenterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkFooterCenterItemStateChanged
        // TODO add your handling code here:
        if(!ChkFooterCenter.isSelected()){
            footerCenterLayout.show(PanelFooterTwo,"FooterCenterFirstCard");
        } else {
            footerCenterLayout.show(PanelFooterTwo,"FooterCenterSecondCard");
        }
    }//GEN-LAST:event_ChkFooterCenterItemStateChanged

    private void ChkFooterRightItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkFooterRightItemStateChanged
        // TODO add your handling code here:
        if(!ChkFooterRight.isSelected()){
            footerRightLayout.show(PanelFooterThree,"FooterRightFirstCard");
        } else {
            footerRightLayout.show(PanelFooterThree,"FooterRightSecondCard");
        }
    }//GEN-LAST:event_ChkFooterRightItemStateChanged

    private void ChkHeaderLeftItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkHeaderLeftItemStateChanged
        // TODO add your handling code here:
        if(!ChkHeaderLeft.isSelected()){
            headerLeftLayout.show(PanelHeaderOne,"HeaderLeftFirstCard");
        } else {
            headerLeftLayout.show(PanelHeaderOne,"HeaderLeftSecondCard");
        }
    }//GEN-LAST:event_ChkHeaderLeftItemStateChanged

    private void ChkHeaderCenterItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkHeaderCenterItemStateChanged
        // TODO add your handling code here:
        if(!ChkHeaderCenter.isSelected()){
            headerCenterLayout.show(PanelHeaderTwo,"HeaderCenterFirstCard");
        } else {
            headerCenterLayout.show(PanelHeaderTwo,"HeaderCenterSecondCard");
        }
    }//GEN-LAST:event_ChkHeaderCenterItemStateChanged

    private void ChkHeaderRightItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkHeaderRightItemStateChanged
        // TODO add your handling code here:
        if(!ChkHeaderRight.isSelected()){
            headerRightLayout.show(PanelHeaderThree,"HeaderRightFirstCard");
        } else {
            headerRightLayout.show(PanelHeaderThree,"HeaderRightSecondCard");
        }
    }//GEN-LAST:event_ChkHeaderRightItemStateChanged

    private void ChkWaterMarkAsInstiNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ChkWaterMarkAsInstiNameMouseClicked
        // TODO add your handling code here:
         if(ChkWaterMarkAsInstiLogo.isSelected()){
            ChkWaterMarkAsInstiLogo.setSelected(false);
            ChkWaterMarkAsInstiName.setSelected(true);
        }
        setWaterMarkPanel();
        
    }//GEN-LAST:event_ChkWaterMarkAsInstiNameMouseClicked

    private void ChkWaterMarkAsInstiLogoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ChkWaterMarkAsInstiLogoMouseClicked
        // TODO add your handling code here:
         if(ChkWaterMarkAsInstiName.isSelected()){
            ChkWaterMarkAsInstiName.setSelected(false);
            ChkWaterMarkAsInstiLogo.setSelected(true);
        }
        setWaterMarkPanel();
    }//GEN-LAST:event_ChkWaterMarkAsInstiLogoMouseClicked

    private void TxtQuestionStartKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TxtQuestionStartKeyPressed
        // TODO add your handling code here:
        keyPressedNumbers(evt,TxtQuestionStart,3);
    }//GEN-LAST:event_TxtQuestionStartKeyPressed

    private void ChkSetLogoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkSetLogoItemStateChanged
        // TODO add your handling code here:
//        
//        if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
//            LogoSelectPanel.setVisible(true);
//        } else {
//            LogoSelectPanel.setVisible(false);
//        }
    }//GEN-LAST:event_ChkSetLogoItemStateChanged

    private void BtnPreviousSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPreviousSettingsActionPerformed
        // TODO add your handling code here:
        if(pdfPageSetupBean != null) {
            nextCall();
        } else {
            JOptionPane.showMessageDialog(rootPane, "You Dont Have Previous Saved Settings.");
        }
    }//GEN-LAST:event_BtnPreviousSettingsActionPerformed

    private void ChkLeftActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkLeftActionPerformed
        // TODO add your handling code here:
        if (!ChkImageActualSize.isSelected()) {
            ChkLeft.setSelected(true);
            ChkRight.setSelected(false);
            ChkTop.setSelected(false);
            ChkBottom.setSelected(false);
        }
        setLogoPosition();
    }//GEN-LAST:event_ChkLeftActionPerformed

    private void ChkRightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkRightActionPerformed
        // TODO add your handling code here:
        if (!ChkImageActualSize.isSelected()) {
            ChkRight.setSelected(true);
            ChkLeft.setSelected(false);
            ChkTop.setSelected(false);
            ChkBottom.setSelected(false);
        }
        setLogoPosition();
    }//GEN-LAST:event_ChkRightActionPerformed

    private void ChkTopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkTopActionPerformed
        // TODO add your handling code here:
        ChkTop.setSelected(true);
        ChkRight.setSelected(false);
        ChkLeft.setSelected(false);
        ChkBottom.setSelected(false);
        setLogoPosition();
    }//GEN-LAST:event_ChkTopActionPerformed

    private void ChkBottomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkBottomActionPerformed
        // TODO add your handling code here:
        ChkBottom.setSelected(true);
        ChkRight.setSelected(false);
        ChkTop.setSelected(false);
        ChkLeft.setSelected(false);
        setLogoPosition();
    }//GEN-LAST:event_ChkBottomActionPerformed

    private void ChkImageActualSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkImageActualSizeActionPerformed
        // TODO add your handling code here:
        if (ChkImageActualSize.isSelected()) {
            ChkLeft.setSelected(false);
            ChkRight.setSelected(false);
            ChkTop.setSelected(true);
            ChkBottom.setSelected(false);
            setLogoPosition();
        }
    }//GEN-LAST:event_ChkImageActualSizeActionPerformed

    private void ChkOptimizeLineSpaceItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ChkOptimizeLineSpaceItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_ChkOptimizeLineSpaceItemStateChanged

    private void ChkOptimizeLineSpaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChkOptimizeLineSpaceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ChkOptimizeLineSpaceActionPerformed
    JFrame frame111 = new JFrame("JColorChooser Sample Popup");
//    frame111.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

    private void rotateWatermarkText(int angle, String Text) {
        String start = "\\begin{array}{l}";
        String end = "\\end{array}";
        String finalQuesitonstr = "\\mbox{\\rotatebox{" + angle + "}{" + Text + "}}";
        try {
            TeXFormula formula = new TeXFormula(start + finalQuesitonstr + end);
            TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
            icon.setInsets(new Insets(0, 0, 0, 0));
            BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = image.createGraphics();
            g2.setColor(Color.white);
            g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());

            LblWaterMarkView.setIcon(icon);
        } catch (Exception e) {
            LblWaterMarkView.setIcon(null);
        }
    }

    private void setButtonBorder(JButton b) {
        BtnPageFormat.setBorderPainted(false);
        BtnPageHeaderFooter.setBorderPainted(false);
        BtnPageLogo.setBorderPainted(false);
        BtnPageType.setBorderPainted(false);
        BtnPageWatermark.setBorderPainted(false);
        BtnPageOther.setBorderPainted(false);

        BtnPageFormat.setFont(new java.awt.Font("Corbel", 0, 16));
        BtnPageHeaderFooter.setFont(new java.awt.Font("Corbel", 0, 16));
        BtnPageLogo.setFont(new java.awt.Font("Corbel", 0, 16));
        BtnPageType.setFont(new java.awt.Font("Corbel", 0, 16));
        BtnPageWatermark.setFont(new java.awt.Font("Corbel", 0, 16));
        BtnPageOther.setFont(new java.awt.Font("Corbel", 0, 16));

        b.setFont(new java.awt.Font("Corbel", 1, 18));
        b.setBorderPainted(true);
        b.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(212, 208, 200), 10));

    }
   
    private void setPaperTypePanel(){
        if (ChkA4.isSelected()) {
            LblA4.setEnabled(true);
            LblLegal.setEnabled(false);
            LblLetter.setEnabled(false);
        } else if(ChkLegal.isSelected()){
            LblA4.setEnabled(false);
            LblLegal.setEnabled(true);
            LblLetter.setEnabled(false);
        } else { 
            LblA4.setEnabled(false);
            LblLegal.setEnabled(false);
            LblLetter.setEnabled(true);
        }
    }
    
    private void setWaterMarkPanel() {
        if (ChkWaterMarkAsInstiName.isSelected()) {
            waterMarkLayout.show(WaterMarkCardPanel, "TextGrayCard");
            rotateWatermarkText(WaterMarkAngleSlider.getValue(), "\\text{Your Institute Name}");
            WaterMarkSettingsPanel.setVisible(true);
//            WaterMarkTextGrayScalePanel.setVisible(true);
//            WaterMarkScalePanel.setVisible(true);
            BtnBrowse.setVisible(false);
            ChkWaterMarkAsInstiLogo.setSelected(false);
        } else if(ChkWaterMarkAsInstiLogo.isSelected()) {
            waterMarkLayout.show(WaterMarkCardPanel, "WaterLogoCard");
            rotateWatermarkText(WaterMarkAngleSlider.getValue(),"\\includegraphics{"+processPath + "/watermarklogo.png}");
            WaterMarkSettingsPanel.setVisible(true);
//            WaterMarkScalePanel.setVisible(true);
            BtnBrowse.setVisible(true);
//            WaterMarkTextGrayScalePanel.setVisible(false);
            ChkWaterMarkAsInstiName.setSelected(false);
        } else {
            WaterMarkSettingsPanel.setVisible(false);
        }
    }
    
    private int getHFComboIndex(String str) {
    //    None, Chapter/Subject, Subject/Group, Division, Date, Institute Name, Page No
        int returnInd = 0;
        if(str.equals("None"))
            returnInd = 0;
        else if(str.equalsIgnoreCase("Chapter/Subject"))
            returnInd = 1;
        else if(str.equalsIgnoreCase("Subject/Group"))
            returnInd = 2;
        else if(str.equalsIgnoreCase("Division"))
            returnInd = 3;
        else if(str.equalsIgnoreCase("Date"))
            returnInd = 4;
        else if(str.equalsIgnoreCase("Institute Name"))
            returnInd = 5;
        else if(str.equalsIgnoreCase("Page Number"))
            returnInd = 6;
        return returnInd;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyFooterPanel;
    private javax.swing.JPanel BodyHeaderPanel;
    private javax.swing.JPanel BorderPanel;
    private javax.swing.JButton BtnBrowse;
    private javax.swing.JButton BtnCancel;
    private javax.swing.JButton BtnPageFormat;
    private javax.swing.JButton BtnPageHeaderFooter;
    private javax.swing.JButton BtnPageLogo;
    private javax.swing.JButton BtnPageOther;
    private javax.swing.JButton BtnPageType;
    private javax.swing.JButton BtnPageWatermark;
    private javax.swing.JButton BtnPreviousSettings;
    private javax.swing.JButton BtnSelectLogo;
    private javax.swing.JButton BtnUpdateSettings;
    private javax.swing.JCheckBox ChkA4;
    private javax.swing.JCheckBox ChkBottom;
    private javax.swing.JCheckBox ChkChapterList;
    private javax.swing.JCheckBox ChkColumnType;
    private javax.swing.JCheckBox ChkFirstFormat;
    private javax.swing.JCheckBox ChkFirstPageHeaderFooter;
    private javax.swing.JCheckBox ChkFooterCenter;
    private javax.swing.JCheckBox ChkFooterLeft;
    private javax.swing.JCheckBox ChkFooterRight;
    private javax.swing.JCheckBox ChkHeaderCenter;
    private javax.swing.JCheckBox ChkHeaderLeft;
    private javax.swing.JCheckBox ChkHeaderRight;
    private javax.swing.JCheckBox ChkImageActualSize;
    private javax.swing.JCheckBox ChkLeft;
    private javax.swing.JCheckBox ChkLegal;
    private javax.swing.JCheckBox ChkLetter;
    private javax.swing.JCheckBox ChkOptimizeLineSpace;
    private javax.swing.JCheckBox ChkOptionBold;
    private javax.swing.JCheckBox ChkPageBorder;
    private javax.swing.JCheckBox ChkQuestionBold;
    private javax.swing.JCheckBox ChkRight;
    private javax.swing.JCheckBox ChkSecondFormat;
    private javax.swing.JCheckBox ChkSetLogo;
    private javax.swing.JCheckBox ChkSolutionBold;
    private javax.swing.JCheckBox ChkTop;
    private javax.swing.JCheckBox ChkWaterMarkAsInstiLogo;
    private javax.swing.JCheckBox ChkWaterMarkAsInstiName;
    private javax.swing.JCheckBox ChkYear;
    private javax.swing.JComboBox CmbBorderLineWidth;
    private javax.swing.JComboBox CmbFontSize;
    private javax.swing.JComboBox CmbFooterCenter;
    private javax.swing.JComboBox CmbFooterLeft;
    private javax.swing.JComboBox CmbFooterRight;
    private javax.swing.JComboBox CmbHeaderCenter;
    private javax.swing.JComboBox CmbHeaderLeft;
    private javax.swing.JComboBox CmbHeaderRight;
    private javax.swing.JComboBox CmbWaterMarkScale;
    private javax.swing.JComboBox CmbWaterMarkTextGrayScale;
    private javax.swing.JPanel FontPanel;
    private javax.swing.JPanel FooterCenterAutoPanel;
    private javax.swing.JPanel FooterCenterManualPanel;
    private javax.swing.JPanel FooterLeftAutoPanel;
    private javax.swing.JPanel FooterLeftManualPanel;
    private javax.swing.JPanel FooterPanel;
    private javax.swing.JPanel FooterRightAutoPanel;
    private javax.swing.JPanel FooterRightManualPanel;
    private javax.swing.JPanel FormatPanel;
    private javax.swing.JPanel HeaderCenterAutoPanel;
    private javax.swing.JPanel HeaderCenterManualPanel;
    private javax.swing.JPanel HeaderFooterPanel;
    private javax.swing.JPanel HeaderLeftAutoPanel;
    private javax.swing.JPanel HeaderLeftManualPanel;
    private javax.swing.JPanel HeaderPanel;
    private javax.swing.JPanel HeaderRightAutoPanel;
    private javax.swing.JPanel HeaderRightManualPanel;
    private javax.swing.JLabel LblA4;
    private javax.swing.JLabel LblBodyHeader;
    private javax.swing.JLabel LblBorderWidth;
    private javax.swing.JLabel LblFirstFormat;
    private javax.swing.JLabel LblFontSize;
    private javax.swing.JLabel LblFooter;
    private javax.swing.JLabel LblHeader;
    private javax.swing.JLabel LblHeaderFooterNote;
    private javax.swing.JLabel LblLegal;
    private javax.swing.JLabel LblLetter;
    private javax.swing.JLabel LblLogoNote;
    private javax.swing.JLabel LblLogoPath;
    private javax.swing.JLabel LblLogoPosition;
    private javax.swing.JLabel LblLogoView;
    private javax.swing.JLabel LblLogoWidth;
    private javax.swing.JLabel LblPaperTypeDimention;
    private javax.swing.JLabel LblPaperTypeFirstHeader;
    private javax.swing.JLabel LblPaperTypeSecondHeader;
    private javax.swing.JLabel LblQuestionStart;
    private javax.swing.JLabel LblQuestionStart1;
    private javax.swing.JLabel LblSecondFormat;
    private javax.swing.JLabel LblWaterLogoPath;
    private javax.swing.JLabel LblWaterMarkAngel;
    private javax.swing.JLabel LblWaterMarkScale;
    private javax.swing.JLabel LblWaterMarkTextGrayScale;
    private javax.swing.JLabel LblWaterMarkView;
    private javax.swing.JPanel LeftBodyPanel;
    private javax.swing.JPanel LogoPanel;
    private javax.swing.JPanel LogoPositionPanel;
    private javax.swing.JPanel LogoSelectPanel;
    private javax.swing.JPanel OtherSettingsPanel;
    private javax.swing.JPanel PanelFooterOne;
    private javax.swing.JPanel PanelFooterThree;
    private javax.swing.JPanel PanelFooterTwo;
    private javax.swing.JPanel PanelHeaderOne;
    private javax.swing.JPanel PanelHeaderThree;
    private javax.swing.JPanel PanelHeaderTwo;
    private javax.swing.JPanel PaperTypePanel;
    private javax.swing.JPanel RightBodyPanel;
    private javax.swing.JTextField TxtFooterCenter;
    private javax.swing.JTextField TxtFooterLeft;
    private javax.swing.JTextField TxtFooterRight;
    private javax.swing.JTextField TxtHeaderCenter;
    private javax.swing.JTextField TxtHeaderLeft;
    private javax.swing.JTextField TxtHeaderRight;
    private javax.swing.JTextField TxtQuestionStart;
    private javax.swing.JPanel WaterLogoPathPanel;
    private javax.swing.JSlider WaterMarkAngleSlider;
    private javax.swing.JPanel WaterMarkCardPanel;
    private javax.swing.JPanel WaterMarkPanel;
    private javax.swing.JPanel WaterMarkScalePanel;
    private javax.swing.JPanel WaterMarkSettingsPanel;
    private javax.swing.JPanel WaterMarkTextGrayScalePanel;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
