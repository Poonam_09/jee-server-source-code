/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.bean.ChapterViewBean;
import com.db.operations.ChapterOperation;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class test {
    public static void main(String[] args) {
        ArrayList<ChapterViewBean> tempList = new ChapterOperation().getViewChapterList();
        for(ChapterViewBean bean : tempList) {
            if(bean.getGroupBean().getGroupId() == 2)
                System.out.println(bean.getChapterId()+"   "+bean.getChapterName()+"   "+bean.getTotalQuesCount());
        }
    }
}
