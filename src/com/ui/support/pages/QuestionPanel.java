/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ui.support.pages;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.InputEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import org.scilab.forge.jlatexmath.*;
import com.LatexProcessing.LatexConversion;
import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import java.util.ArrayList;
import ui.ZoomImage;

/**
 *
 * @author Administrator
 */
public class QuestionPanel extends javax.swing.JPanel {

    /**
     * Creates new form QuestionPanel
     */
    String start, end;
    private ArrayList<ImageRatioBean> imageRatioList;
    QuestionBean currentQuestion;
    QuestionBean currentsQuestion;
    String qImagePath = "", oImagePath = "", hImagePath = "";
    LatexConversion lc=new LatexConversion();
    public QuestionPanel() {
        initComponents();
        setOpaque(false);
        start = "\\begin{array}{l}";
        end = "\\end{array}";
        lblHint.setVisible(false);
//        setIconImage(new ImageIcon(getClass().getResource("/ui/images/c.gif")).getImage());
    }

    public String getUserAnswer() {
        ButtonModel buttonModel = buttonGroup1.getSelection();
        if (buttonModel != null) {
            String userAnswer = buttonModel.getActionCommand();
            
   
            if (!userAnswer.equals("") || userAnswer != null) {
                currentQuestion.setAnswer(userAnswer);
                return userAnswer;
            } else {
                return null;
            }
        }
        return null;
    }

    public void showOptions() {
        lblA.setVisible(true);
        lblOptionA.setVisible(true);
        lblB.setVisible(true);
        lblOptionB.setVisible(true);
        lblC.setVisible(true);
        lblOptionC.setVisible(true);
        lblD.setVisible(true);
        lblOptionD.setVisible(true);
        lblOptionAsImage.setVisible(false);
    }

    public void hideOptions() {
        lblA.setVisible(false);
        lblOptionA.setVisible(false);
        lblB.setVisible(false);
        lblOptionB.setVisible(false);
        lblC.setVisible(false);
        lblOptionC.setVisible(false);
        lblD.setVisible(false);
        lblOptionD.setVisible(false);
        lblOptionAsImage.setVisible(true);
    }

    public void setLableText(JLabel l, String str) {
        try {
            if (!str.equals("")) {
                l.setText("");
                TeXFormula formula;
                TeXIcon icon;
                BufferedImage image;
                Graphics2D g2;
                JLabel jl;
                str = start + str + end;
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0, 0, 0, 0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                l.setIcon(icon);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null,e.getMessage()); 
        }
    }

    public void showQuestionImage() {
        lblQuestionAsImage.setVisible(true);
    }

    public void hideQuestionImage() {
        lblQuestionAsImage.setVisible(false);
    }

    public void loadImage(String path, JLabel lbl) {
        BufferedImage image;
        try {
            File file = new File(path);
            image = ImageIO.read(file);
            ImageIcon icon;
            float width = image.getWidth();
            float height = image.getHeight();
            float val = (float) 1.0;
            if(imageRatioList != null) {
                for(ImageRatioBean imageRatioBean : imageRatioList) {
                    if(imageRatioBean.getImageName().equalsIgnoreCase(path)) {
                        val = (float) imageRatioBean.getViewDimention();
                        break;
                    }   
                }
            }
//            val = new Quest.chapterwise.QuestFirstPageSettingC().getImageDim(path);
            width = (float) (width * val);
            height = (float) (height * val);
 
//                width=(float) (width*0.3);
//                height=(float) (height*0.3);
            //Determine how the image has to be scaled if it is large:
            Image thumb = image.getScaledInstance((int) width, (int) height, Image.SCALE_AREA_AVERAGING);
            icon = new ImageIcon(thumb);

            lbl.setIcon(icon);
            lbl.setText("");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void setQuestionOnPanel(QuestionBean question, int index) {
        
        currentQuestion = question;

        setLableText(lblQuestion, question.getQuestion());
        qImagePath = currentQuestion.getQuestionImagePath();
        oImagePath = currentQuestion.getOptionImagePath();
        hImagePath = currentQuestion.getHintImagePath();
        if (!question.isIsQuestionAsImage()) {
            hideQuestionImage();
        } else {
            showQuestionImage();
            loadImage(question.getQuestionImagePath(), lblQuestionAsImage);
        }
        if (!question.isIsHintAsImage()) {
            lblHintImage.setVisible(false);
        } else {
            lblHintImage.setVisible(true);
            loadImage(question.getHintImagePath(), lblHintImage);
        }
        if (!question.isIsOptionAsImage()) {
            showOptions();
            setLableText(lblOptionA, question.getOptionA());
            setLableText(lblOptionB, question.getOptionB());
            setLableText(lblOptionC, question.getOptionC());
            setLableText(lblOptionD, question.getOptionD());
        } else {
            hideOptions();
            loadImage(question.getOptionImagePath(), lblOptionAsImage);
        }

        String userAnswer = question.getAnswer();
        if (userAnswer.equals("UnAttempted")) {
        } else {
            if (userAnswer.equals("A")) {
                lblAns.setText("A");
            } else if (userAnswer.equals("B")) {
                lblAns.setText("B");
            } else if (userAnswer.equals("C")) {
                lblAns.setText("C");
            }
            if (userAnswer.equals("D")) {
                lblAns.setText("D");
            }
        }
        if (question.getLevel() == 0) {
            lblLevel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/level_Easy.png")));
        } else if (question.getLevel() == 1) {
            lblLevel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/level_Medium.png")));
        } else if (question.getLevel() ==2) {
            lblLevel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/level_Hard.png")));
        }
        lblYear.setText(question.getYear());
        if (question.getHint().equals("\\mbox{}")) {
            lblHint.setVisible(false);
            lblQHint.setVisible(false);
        } else {
            lblHint.setVisible(true);
            lblQHint.setVisible(true);
            setLableText(lblQHint, question.getHint());
        }
        lblQuestionNo.setText("Q. " + index);
    }

    //Aniket
    public void setQuestionsOnPanel(QuestionBean question, int index,ArrayList<ImageRatioBean> imageRatioList) {
//        currentsQuestion = question;
       
        txtNumericalAnswer.setIcon(null);
        
        this.imageRatioList = imageRatioList;
        lc.setLableText(lblQuestion, question.getQuestion());
        qImagePath = question.getQuestionImagePath();
        oImagePath = question.getOptionImagePath();
        hImagePath = question.getHintImagePath();
        if (!question.isIsQuestionAsImage()) {
            hideQuestionImage();
        } else {
            showQuestionImage();
            loadImage(question.getQuestionImagePath(), lblQuestionAsImage);
        }
        if (!question.isIsHintAsImage()) {
            lblHintImage.setVisible(false);
        } else {
            lblHintImage.setVisible(true);
            loadImage(question.getHintImagePath(), lblHintImage);
        }
        if(question.getType()==0||question.getType()==1)
        { 
               lblQuestionNo6.setVisible(true);
               lblNumericalAnswer.setVisible(false);
               txtNumericalAnswer.setVisible(false);
               
                if (!question.isIsOptionAsImage()) {
                    showOptions();
                    lc.setLableText(lblOptionA, question.getOptionA());
                    lc.setLableText(lblOptionB, question.getOptionB());
                    lc.setLableText(lblOptionC, question.getOptionC());
                    lc.setLableText(lblOptionD, question.getOptionD());
                } else {
                    hideOptions();
                    loadImage(question.getOptionImagePath(), lblOptionAsImage);
                }

                String userAnswer = question.getAnswer();
                if (userAnswer.equals("UnAttempted")) {
                } else {
                    if (userAnswer.equals("A")) {
                        lblAns.setText("A");
                    } else if (userAnswer.equals("B")) {
                        lblAns.setText("B");
                    } else if (userAnswer.equals("C")) {
                        lblAns.setText("C");
                    }
                    if (userAnswer.equals("D")) {
                        lblAns.setText("D");
                    }
                }
        }
        else if(question.getType()==2)
        { 
               lblNumericalAnswer.setVisible(true);
               txtNumericalAnswer.setVisible(true);
               
                hideOptions();

                lblQuestionNo6.setVisible(false);
                lblOptionAsImage.setVisible(false);
                lblAns.setVisible(false);
                lblQuestionNo5.setVisible(false);
                
                
                      if(question.getNumericalAnswer()!=null)  
                      {    
                          lc.setLableText(txtNumericalAnswer, question.getNumericalAnswer().trim()+question.getUnit().trim());
                          System.out.println("Num Answer---------"+question.getNumericalAnswer().trim()+question.getUnit().trim());
                      }
                      else 
                      {
                          System.out.println("Null Ans----------------");
                          System.out.println("Question Id-------------"+question.getQuestionId());
                          System.out.println("Question Answer-------------"+question.getAnswer());
                      }
                      
        }
        
        if (question.getLevel() == 0) {
            lblLevel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/level_Easy.png")));
        } else if (question.getLevel() == 1) {
            lblLevel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/level_Medium.png")));
        } else if (question.getLevel() ==2) {
            lblLevel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/level_Hard.png")));
        }
        lblYear.setText(question.getYear());
        if (question.getHint().equals("\\mbox{}")) {
            lblHint.setVisible(false);
            lblQHint.setVisible(false);
        } else {
            lblHint.setVisible(true);
            lblQHint.setVisible(true);
            lc.setLableText(lblQHint, question.getHint());
        }
        lblQuestionNo.setText("Q. " + index);
        
      
    }
    
    
    public void setQuestionOnPanelForQP(QuestionBean question, int index) {
        currentQuestion = question;

        setLableText(lblQuestion, question.getQuestion());
        qImagePath = currentQuestion.getQuestionImagePath();
        oImagePath = currentQuestion.getOptionImagePath();
        hImagePath = currentQuestion.getHintImagePath();
        if (!question.isIsQuestionAsImage()) {
            hideQuestionImage();
        } else {
            showQuestionImage();
            loadImage(question.getQuestionImagePath(), lblQuestionAsImage);
        }
        if (!question.isIsHintAsImage()) {
            lblHintImage.setVisible(false);
        } else {
            lblHintImage.setVisible(true);
            loadImage(question.getHintImagePath(), lblHintImage);
        }
        if (!question.isIsOptionAsImage()) {
            showOptions();
            setLableText(lblOptionA, question.getOptionA());
            setLableText(lblOptionB, question.getOptionB());
            setLableText(lblOptionC, question.getOptionC());
            setLableText(lblOptionD, question.getOptionD());
        } else {
            hideOptions();
            loadImage(question.getOptionImagePath(), lblOptionAsImage);
        }

        String userAnswer = question.getAnswer();
        if (userAnswer.equals("UnAttempted")) {
        } else {
            if (userAnswer.equals("A")) {
                lblAns.setText("A");
            } else if (userAnswer.equals("B")) {
                lblAns.setText("B");
            } else if (userAnswer.equals("C")) {
                lblAns.setText("C");
            }
            if (userAnswer.equals("D")) {
                lblAns.setText("D");
            }
        }
        lblLevel.setVisible(false);
        lblYear.setVisible(false);
        if (question.getHint().equals("\\mbox{}")) {
            lblHint.setVisible(false);
            lblQHint.setVisible(false);
        } else {
            lblHint.setVisible(true);
            lblQHint.setVisible(true);
            setLableText(lblQHint, question.getHint());
        }
        lblQuestionNo.setText("Q. " + index);
    }
    
//    Image bg = new ImageIcon(getClass().getResource("/ui/images/Background.jpg")).getImage();
//    
//    
//    @Override
//    public void paintComponent(Graphics g) {
//        g.drawImage(bg, 0, 0, getWidth(), getHeight(), this);
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        lblQuestionNo = new javax.swing.JLabel();
        lblQuestion = new javax.swing.JLabel();
        lblQuestionAsImage = new javax.swing.JLabel();
        lblQuestionNo6 = new javax.swing.JLabel();
        lblOptionA = new javax.swing.JLabel();
        lblOptionAsImage = new javax.swing.JLabel();
        lblB = new javax.swing.JLabel();
        lblOptionB = new javax.swing.JLabel();
        lblC = new javax.swing.JLabel();
        lblOptionC = new javax.swing.JLabel();
        lblD = new javax.swing.JLabel();
        lblOptionD = new javax.swing.JLabel();
        lblA = new javax.swing.JLabel();
        lblQuestionNo5 = new javax.swing.JLabel();
        lblLevel = new javax.swing.JLabel();
        lblYear = new javax.swing.JLabel();
        lblQHint = new javax.swing.JLabel();
        lblHint = new javax.swing.JLabel();
        lblAns = new javax.swing.JLabel();
        lblHintImage = new javax.swing.JLabel();
        lblNumericalAnswer = new javax.swing.JLabel();
        txtNumericalAnswer = new javax.swing.JLabel();

        setBackground(new java.awt.Color(204, 255, 255));
        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setAutoscrolls(true);

        lblQuestionNo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo.setText("Q. 1");
        lblQuestionNo.setName("lblQuestionNo"); // NOI18N

        lblQuestion.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblQuestion.setText("Question"); // NOI18N
        lblQuestion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestion.setName("lblQuestion"); // NOI18N

        lblQuestionAsImage.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblQuestionAsImage.setText("Question Image");
        lblQuestionAsImage.setToolTipText("Click Here For Zoom Image.");
        lblQuestionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblQuestionAsImage.setName("lblQuestionAsImage"); // NOI18N
        lblQuestionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQuestionAsImageMouseClicked(evt);
            }
        });

        lblQuestionNo6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo6.setText("Options :");
        lblQuestionNo6.setName("lblQuestionNo6"); // NOI18N

        lblOptionA.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblOptionA.setText("jLabel7");
        lblOptionA.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionA.setName("lblOptionA"); // NOI18N

        lblOptionAsImage.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblOptionAsImage.setText("Option Image");
        lblOptionAsImage.setToolTipText("Click Here For Zoom Image.");
        lblOptionAsImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionAsImage.setName("lblOptionAsImage"); // NOI18N
        lblOptionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblOptionAsImageMouseClicked(evt);
            }
        });

        lblB.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblB.setText("B :");
        lblB.setName("lblB"); // NOI18N

        lblOptionB.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblOptionB.setText("jLabel7");
        lblOptionB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionB.setName("lblOptionB"); // NOI18N

        lblC.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblC.setText("C :");
        lblC.setName("lblC"); // NOI18N

        lblOptionC.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblOptionC.setText("jLabel7");
        lblOptionC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionC.setName("lblOptionC"); // NOI18N

        lblD.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblD.setText("D :");
        lblD.setName("lblD"); // NOI18N

        lblOptionD.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblOptionD.setText("jLabel7");
        lblOptionD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblOptionD.setName("lblOptionD"); // NOI18N

        lblA.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblA.setText("A :");
        lblA.setName("lblA"); // NOI18N

        lblQuestionNo5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQuestionNo5.setText("Correct Answer:");
        lblQuestionNo5.setName("lblQuestionNo5"); // NOI18N

        lblLevel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblLevel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/level_Default.png"))); // NOI18N
        lblLevel.setName("lblLevel"); // NOI18N

        lblYear.setFont(new java.awt.Font("Tahoma", 1, 17)); // NOI18N
        lblYear.setText("jLabel1");
        lblYear.setName("lblYear"); // NOI18N

        lblQHint.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblQHint.setText("Hint:");
        lblQHint.setName("lblQHint"); // NOI18N

        lblHint.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblHint.setText("Hint:");
        lblHint.setName("lblHint"); // NOI18N

        lblAns.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblAns.setText("A");
        lblAns.setName("lblAns"); // NOI18N

        lblHintImage.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lblHintImage.setText("Option Image");
        lblHintImage.setToolTipText("Click Here For Zoom Image.");
        lblHintImage.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblHintImage.setName("lblHintImage"); // NOI18N
        lblHintImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblHintImageMouseClicked(evt);
            }
        });

        lblNumericalAnswer.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblNumericalAnswer.setText("Numerical Answer:");
        lblNumericalAnswer.setName("lblNumericalAnswer"); // NOI18N

        txtNumericalAnswer.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtNumericalAnswer.setText("jLabel3");
        txtNumericalAnswer.setName("txtNumericalAnswer"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblNumericalAnswer)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNumericalAnswer, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo5)
                        .addGap(18, 18, 18)
                        .addComponent(lblAns))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo)
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblQuestionAsImage)
                            .addComponent(lblQuestion)
                            .addComponent(lblLevel)
                            .addComponent(lblYear)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblB)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionB))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionA))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblC)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionC))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblD)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionD))
                            .addComponent(lblOptionAsImage)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblHint)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblHintImage)
                            .addComponent(lblQHint))))
                .addContainerGap(349, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo)
                    .addComponent(lblQuestion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQuestionAsImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblYear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblLevel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNumericalAnswer)
                    .addComponent(txtNumericalAnswer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo6)
                    .addComponent(lblA)
                    .addComponent(lblOptionA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB)
                    .addComponent(lblOptionB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblC)
                    .addComponent(lblOptionC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblD)
                    .addComponent(lblOptionD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOptionAsImage)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo5)
                    .addComponent(lblAns))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblHint)
                    .addComponent(lblQHint))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblHintImage))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lblQuestionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQuestionAsImageMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new ZoomImage(qImagePath).setVisible(true);
            }
        }

    }//GEN-LAST:event_lblQuestionAsImageMouseClicked

    private void lblOptionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOptionAsImageMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new ZoomImage(oImagePath).setVisible(true);

                System.out.println(oImagePath);
            }
        }
    }//GEN-LAST:event_lblOptionAsImageMouseClicked

    private void lblHintImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblHintImageMouseClicked
switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                new ZoomImage(hImagePath).setVisible(true);
            }
        }        // TODO add your handling code here:
    }//GEN-LAST:event_lblHintImageMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel lblA;
    private javax.swing.JLabel lblAns;
    private javax.swing.JLabel lblB;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblD;
    private javax.swing.JLabel lblHint;
    private javax.swing.JLabel lblHintImage;
    private javax.swing.JLabel lblLevel;
    private javax.swing.JLabel lblNumericalAnswer;
    private javax.swing.JLabel lblOptionA;
    private javax.swing.JLabel lblOptionAsImage;
    private javax.swing.JLabel lblOptionB;
    private javax.swing.JLabel lblOptionC;
    private javax.swing.JLabel lblOptionD;
    private javax.swing.JLabel lblQHint;
    private javax.swing.JLabel lblQuestion;
    private javax.swing.JLabel lblQuestionAsImage;
    private javax.swing.JLabel lblQuestionNo;
    private javax.swing.JLabel lblQuestionNo5;
    private javax.swing.JLabel lblQuestionNo6;
    private javax.swing.JLabel lblYear;
    private javax.swing.JLabel txtNumericalAnswer;
    // End of variables declaration//GEN-END:variables
}
