/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 *
 * @author Scholars Katta
 */
public class DbConnectionForImport {
    public Connection getSorceConnection(String MergePathFrom){
        // Load the JDBC Driver
        String DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
//      String DERBY_EMBEDDED_URL="jdbc:derby:MERGEDBFROM;";
//     
//      String DERBY_EMBEDDED_URL="jdbc:derby:E:\\Aniket\\E Drive\\MH-CET\\Mearge(for android PCMB)\\CruncherMH-CET-PCB\\CruncherPrintingPCBFrom;";
       
//      String DERBY_EMBEDDED_URL = "jdbc:derby:E:\\Aniket\\D Drive\\CrunchMerge\\new merge db(12-12-2018)\\Board PCM and PCB DB(11th)\\Board Data Entry PCB (NEW PATTERN)\\MERGEDBFrom;";       
//      String DERBY_EMBEDDED_URL = "jdbc:derby:E:\\Aniket\\D Drive\\CrunchMerge\\DATATRANSFERUTILITYMHCETMERGE\\CruncherPrintingJEEFrom";  //from
        String DERBY_EMBEDDED_URL = "jdbc:derby:" + MergePathFrom+ "";  //from
        try{
            Class.forName(DERBY_EMBEDDED_DRIVER).newInstance();
        } 
        catch (Exception e) {
            System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
            e.printStackTrace();
            System.exit(1);  //critical error, so exit
        }
        Properties properties = new java.util.Properties();		
        properties.setProperty("user","root");
        properties.setProperty("password","root");

        // Get database connection via DriverManager api
        try{			
            Connection conn = (Connection) DriverManager.getConnection(DERBY_EMBEDDED_URL, "root", "root");
//                    System.out.println("Embedded Connection request Successful ");
            return conn;
        } 
        catch(Exception e){
                System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
                System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
                e.printStackTrace();
                System.exit(1);  //critical error, so exit
          }

        return null;
    }
    
    public Connection getDestConnection(){
        // Load the JDBC Driver
        String DERBY_EMBEDDED_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
       // String DERBY_EMBEDDED_URL="jdbc:derby:CruncherPrintingPCB;";
//        String DERBY_EMBEDDED_URL="jdbc:derby:E:\\Aniket\\E Drive\\MH-CET\\Mearge(for android PCMB)\\CruncherMH-CET-PCM\\CruncherPrintingPCMTo";
//        String DERBY_EMBEDDED_URL="jdbc:derby:E:\\Aniket\\D Drive\\CrunchMerge\\DATATRANSFERUTILITYMHCETMERGE\\CruncherPrintingJEETo";     //To
         String DERBY_EMBEDDED_URL="jdbc:derby:MERGEDB;";     //To
        
        try{
            Class.forName(DERBY_EMBEDDED_DRIVER).newInstance();
        } 
        catch (Exception e) {
            System.out.println("[NsSample] Unable to load the JDBC driver. Following exception was thrown");
            e.printStackTrace();
            System.exit(1);  //critical error, so exit
        }
        Properties properties = new java.util.Properties();		
        properties.setProperty("user","root");
        properties.setProperty("password","root");

        // Get database connection via DriverManager api
        try{			
            Connection conn = (Connection) DriverManager.getConnection(DERBY_EMBEDDED_URL, "root", "root");
//                    System.out.println("Embedded Connection request Successful ");
            return conn;
        } 
        catch(Exception e){
                System.out.println("[NsSample] Connection request unsuccessful, exception thrown was: ");
                System.out.println("[NsSample] Please check if all the jar files are in the classpath and the dbUrl is set correctly.");
                e.printStackTrace();
                System.exit(1);  //critical error, so exit
          }

        return null;
    }
}
