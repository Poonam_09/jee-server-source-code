/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.LatexProcessing.pdf;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class NewHintLatexProcessing {
    
    public String mboxProcessing(String str){
        String returnString = str;
        //Newly Added
        returnString=  returnString.replace("–", "-");
        returnString=  returnString.replace("…...", "...");
        returnString=  returnString.replace("…", "...");
        returnString = returnString.replace("\\hspace{15pt}", "");
        
        returnString = returnString.replace("\\textit", "tteextitt"); 
        returnString = returnString.replace("\\textbf", "tteextbff");        
        
        returnString = returnString.replace("\\text", "\\mbox");
        
        returnString = returnString.replace("tteextitt", "\\textit"); 
        returnString = returnString.replace("tteextbff", "\\textbf");
        
        returnString = returnString.replace("  ", " ");//1
        returnString = returnString.replace("  ", " ");//2
        returnString = returnString.replace("} \\\\ \\mbox{", "}\\\\\\mbox{");//3
        returnString = returnString.replace("}\\\\ \\mbox{","}\\\\\\mbox{");//4
        returnString = returnString.replace("} \\\\\\mbox{", "}\\\\\\mbox{");//5
        returnString = returnString.replace("}  \\\\ \\mbox{", "}\\\\\\mbox{");//6
        returnString = returnString.replace("}   \\\\ \\mbox{", "}\\\\\\mbox{");//7
        returnString = returnString.replace("} \\\\  \\mbox{", "}\\\\\\mbox{");//8
        returnString = returnString.replace("\\_", "_");//9
        
        returnString = removeMboxHint(returnString);
        returnString = returnString.trim();
        returnString = refineThereForeSolution(returnString);
        
        return returnString;
    }
    
    //Processing Methods
    private String removeMboxHint(String str) { 
        str = str.replace("\\mbox {", "\\mbox{");
        str = "  "+str+"  ";
        char [] carray = str.toCharArray();
        char [] tarray = new char[str.length()+7] ;
        int clen = str.length();
        int k = 0,j = 0;int flag = 0;
        for(int i=0;i<clen;) {
            if(carray[i] == '\\'&& carray[i+1] == 'm' && carray[i+2] == 'b' && carray[i+3] == 'o' && carray[i+4] == 'x' && carray[i+5] == '{') {
                if(flag == 1 && carray[i+6]!=' ') {
                    tarray[j]=' ';j++;
                }
                i += 6;
                k = newSingleClosingBrackPosition(str, i-6);
                for(;i<k&&i<clen;) {
                    tarray[j] = carray[i]; i++; j++;
                }
                i++;
                flag = 1;
            } else {
                tarray[j] = carray[i]; i++; j++;
            }
        }
        String s1 = new String(tarray);
        s1 = s1.trim();
        return s1;
    }
    
    private int newSingleClosingBrackPosition(String str,int position) {
        char [] carray = str.toCharArray();
        int clen = carray.length;
        int count = 0;
        int i = position;
        for(;carray[i]!='{';) {
            i++;
        }     
        for(;i<clen;i++) {
            if(carray[i]=='{'&&carray[i-1]!='\\') 
                count+=1;
            if(carray[i]=='}'&&carray[i-1]!='\\') 
                count-=1;
            if(count==0)
                return i;
        }
        return 0;
    }
    
    private String refineThereForeSolution(String str) {
        String returnString = "";
        char[] mainArray = str.toCharArray();
        
        int strLength = mainArray.length;
        ArrayList<Character> finalList = new ArrayList<Character>();
        for(int i=0;i<strLength;i++) {
            if(strLength > (i + 9)) {
                if(mainArray[i] == '\\' && mainArray[i+1] == 't' && mainArray[i+2] == 'h' && 
                    mainArray[i+3] == 'e' && mainArray[i+4] == 'r' && mainArray[i+5] == 'e' && 
                    mainArray[i+6] == 'f' && mainArray[i+7] == 'o' && mainArray[i+8] == 'r' && mainArray[i+9] == 'e') {
                    if(getDollarCount(finalList) % 2 == 0) {
                    
                        finalList.add('$');
                        finalList.add('\\');finalList.add('t');finalList.add('h');finalList.add('e');finalList.add('r');
                        finalList.add('e');finalList.add('f');finalList.add('o');finalList.add('r');finalList.add('e');
                        finalList.add('$');
                        i+=9;
                    } else {
                        finalList.add('\\');finalList.add('t');finalList.add('h');finalList.add('e');finalList.add('r');
                        finalList.add('e');finalList.add('f');finalList.add('o');finalList.add('r');finalList.add('e');
                        i+=9;
                    }
                    
                } else {
                    finalList.add(mainArray[i]);    
                }
            } else {
                finalList.add(mainArray[i]);
            }
        }
        if(finalList.size() != 0) {
            for(char ch : finalList)
                returnString += ch;
        } else {
            returnString = str;
        }
        return returnString;
    }
    
    private int getDollarCount(ArrayList<Character> charList) {
        int dollarcount = 0;
        for(char ch : charList) {
            if(ch == '$')
                dollarcount++;
        }
        return dollarcount;
    }
}
