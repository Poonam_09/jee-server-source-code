/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author admin
 */
public class SubjectBean {
    private int subjectId;
    private String subjectName;
    private Double marksPerQuestion;
    private Double marksPerWrong;
    private boolean status;

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Double getMarksPerQuestion() {
        return marksPerQuestion;
    }

    public void setMarksPerQuestion(Double marksPerQuestion) {
        this.marksPerQuestion = marksPerQuestion;
    }

    public Double getMarksPerWrong() {
        return marksPerWrong;
    }

    public void setMarksPerWrong(Double marksPerWrong) {
        this.marksPerWrong = marksPerWrong;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
}
