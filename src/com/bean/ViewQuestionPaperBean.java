/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

/**
 *
 * @author Aniket
 */
public class ViewQuestionPaperBean {
    private String previousYear;
    private int totalQues;
    private int subjectFirstQues;
    private int subjectSecondQues;
    private int subjectThirdQues;
    private int usedQues;

    public String getPreviousYear() {
        return previousYear;
    }

    public void setPreviousYear(String previousYear) {
        this.previousYear = previousYear;
    }

    public int getTotalQues() {
        return totalQues;
    }

    public void setTotalQues(int totalQues) {
        this.totalQues = totalQues;
    }

    public int getSubjectFirstQues() {
        return subjectFirstQues;
    }

    public void setSubjectFirstQues(int subjectFirstQues) {
        this.subjectFirstQues = subjectFirstQues;
    }

    public int getSubjectSecondQues() {
        return subjectSecondQues;
    }

    public void setSubjectSecondQues(int subjectSecondQues) {
        this.subjectSecondQues = subjectSecondQues;
    }

    public int getSubjectThirdQues() {
        return subjectThirdQues;
    }

    public void setSubjectThirdQues(int subjectThirdQues) {
        this.subjectThirdQues = subjectThirdQues;
    }

    public int getUsedQues() {
        return usedQues;
    }

    public void setUsedQues(int usedQues) {
        this.usedQues = usedQues;
    } 
}
