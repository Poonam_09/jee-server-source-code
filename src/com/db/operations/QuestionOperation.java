/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.Model.ProcessManager;
import com.bean.ChangeIdReferenceBean;
import com.bean.ChapterBean;
import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.bean.SubMasterChapterIdReferenceBean;
import com.bean.SubjectBean;
import com.db.DbConnection;
import com.id.operations.NewIdOperation;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class QuestionOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<QuestionBean> getQuestuionsSubjectWise(int subjectId) {
        ArrayList<QuestionBean> returnList = new ArrayList<QuestionBean>();
        try {
            conn = new DbConnection().getConnection();
            String query = "Select * from Question_Info where subject_id=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, subjectId);
            rs = ps.executeQuery();
            QuestionBean bean;
            
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));  
                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList.clear();
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
       public ArrayList<QuestionBean> getOptionQuestuionsSubjectWise(int subjectId) {
        ArrayList<QuestionBean> returnList = new ArrayList<QuestionBean>();
        try {
            conn = new DbConnection().getConnection();
            String query = "Select * from Question_Info where Question_Type!=2 and subject_id=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, subjectId);
            rs = ps.executeQuery();
            QuestionBean bean;
            
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));   
                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList.clear();
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
  
    public ArrayList<QuestionBean> getQuestuionsChapterWise(int chapterId){
        ArrayList<QuestionBean> returnList = new ArrayList<QuestionBean>();
        try {
            conn = new DbConnection().getConnection();
            String query = "Select * from Question_Info where chapter_id=?";
            ps=conn.prepareStatement(query);
            ps.setInt(1, chapterId);
            rs=ps.executeQuery();
            QuestionBean bean;
            while (rs.next()) {
                bean=new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));  
                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList.clear();
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    
    public ArrayList<QuestionBean> getNumQuestuionsSubjectWise(int subjectId) {
        ArrayList<QuestionBean> returnList = new ArrayList<QuestionBean>();
        try {
            conn = new DbConnection().getConnection();
            String query = "Select * from Question_Info where Question_Type=2 and subject_id=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, subjectId);
            rs = ps.executeQuery();
            QuestionBean bean;
            
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));   
                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList.clear();
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public ArrayList<QuestionBean> getQuestuionsUnitWise(ArrayList<ChapterBean> chaptersIdList){
        ArrayList<QuestionBean> viewQuestionList = new ArrayList<QuestionBean>();
        try {
            conn = new DbConnection().getConnection();
//            String query = "select * from QUESTION_INFO where CHAPTER_ID = ? AND (ISQUESTIONASIMAGE = true or ISOPTIONASIMAGE = true or ISHINTASIMAGE = true)";
            String query = "Select * from Question_Info where chapter_id=?";
            ps=conn.prepareStatement(query);
            for(ChapterBean chapterBean : chaptersIdList) {
                ps.setInt(1, chapterBean.getChapterId());
                rs=ps.executeQuery();
                QuestionBean bean;
                while (rs.next()) {
                    bean=new QuestionBean();
                    bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                    bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                    bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                    bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                    bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                    bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                    bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                    bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                    bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                    bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                    bean.setYear(rs.getString(21));                  bean.setSelected(false);
                    bean.setNumericalAnswer(rs.getString(22));       bean.setUnit(rs.getString(23));   
                    viewQuestionList.add(bean);
                }
            }
        } catch (Exception ex) {
            viewQuestionList.clear();
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return viewQuestionList;
    }
    
    
    public boolean updateModifyQuestion(QuestionBean questionBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        System.out.println("Modified into Question Info");
        try{
            String query = "UPDATE QUESTION_INFO SET QUESTION = ?,OPTIONA = ?,OPTIONB = ?,OPTIONC = ?,OPTIOND = ?,"
                    + "ANSWER = ?,HINT = ?,QUESTION_LEVEL = ?,CHAPTER_ID = ?,TOPIC_ID = ?,ISQUESTIONASIMAGE = ?,"
                    + "QUESTIONIMAGEPATH = ?,ISOPTIONASIMAGE = ?,OPTIONIMAGEPATH = ?,ISHINTASIMAGE = ?,HINTIMAGEPATH = ?,"
                    + "QUESTION_TYPE = ?,QUESTION_YEAR = ?, NUMERICAL_ANSWER=?,UNIT=? WHERE QUESTION_ID = ?";
            ps=conn.prepareStatement(query);
            ps.setString(1, questionBean.getQuestion());
            ps.setString(2, questionBean.getOptionA());
            ps.setString(3, questionBean.getOptionB());
            ps.setString(4, questionBean.getOptionC());
            ps.setString(5, questionBean.getOptionD());
            ps.setString(6, questionBean.getAnswer());
            ps.setString(7, questionBean.getHint());
            ps.setInt(8, questionBean.getLevel());
            ps.setInt(9, questionBean.getChapterId());
            ps.setInt(10, questionBean.getTopicId());
            ps.setBoolean(11, questionBean.isIsQuestionAsImage());
            ps.setString(12, questionBean.getQuestionImagePath());
            ps.setBoolean(13, questionBean.isIsOptionAsImage());
            ps.setString(14, questionBean.getOptionImagePath());
            ps.setBoolean(15, questionBean.isIsHintAsImage());
            ps.setString(16, questionBean.getHintImagePath());
            ps.setInt(17, questionBean.getType());
            ps.setString(18, questionBean.getYear());
            ps.setString(19,questionBean.getNumericalAnswer());
            System.out.println("questionBean.getNumericalAnswer()---"+questionBean.getNumericalAnswer());
            ps.setString(20,questionBean.getUnit());
            ps.setInt(21, questionBean.getQuestionId());
            ps.executeUpdate();
            System.out.println("QUESTION_TYPE-----------"+questionBean.getType());
            returnValue = true;
        }
        catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    
    public boolean insertQuestion(QuestionBean questionBean) {
        boolean returnValue = false;
        conn = new DbConnection().getConnection();
        try{
            String query = "Insert into Question_Info values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps=conn.prepareStatement(query);
            ps.setInt(1, questionBean.getQuestionId());
            ps.setString(2, questionBean.getQuestion());
            ps.setString(3, questionBean.getOptionA());
            ps.setString(4, questionBean.getOptionB());
            ps.setString(5, questionBean.getOptionC());
            ps.setString(6, questionBean.getOptionD());
            ps.setString(7, questionBean.getAnswer());
            ps.setString(8, questionBean.getHint());
            ps.setInt(9, questionBean.getLevel());
            ps.setInt(10, questionBean.getSubjectId());
            ps.setInt(11, questionBean.getChapterId());
            ps.setInt(12, questionBean.getTopicId());
            ps.setBoolean(13, questionBean.isIsQuestionAsImage());
            ps.setString(14, questionBean.getQuestionImagePath());
            ps.setBoolean(15, questionBean.isIsOptionAsImage());
            ps.setString(16, questionBean.getOptionImagePath());
            ps.setBoolean(17, questionBean.isIsHintAsImage());
            ps.setString(18, questionBean.getHintImagePath());
            ps.setInt(19, questionBean.getAttempt());
            ps.setInt(20, questionBean.getType());
            ps.setString(21,questionBean.getYear());
            ps.setString(22,questionBean.getNumericalAnswer());
            ps.setString(23, questionBean.getUnit());
            ps.executeUpdate();
            returnValue = true;
        }
        catch(Exception ex) {
            returnValue = false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
     
   
    public ArrayList<QuestionBean> getQuestuionsList() {
        ArrayList<QuestionBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM QUESTION_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            QuestionBean bean = null;
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                 bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                 bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                 bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                  bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                      bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                 bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));     bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));       bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));         bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                   bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                   bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));        bean.setUnit(rs.getString(23)); 
                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();
                
                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public boolean deleteQue(QuestionBean deleteQuestionBean) {
        boolean returnValue = false;
        String processPath = new ProcessManager().getProcessPath();
        try {
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM QUESTION_INFO WHERE QUESTION_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, deleteQuestionBean.getQuestionId());
            ps.execute();
            
            query = "DELETE FROM OPTION_IMAGE_DIMENSIONS WHERE QUEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, deleteQuestionBean.getQuestionId());
            ps.execute();
            
            ArrayList<String> imageList = null;
            if(deleteQuestionBean.isIsQuestionAsImage() || deleteQuestionBean.isIsHintAsImage() || deleteQuestionBean.isIsOptionAsImage()) {
                imageList = new ArrayList<String>();
                if(deleteQuestionBean.isIsQuestionAsImage())
                    imageList.add(deleteQuestionBean.getQuestionImagePath());
                
                if(deleteQuestionBean.isIsOptionAsImage())
                    imageList.add(deleteQuestionBean.getOptionImagePath());
                
                if(deleteQuestionBean.isIsHintAsImage())
                    imageList.add(deleteQuestionBean.getHintImagePath());
                
                new ImageRatioOperation().deleteImageNameList(imageList);
            }
            
            if(deleteQuestionBean.isIsQuestionAsImage())
                deleteImageFile(processPath, deleteQuestionBean.getQuestionImagePath());
            
            if(deleteQuestionBean.isIsOptionAsImage())
                deleteImageFile(processPath, deleteQuestionBean.getOptionImagePath());

            if(deleteQuestionBean.isIsHintAsImage())
                deleteImageFile(processPath, deleteQuestionBean.getHintImagePath());
            
            returnValue = true;
        } catch (SQLException ex) {
            returnValue=false;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;    
    }
    
    public boolean deleteQuestion (QuestionBean deleteQuestionBean) {
        ArrayList<QuestionBean> questionsList = getQuestuionsList();
        boolean returnValue = false;
        String processPath = new ProcessManager().getProcessPath();
        int lastQuesId = 1;
        
        if (questionsList != null)
            lastQuesId = questionsList.size();
        
        int currentQueId = deleteQuestionBean.getQuestionId();
        
        String qim = "", him = "", oim = "";
//        questionList.remove(deleteQuestionBean);
        try {
            
            conn = new DbConnection().getConnection();
            String query = "DELETE FROM OPTION_IMAGE_DIMENSIONS WHERE QUEST_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentQueId);
            ps.execute();
            
            query = "DELETE FROM QUESTION_INFO WHERE QUESTION_ID = ?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, currentQueId);
            returnValue = ps.execute();
            
            ArrayList<String> imageList = null;
            
            if(deleteQuestionBean.isIsQuestionAsImage() || deleteQuestionBean.isIsHintAsImage() || deleteQuestionBean.isIsOptionAsImage()) {
                imageList = new ArrayList<String>();
                if(deleteQuestionBean.isIsQuestionAsImage())
                    imageList.add(deleteQuestionBean.getQuestionImagePath());
                
                if(deleteQuestionBean.isIsOptionAsImage())
                    imageList.add(deleteQuestionBean.getOptionImagePath());
                
                if(deleteQuestionBean.isIsHintAsImage())
                    imageList.add(deleteQuestionBean.getHintImagePath());
                
                new ImageRatioOperation().deleteImageNameList(imageList);
            }
            
            if(deleteQuestionBean.isIsQuestionAsImage()) {
                deleteImageFile(processPath, deleteQuestionBean.getQuestionImagePath());
            }
            
            if(deleteQuestionBean.isIsOptionAsImage()) {
                deleteImageFile(processPath, deleteQuestionBean.getOptionImagePath());
            }

            if(deleteQuestionBean.isIsHintAsImage()) {
                deleteImageFile(processPath, deleteQuestionBean.getHintImagePath());
            }
            
            if(!returnValue) {
                if(currentQueId != lastQuesId) {
                    ArrayList<String> imageNameList = new ArrayList<String>();
                    for(int i=currentQueId; i<lastQuesId ;i++) {
                        QuestionBean questionsBean = questionsList.get(i);
//                        QuestionBean updateQuestionBean = null;
//                        for(QuestionBean qb : questionList) {
//                            if(qb.getQuestionId() == (i+1)) {
//                                updateQuestionBean = qb;
//                                updateQuestionBean.setQuestionId(i);
//                                break;
//                            }
//                        }
                        
                        File oldfile = null;
                        File newfile = null;
                        if(imageNameList.size() != 0)
                            imageNameList.clear();
                        
                        String quesPath = "";
                        if(questionsBean.isIsQuestionAsImage()) {
                            quesPath = "images/"+questionsBean.getChapterId()+"Question"+i+".png";
                            oldfile = new File(questionsBean.getQuestionImagePath());
                            newfile = new File(quesPath);
                            imageNameList.add(questionsBean.getQuestionImagePath()+","+quesPath);
//                            if(updateQuestionBean != null)
//                                updateQuestionBean.setQuestionImagePath(quesPath);
                            try {
                                oldfile.renameTo(newfile);
                            } catch(Exception ex) {
                                System.out.println("Error In Question Local");
                            }
                            oldfile = new File(processPath+"/"+questionsBean.getQuestionImagePath());
                            newfile = new File(processPath+"/"+quesPath);
                            try {
                                oldfile.renameTo(newfile);
                            } catch(Exception ex) {
                                System.out.println("Error In Question C Drive");
                            }
                        }
                        
                        String optPath = "";
                        if(questionsBean.isIsOptionAsImage()) {
                            optPath = "images/"+questionsBean.getChapterId()+"Option"+i+".png";
                            oldfile = new File(questionsBean.getOptionImagePath());
                            newfile = new File(optPath);
                            imageNameList.add(questionsBean.getOptionImagePath()+","+optPath);
//                            if(updateQuestionBean != null)
//                                updateQuestionBean.setOptionImagePath(optPath);
                            try {
                                oldfile.renameTo(newfile);
                            } catch(Exception ex) {
                                System.out.println("Error In Option Local");
                            }
                            oldfile = new File(processPath+"/"+questionsBean.getOptionImagePath());
                            newfile = new File(processPath+"/"+optPath);
                            try {
                                oldfile.renameTo(newfile);
                            } catch(Exception ex) {
                                System.out.println("Error In Option C Drive");
                            }
                        }
                        
                        String hintPath = "";
                        if(questionsBean.isIsHintAsImage()) {
                            hintPath = "images/"+questionsBean.getChapterId()+"Hint"+i+".png";
                            oldfile = new File(questionsBean.getHintImagePath());
                            newfile = new File(hintPath);
                            imageNameList.add(questionsBean.getHintImagePath()+","+hintPath);
//                            if(updateQuestionBean != null)
//                                updateQuestionBean.setHintImagePath(hintPath);
                            try {
                                oldfile.renameTo(newfile);
                            } catch(Exception ex) {
                                System.out.println("Error In Hint Local");
                            }
                            oldfile = new File(processPath+"/"+questionsBean.getHintImagePath());
                            newfile = new File(processPath+"/"+hintPath);
                            try {
                                oldfile.renameTo(newfile);
                            } catch(Exception ex) {
                                System.out.println("Error In Hint C Drive");
                            }
                        }
                        
//		if(oldfile.renameTo(newfile))
                        query = "UPDATE QUESTION_INFO SET QUESTION_ID = ?,QUESTIONIMAGEPATH = ?,OPTIONIMAGEPATH = ?,HINTIMAGEPATH = ? where QUESTION_ID = ?";
//                        query = "update Question_Info set Question_Id = ? where Question_Id = ?";
                        ps=conn.prepareStatement(query);
                        ps.setInt(1, i);
                        ps.setString(2, quesPath);
                        ps.setString(3, optPath);
                        ps.setString(4, hintPath);
                        ps.setInt(5, (i+1));
                        ps.executeUpdate();
                        
                        query = "UPDATE OPTION_IMAGE_DIMENSIONS SET QUEST_ID = ? where QUEST_ID = ?";
                        ps=conn.prepareStatement(query);
                        ps.setInt(1, i);
                        ps.setInt(2, (i+1));
                        ps.executeUpdate();
                        
                        query = "UPDATE IMAGERATIO SET IMAGENAME = ? WHERE IMAGENAME = ?";
                        for(String fileName : imageNameList) {
                            String[] tempArray = fileName.split(",");
                            ps=conn.prepareStatement(query);
                            ps.setString(1, tempArray[1].trim());
                            ps.setString(2, tempArray[0].trim());
                            ps.executeUpdate();
                        }
                    }
                }
            }
        
            return returnValue;
            
        } catch (SQLException ex) {
            returnValue=true;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnValue;
    }
    
    public ArrayList<Integer> getSubjectWiseQuestionCount(ArrayList<SubjectBean> subjectList) {
        ArrayList<Integer> returnList = new ArrayList<Integer>();
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT COUNT(*) FROM QUESTION_INFO WHERE SUBJECT_ID = ?";
            ps = conn.prepareStatement(query);
            for(SubjectBean subjectBean : subjectList) {
                ps.setInt(1, subjectBean.getSubjectId());
                rs = ps.executeQuery();
                while(rs.next()) {
                    returnList.add(rs.getInt(1));
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        
        return returnList;
    }
    
    public ArrayList<QuestionBean> getTestQuestuionsList(ArrayList<Integer> quesIdList) {
        ArrayList<QuestionBean> returnList = null;
        try {
            conn = new DbConnection().getConnection();
            String query = "SELECT * FROM QUESTION_INFO WHERE QUESTION_ID = ?";
            ps = conn.prepareStatement(query);
            for(int queId : quesIdList) {
                ps.setInt(1, queId);
                rs = ps.executeQuery();

                QuestionBean bean = null;
                
                while (rs.next()) {
                    bean = new QuestionBean();
                    bean.setQuestionId(rs.getInt(1));                 bean.setQuestion(rs.getString(2));
                    bean.setOptionA(rs.getString(3));                 bean.setOptionB(rs.getString(4));
                    bean.setOptionC(rs.getString(5));                 bean.setOptionD(rs.getString(6));
                    bean.setAnswer(rs.getString(7));                  bean.setHint(rs.getString(8));
                    bean.setLevel(rs.getInt(9));                      bean.setSubjectId(rs.getInt(10));
                    bean.setChapterId(rs.getInt(11));                 bean.setTopicId(rs.getInt(12));
                    bean.setIsQuestionAsImage(rs.getBoolean(13));     bean.setQuestionImagePath(rs.getString(14));
                    bean.setIsOptionAsImage(rs.getBoolean(15));       bean.setOptionImagePath(rs.getString(16));
                    bean.setIsHintAsImage(rs.getBoolean(17));         bean.setHintImagePath(rs.getString(18));
                    bean.setAttempt(rs.getInt(19));                   bean.setType(rs.getInt(20));
                    bean.setYear(rs.getString(21));                   bean.setSelected(false);
                    bean.setNumericalAnswer(rs.getString(22));        bean.setUnit(rs.getString(23));
                    if(returnList == null)
                        returnList = new ArrayList<QuestionBean>();

                    returnList.add(bean);
                }
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    private void deleteImageFile(String processPath, String imageName) {
        File f1 = new File(imageName);
        File f2 = new File(processPath + "/" + imageName);
        f1.delete();
        f2.delete();
    }
    
//    private String imageRename(String str,int queId) {
//        
//        
//        File oldfile =new File(str);
//	File newfile =new File(str);
//
//        if(oldfile.renameTo(newfile)) {
//            System.out.println("Rename succesful");
//        } else {
//            System.out.println("Rename failed");
//        }
//        return null;        
//    }
    
    //Pattern Process
    public ArrayList<ChangeIdReferenceBean> insertQuestionList(ArrayList<QuestionBean> questionList,ArrayList<SubMasterChapterIdReferenceBean> changeChapterIdList) {
        String processPath = new ProcessManager().getProcessPath();
        
        ArrayList<ChangeIdReferenceBean> returnList = null;
        ChangeIdReferenceBean changeIdReferenceBean = null;
        int chapterId = 0;
        String queImgPath = "";
        String optImgPath = "";
        String hintImgPath = "";
        double viewDimention = 0.0;
        ArrayList<ImageRatioBean> selectedImageRatioList = new MasterImageRationOperation().getImageRatioList();
        ImageRatioBean imageRatioBean = null;
        ArrayList<ImageRatioBean> newImageRatioList = null;
        int questionId = new NewIdOperation().getNewId("QUESTION_INFO");
        conn = new DbConnection().getConnection();
        try{
            String query = "INSERT INTO QUESTION_INFO VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps=conn.prepareStatement(query);
           
            for(QuestionBean questionBean : questionList) {
//                for(ChangeIdReferenceBean chapterReferenceBean : referenceList) {
//                    if(questionBean.getChapterId() == chapterReferenceBean.getOldId()) {
//                        chapterId = chapterReferenceBean.getNewId();
//                    }
//                }
                for(SubMasterChapterIdReferenceBean idReferenceBean : changeChapterIdList) {
                    String[] strIdArray = idReferenceBean.getMasterChapterIds().split(",");
                    int[] intIdArray = new int[strIdArray.length];
                    for(int i=0;i<strIdArray.length;i++) {
                        intIdArray[i] = Integer.parseInt(strIdArray[i]);
                    }
                    for(int chpId : intIdArray) {
                        if(chpId == questionBean.getChapterId()) {
                            chapterId = idReferenceBean.getNewChapterID();
                        }
                    }
                }
                
//              ps.setInt(1, questionBean.getQuestionId());
                ps.setInt(1, questionId);
                ps.setString(2, questionBean.getQuestion());
                ps.setString(3, questionBean.getOptionA());
                ps.setString(4, questionBean.getOptionB());
                ps.setString(5, questionBean.getOptionC());
                ps.setString(6, questionBean.getOptionD());
                ps.setString(7, questionBean.getAnswer());
                ps.setString(8, questionBean.getHint());
                ps.setInt(9, questionBean.getLevel());
                ps.setInt(10, questionBean.getSubjectId());
                ps.setInt(11, chapterId);
                ps.setInt(12, chapterId);
                
                if(questionBean.isIsQuestionAsImage()) {
//                    queImgPath = "images/"+chapterId+"Question"+questionBean.getQuestionId()+".png";        
                    queImgPath = "images/"+chapterId+"Question"+questionId+".png";
                    ps.setBoolean(13, true);
                    copyFile(questionBean.getQuestionImagePath(), queImgPath);
                    copyFile(questionBean.getQuestionImagePath(), processPath+"/"+queImgPath);
                    
                    viewDimention = getViewDimention(selectedImageRatioList, questionBean.getQuestionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(queImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        if(newImageRatioList == null)
                            newImageRatioList = new ArrayList<ImageRatioBean>();
                        newImageRatioList.add(imageRatioBean);
                    }
                    
                } else {
                    ps.setBoolean(13, false);
                    queImgPath = "";
                }
                ps.setString(14, queImgPath);
        
                if(questionBean.isIsOptionAsImage()) {
//                    optImgPath = "images/"+chapterId+"Option"+questionBean.getQuestionId()+".png";
                    optImgPath = "images/"+chapterId+"Option"+questionId+".png";
                    ps.setBoolean(15, true);
                    copyFile(questionBean.getOptionImagePath(), optImgPath);
                    copyFile(questionBean.getOptionImagePath(), processPath+"/"+optImgPath);
                    viewDimention = getViewDimention(selectedImageRatioList, questionBean.getOptionImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(optImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        if(newImageRatioList == null)
                            newImageRatioList = new ArrayList<ImageRatioBean>();
                        newImageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(15, false);
                    optImgPath = "";
                }
                ps.setString(16, optImgPath);
                
                if(questionBean.isIsHintAsImage()) {
//                    hintImgPath = "images/"+chapterId+"Hint"+questionBean.getQuestionId()+".png";
                    hintImgPath = "images/"+chapterId+"Hint"+questionId+".png";
                    ps.setBoolean(17, true);
                    copyFile(questionBean.getHintImagePath(), hintImgPath);
                    copyFile(questionBean.getHintImagePath(), processPath+"/"+hintImgPath);
                    viewDimention = getViewDimention(selectedImageRatioList, questionBean.getHintImagePath());
                    if(viewDimention != 0.0) {
                        imageRatioBean = new ImageRatioBean();
                        imageRatioBean.setImageName(hintImgPath);
                        imageRatioBean.setViewDimention(viewDimention);
                        if(newImageRatioList == null)
                            newImageRatioList = new ArrayList<ImageRatioBean>();
                        newImageRatioList.add(imageRatioBean);
                    }
                } else {
                    ps.setBoolean(17, false);
                    hintImgPath = "";
                }
                ps.setString(18, hintImgPath);
                ps.setInt(19, 0);
                ps.setInt(20, questionBean.getType());
                ps.setString(21, questionBean.getYear());
                ps.setString(22, questionBean.getNumericalAnswer());
                ps.setString(23, questionBean.getUnit());
                ps.executeUpdate();
                
                changeIdReferenceBean = new ChangeIdReferenceBean();
                changeIdReferenceBean.setOldId(questionBean.getQuestionId());
                changeIdReferenceBean.setNewId(questionId++);
                
                if(returnList == null)
                    returnList = new ArrayList<ChangeIdReferenceBean>();
                
                returnList.add(changeIdReferenceBean);
            }
            
            if(newImageRatioList != null) {
                if(!new ImageRatioOperation().insertImageRatioList(newImageRatioList))
                    JOptionPane.showMessageDialog(null, "Error Insertion of Image Ratio.");
            }
        }
        catch(Exception ex) {
            returnList = null;
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return returnList;
    }
    
    public void updateUsedValue(ArrayList<QuestionBean> questionList) {
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE QUESTION_INFO SET ATTEMPTEDVALUE = ? WHERE QUESTION_ID = ?";
            for(QuestionBean questionBean : questionList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, questionBean.getAttempt());
                ps.setInt(2, questionBean.getQuestionId());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    
    public void copyFile(String sourcePath, String destPath) {
        File sourceFile = new File(sourcePath);
        File destFile = new File(destPath);
        FileChannel source = null;
        FileChannel destination = null;
        try {
            if (!destFile.exists()) {
                destFile.createNewFile();
            }
            source = new RandomAccessFile(sourceFile, "rw").getChannel();
            destination = new RandomAccessFile(destFile, "rw").getChannel();

            long position = 0;
            long count = source.size();

            source.transferTo(position, count, destination);
        } catch(IOException ex) {
            ex.printStackTrace();
        } finally {
            if (source != null) {
                try {
                    source.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if (destination != null) {
                try {
                    destination.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    private double getViewDimention(ArrayList<ImageRatioBean> imageRatioList,String path) {
        double returnValue = 0.0;
        for(ImageRatioBean imageRatioBean : imageRatioList) {
            if(imageRatioBean.getImageName().trim().equalsIgnoreCase(path)) {
                returnValue = imageRatioBean.getViewDimention();
                break;
            }
        }
        return returnValue;
    }
    
    //Refine Process
    
//    public void updateRefinedValue(ArrayList<QuestionBean> questionList) {
//        try {
//            conn = new DbConnection().getConnection();
//            String query = "UPDATE QUESTION_INFO SET QUESTION_LEVEL = ?,QUESTION_TYPE = ? WHERE QUESTION_ID = ?";
//            for(QuestionBean questionBean : questionList) {
//                ps = conn.prepareStatement(query);
//                ps.setInt(1, questionBean.getLevel());
//                ps.setInt(2, questionBean.getType());
//                ps.setInt(3, questionBean.getQuestionId());
//                ps.executeUpdate();
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        } finally {
//            sqlClose();
//        }
//    }
    public void updateRefinedValue(ArrayList<QuestionBean> questionList) {
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE QUESTION_INFO SET QUESTION_LEVEL = ? WHERE QUESTION_ID = ?";
            for(QuestionBean questionBean : questionList) {
                ps = conn.prepareStatement(query);
                ps.setInt(1, questionBean.getLevel());
                ps.setInt(2, questionBean.getQuestionId());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
    }
    public boolean RefreshUsedValue() {
         boolean status = false;
        try {
            conn = new DbConnection().getConnection();
            String query = "UPDATE QUESTION_INFO SET ATTEMPTEDVALUE = ? WHERE ATTEMPTEDVALUE =?";
           
                ps = conn.prepareStatement(query);
                ps.setInt(1, 0);
                ps.setInt(2, 1);
                ps.executeUpdate();
                return  status=true;
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return  status=true;
    }
    
     public ArrayList<QuestionBean> getQuestionList(Connection conn) {
        ArrayList<QuestionBean> returnList = null;
        QuestionBean bean = null;
        try {
            String query = "SELECT * FROM QUESTION_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
                
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));        bean.setUnit(rs.getString(23));
                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();

                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        }
        
        return returnList;
    }
     
    //    For import file..................
    
      public ArrayList<QuestionBean> getMasterQuestionList(Connection conn) {
        ArrayList<QuestionBean> returnList = null;
        QuestionBean bean = null;
        try {
            String query = "SELECT * FROM MASTER_QUESTION_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
                
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);
                bean.setNumericalAnswer(rs.getString(22));        bean.setUnit(rs.getString(23));
                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();

                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        }
        
        return returnList;
    }
    
   
    
    public ArrayList<QuestionBean> getPaperQuestionList(Connection conn) {
        ArrayList<QuestionBean> returnList = null;
        QuestionBean bean = null;
        try {
            String query = "SELECT * FROM QUESTION_PAPER_INFO";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
                
            while (rs.next()) {
                bean = new QuestionBean();
                bean.setQuestionId(rs.getInt(1));                bean.setQuestion(rs.getString(2));
                bean.setOptionA(rs.getString(3));                bean.setOptionB(rs.getString(4));
                bean.setOptionC(rs.getString(5));                bean.setOptionD(rs.getString(6));
                bean.setAnswer(rs.getString(7));                 bean.setHint(rs.getString(8));
                bean.setLevel(rs.getInt(9));                     bean.setSubjectId(rs.getInt(10));
                bean.setChapterId(rs.getInt(11));                bean.setTopicId(rs.getInt(12));
                bean.setIsQuestionAsImage(rs.getBoolean(13));    bean.setQuestionImagePath(rs.getString(14));
                bean.setIsOptionAsImage(rs.getBoolean(15));      bean.setOptionImagePath(rs.getString(16));
                bean.setIsHintAsImage(rs.getBoolean(17));        bean.setHintImagePath(rs.getString(18));
                bean.setAttempt(rs.getInt(19));                  bean.setType(rs.getInt(20));
                bean.setYear(rs.getString(21));                  bean.setSelected(false);

                if(returnList == null)
                    returnList = new ArrayList<QuestionBean>();

                returnList.add(bean);
            }
        } catch (Exception ex) {
            returnList = null;
            ex.printStackTrace();
        }
        
        return returnList;
    }
    
    public void updateRemoveQuesUnwanted(ArrayList<QuestionBean> questionList,Connection conn) {
        try {
            
            String query = "UPDATE QUESTION_INFO SET QUESTION = ?,OPTIONA = ?,OPTIONB = ?,OPTIONC = ?,OPTIOND = ?,HINT = ?,QUESTION_YEAR = ?  WHERE QUESTION_ID = ?";
            for(QuestionBean questionBean : questionList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, questionBean.getQuestion().trim());
                ps.setString(2, questionBean.getOptionA().trim());
                ps.setString(3, questionBean.getOptionB().trim());
                ps.setString(4, questionBean.getOptionC().trim());
                ps.setString(5, questionBean.getOptionD().trim());
                ps.setString(6, questionBean.getHint().trim());
                ps.setString(7, questionBean.getYear().trim());
                ps.setInt(8, questionBean.getQuestionId());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void updateRemoveQuesPaperUnwanted(ArrayList<QuestionBean> questionList,Connection conn) {
        try {
            
            String query = "UPDATE QUESTION_PAPER_INFO SET QUESTION = ?,OPTIONA = ?,OPTIONB = ?,OPTIONC = ?,OPTIOND = ?,HINT = ?,QUESTION_YEAR = ?  WHERE QUESTION_ID = ?";
            for(QuestionBean questionBean : questionList) {
                ps = conn.prepareStatement(query);
                ps.setString(1, questionBean.getQuestion().trim());
                ps.setString(2, questionBean.getOptionA().trim());
                ps.setString(3, questionBean.getOptionB().trim());
                ps.setString(4, questionBean.getOptionC().trim());
                ps.setString(5, questionBean.getOptionD().trim());
                ps.setString(6, questionBean.getHint().trim());
                ps.setString(7, questionBean.getYear().trim());
                ps.setInt(8, questionBean.getQuestionId());
                ps.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    //    For import file.................. 
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
