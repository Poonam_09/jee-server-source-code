/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Model;

import com.LatexProcessing.word.QuestionsProcessing;
import com.Word.Generation.WordFinalPanel;
import com.bean.ChapterBean;
import com.bean.PdfSecondFormatBean;
import com.bean.PrintWordBean;
import com.bean.QuestionBean;
import com.bean.SubjectBean;
import com.bean.WordPageSetupBean;
import com.db.operations.RegistrationOperation;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class PrintWord {
    
    public void setPrintWord(WordPageSetupBean wordPageSetupBean,PdfSecondFormatBean pdfSecondFormatBean,ArrayList<SubjectBean> selectedSubjectList,ArrayList<QuestionBean> selectedQuestionList,String printingPaperType,boolean twoColumnCompatible,boolean corectAnswerSheet,boolean solutionSheet,WordFinalPanel wordFinalPanel,String fileLocation) {
        String instituteName = new RegistrationOperation().getLastRegistrationInfoBean().getInstituteName().trim();
        PrintWordBean printWordBean = new PrintWordBean();
        printWordBean.setInstituteName(instituteName.trim());
        printWordBean.setTimePaperTotalText(getThreeFieldsString(pdfSecondFormatBean.getSelectedTime().trim(), pdfSecondFormatBean.getDivisionName(), ""+pdfSecondFormatBean.getTotalMarks()));
        printWordBean.setTestType(pdfSecondFormatBean.getSubjectName().trim());
        printWordBean.setMainString(new QuestionsProcessing().getMainString(selectedQuestionList, selectedSubjectList, printingPaperType, wordPageSetupBean, twoColumnCompatible, corectAnswerSheet, solutionSheet));
        new ExportProcess().createWord(printWordBean, fileLocation, wordFinalPanel);
    }
        
    private String getThreeFieldsString(String timeDuration, String division, String totalMarks) {
        String finalstr = "";
        timeDuration = "Time : " + timeDuration;
        totalMarks = "Marks : " + totalMarks;

        int i = timeDuration.length();
        int j = division.length();
        int k = totalMarks.length();

        int spacecount = (175 - (i + j + k)) / 5;

        finalstr += timeDuration;
        for (int x = 1; x < spacecount; x++) {
            finalstr = finalstr + " \\ ";
        }

        finalstr += division;
        for (int x = 1; x < spacecount; x++) {
            finalstr = finalstr + " \\ ";
        }
        finalstr += totalMarks;
        return finalstr;
    }
}
