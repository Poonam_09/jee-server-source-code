/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.db.DbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Aniket
 */
public class NetworkPrefixOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt = null;
    
    
     public String getNetworkPrefix() {
         conn = null;
        try {
            conn = new DbConnection().getConnection();
            if (conn != null) {
                String sql = "Select * from Network_Prefix";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    return rs.getString(1);
                }
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(NetworkPrefixOperation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
     
      public void updateNetworkPrefix(String networkPrefix) {
        
        conn = new DbConnection().getConnection();
        try {
            if (conn != null) {
                String sql = "delete from Network_Prefix";
                ps = conn.prepareStatement(sql);
                ps.executeUpdate();
                sql = "insert into Network_Prefix values(?)";
                ps = conn.prepareStatement(sql);
                ps.setString(1, networkPrefix);
                int executeUpdate = ps.executeUpdate();
                if (executeUpdate > 0) {
                    JOptionPane.showMessageDialog(null, "Updated");
                } else {
                    JOptionPane.showMessageDialog(null, "Failed to update");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(NetworkPrefixOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(NetworkPrefixOperation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
