/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Aniket
 */
public class NewIdOperation {
    private Connection conn = null;
    private ResultSet rs = null;
    private Statement stmt = null;
    
    public int getNewMaxId(String tableName,String columnName,Connection conn){
        int newId = 0;
        try {
            stmt = conn.createStatement();
            String query = "SELECT MAX("+columnName+") FROM "+tableName;
            rs=stmt.executeQuery(query);
            while (rs.next()) {
                newId = rs.getInt(1);
            }
            newId ++;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return newId;
    }
    
}
