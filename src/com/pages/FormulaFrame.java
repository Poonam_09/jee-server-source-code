package com.pages;

import javax.swing.*;
import java.awt.event.*;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.Model.TitleInfo;

public class FormulaFrame extends javax.swing.JFrame {

    /**
     * Creates new form FormulaFrame
     */
    int maxPoints;
    String latex, start, end, lastchar;
    TeXFormula formula;
    TeXIcon icon;
    BufferedImage image;
    Graphics2D g2;
    JLabel jl;
    ArrayList<String> lastStr;
    boolean flag = true;
    Connection con;
    Statement st;
    ResultSet rs;
    int i = 1;
    boolean flagc = false;
    boolean flagScreen = true;

    public FormulaFrame() {
        initComponents();
        String titl = new TitleInfo().getTitle();
        setTitle(titl);
        String lgo = new TitleInfo().getLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        start = "\\begin{array}{l}";
        end = "\\end{array}";
        latex = "";
        lastStr = new ArrayList<String>();
        String[] Greek_and_Hebrew_letters1 = {"\\alpha", "\\kappa", "\\psi", "\\digamma", "\\Delta", "\\Theta", "\\beta", "\\lambda", "\\rho", "\\varepsilon", "\\Gamma",
            "\\Upsilon", "\\chi", "\\mu", "\\sigma", "\\varkappa", "\\Lambda", "\\Xi", "\\delta", "\\nu", "\\tau", "\\varphi", "\\Omega", "\\epsilon", "\\theta", "\\varpi",
            "\\Phi", "\\aleph", "\\eta", "\\omega", "\\upsilon", "\\varrho", "\\Pi", "\\beth", "\\gamma", "\\phi", "\\xi", "\\varsigma", "\\Psi", "\\daleth", "\\iota",
            "\\pi", "\\zeta", "\\vartheta", "\\Sigma", "\\gimel"};
        String[] LATEX_math_constructs1 = {"\\frac{abc}{xyz}", "\\overline{abc}", "\\overrightarrow{abc}", "f’", "\\underline{abc}", "\\overleftarrow{abc}", "\\sqrt{abc}",
            "\\widehat{abc}", "\\overbrace{abc}", "\\sqrt[n]{abc}", "\\widetilde{abc}", "\\underbrace{abc}"};
        String[] Delimiters1 = {"|", "\\{", "\\lfloor", "/", "\\Uparrow", "\\llcorner", "\\vert", "\\}", "\\rfloor", "\\backslash", "\\uparrow", "\\lrcorner",
            "\\langle", "\\lceil", "[", "]", "\\Downarrow", "\\ulcorner", "\\Vert", "\\rangle", "\\rceil", "\\downarrow", "\\urcorner", "\\|"};
        String[] Variable_sized_symbols1 = {"\\sum", "\\int", "\\biguplus", "\\bigoplus", "\\bigvee", "\\prod", "\\oint", "\\bigcap",
            "\\bigotimes", "\\bigwedge", "\\coprod", "\\iint", "\\bigcup", "\\bigodot", "\\bigsqcup"};
        String[] Standard_Function_Names1 = {"\\arccos", "\\arcsin", "\\arctan", "\\arg", "\\cos", "\\cosh", "\\cot", "\\coth", "\\csc", "\\deg", "\\det", "\\dim", "\\exp", "\\gcd", "\\hom", "\\inf", "\\ker",
            "\\lg", "\\lim", "\\liminf", "\\limsup", "\\ln", "\\log", "\\max", "\\min", "\\Pr", "\\sec", "\\sin", "\\sinh", "\\sup", "\\tan", "\\tanh"};
        String[] Binary_Operation_Relation_Symbols1 = {"\\ast", "\\pm", "\\star", "\\mp", "\\cdot", "\\amalg", "\\circ", "\\odot", "\\bullet", "\\ominus", "\\bigcirc", "\\oplus", "\\diamond", "\\oslash",
            "\\times", "\\otimes", "\\div", "\\wr", "\\centerdot", "\\Box", "\\circledast", "\\boxplus", "\\circledcirc", "\\boxminus", "\\circleddash",
            "\\boxtimes", "\\dotplus", "\\boxdot", "\\divideontimes", "\\square", "\\cap", "\\cup", "\\uplus", "\\sqcap", "\\sqcup", "\\wedge", "\\vee",
            "\\dagger", "\\ddagger", "\\barwedge", "\\curlywedge", "\\Cap", "\\bot", "\\intercal", "\\lhd", "\\rhd", "\\triangleleft",
            "\\triangleright", "\\unlhd", "\\unrhd", "\\bigtriangledown", "\\bigtriangleup", "\\setminus", "\\veebar", "\\curlyvee", "\\Cup", "\\top",
            "\\rightthreetimes", "\\leftthreetimes", "\\equiv", "\\cong", "\\neq", "\\sim", "\\simeq", "\\approx", "\\asymp", "\\doteq", "\\propto",
            "\\models", "\\leq", "\\prec", "\\preceq", "\\ll", "\\subset", "\\subseteq", "\\sqsubset", "\\sqsubseteq", "\\dashv", "\\in", "\\geq", "\\succ",
            "\\succeq", "\\gg", "\\supset", "\\supseteq", "\\sqsupseteq", "\\sqsupset", "\\vdash", "\\ni", "\\perp", "\\mid", "\\parallel", "\\bowtie",
            "\\ltimes", "\\rtimes", "\\smile", "\\frown", "\\notin", "\\thicksim", "\\backsim", "\\backsimeq", "\\triangleq",
            "\\circeq", "\\bumpeq", "\\Bumpeq", "\\doteqdot", "\\thickapprox", "\\fallingdotseq", "\\risingdotseq", "\\varpropto", "\\therefore",
            "\\because", "\\eqcirc", "\\neq", "\\leqq", "\\leqslant", "\\lessapprox", "\\lll", "\\lessdot", "\\lesssim", "\\eqslantless", "\\precsim",
            "\\precapprox", "\\Subset", "\\subseteqq", "\\preccurlyeq", "\\curlyeqprec", "\\blacktriangleleft", "\\trianglelefteq",
            "\\vartriangleleft", "\\geqq", "\\gtrapprox", "\\ggg", "\\gtrdot", "\\gtrsim", "\\eqslantgtr", "\\succsim", "\\succapprox",
            "\\Supset", "\\supseteqq", "\\sqsupset", "\\succcurlyeq", "\\curlyeqsucc", "\\blacktriangleright", "\\trianglerighteq", "\\vartriangleright",
            "\\lessgtr", "\\lesseqqgtr", "\\lesseqgtr", "\\gtreqqless", "\\gtreqless", "\\gtrless", "\\backepsilon", "\\between", "\\pitchfork",
            "\\shortmid", "\\smallfrown", "\\smallsmile", "\\Vdash", "\\vDash", "\\Vvdash", "\\shortparallel", "\\nshortparallel", "\\ncong", "\\nmid",
            "\\nparallel", "\\nshortparallel", "\\nshortmid", "\\nsim", "\\nVDash", "\\nvdash", "\\ntriangleleft", "\\ntrianglelefteq", "\\ntriangleright",
            "\\ntrianglerighteq", "\\nleq", "\\nleqq", "\\nleqslant", "\\nless", "\\nprec", "\\npreceq", "\\precnapprox", "\\lnapprox", "\\precnsim",
            "\\lvertneqq", "\\lneq", "\\lneqq", "\\lnsim", "\\ngeq", "\\ngeqq", "\\ngeqslant", "\\succnapprox", "\\ngtr", "\\nsucc", "\\nsucceq",
            "\\succnsim", "\\gnapprox", "\\gvertneqq", "\\gneq", "\\gneqq", "\\gnsim", "\\nsubseteq", "\\nsupseteq", "\\nsubseteqq", "\\nsupseteqq",
            "\\subsetneq", "\\supsetneq", "\\subsetneqq", "\\supsetneqq", "\\varsubsetneq", "\\varsupsetneq", "\\varsubsetneqq", "\\varsupsetneqq"};
        String[] Arrow_symbols1 = {"\\leftarrow", "\\longleftarrow", "\\uparrow", "\\Leftarrow", "\\Longleftarrow", "\\Uparrow", "\\rightarrow", "\\longrightarrow",
            "\\downarrow", "\\Rightarrow", "\\Longrightarrow", "\\Downarrow", "\\leftrightarrow", "\\longleftrightarrow", "\\updownarrow",
            "\\Leftrightarrow", "\\Longleftrightarrow", "\\Updownarrow", "\\mapsto", "\\longmapsto", "\\nearrow", "\\hookleftarrow", "\\hookrightarrow",
            "\\searrow", "\\leftharpoonup", "\\rightharpoonup", "\\swarrow", "\\leftharpoondown", "\\rightharpoondown", "\\nwarrow", "\\rightleftharpoons",
            "\\leadsto", "\\leftleftarrows", "\\leftrightarrows", "\\Lleftarrow", "\\twoheadleftarrow",
            "\\leftarrowtail", "\\looparrowleft", "\\leftrightharpoons", "\\curvearrowleft", "\\circlearrowleft", "\\Lsh", "\\upuparrows",
            "\\upharpoonleft", "\\downharpoonleft", "\\multimap", "\\leftrightsquigarrow", "\\rightrightarrows", "\\rightleftarrows",
            "\\rightrightarrows", "\\rightleftarrows", "\\twoheadrightarrow", "\\rightarrowtail", "\\looparrowright", "\\rightleftharpoons",
            "\\curvearrowright", "\\Rsh", "\\downdownarrows", "\\upharpoonright", "\\downharpoonright", "\\rightsquigarrow",
            "\\nleftarrow", "\\nrightarrow", "\\nLeftarrow", "\\nRightarrow", "\\nleftrightarrow", "\\nLeftrightarrow"};
        String[] Miscellaneous_symbols1 = {"\\infty", "\\forall", "\\Bbbk", "\\wp", "\\nabla", "\\exists", "\\bigstar", "\\angle", "\\partial", "\\nexists", "\\diagdown", "\\Finv",
            "\\measuredangle", "\\eth", "\\emptyset", "\\diagup", "\\sphericalangle", "\\clubsuit", "\\varnothing", "\\Diamond", "\\complement",
            "\\diamondsuit", "\\imath", "\\triangledown", "\\heartsuit", "\\jmath", "\\Game", "\\triangle", "\\spadesuit", "\\ell", "\\hbar",
            "\\cdots", "\\iiiint", "\\hslash", "\\blacklozenge", "\\vdots", "\\iiint", "\\lozenge", "\\blacksquare", "\\ldots", "\\iint",
            "\\mho", "\\blacktriangle", "\\ddots", "\\sharp", "\\prime", "\\Im", "\\flat", "\\square", "\\backprime", "\\Re",
            "\\natural", "\\surd", "\\circledS"};
        String[] Math_mode_accents1 = {"\\acute{a}", "\\bar{a}", "\\breve{a}", "\\check{a}", "\\ddot{a}", "\\dot{a}", "\\grave{a}", "\\hat{a}", "\\tilde{a}", "\\vec{a}"};
//        String[] Array_environment1 = {"\\left( \\begin{array}{cc} 2\\tau & 7\\phi-frac5{12} \\\\ 3\\psi & \\frac{\\pi}8 \\end{array} \\right) \\left( \\begin{array}{c} x \\\\ y \\end{array} \\right) \\mbox{~and~} \\left[ \\begin{array}{cc|r} 3 & 4 & 5 \\\\ 1 & 3 & 729 \\end{array} \\right]",
//            "f(z) = \\left\\{ \\begin{array}{rcl} \\overline{\\overline{z^2}+\\cos z} & \\mbox{for} & |z|<3 \\\\ 0 & \\mbox{for} & 3\\leq|z|\\leq5 \\\\ \\sin\\overline{z} & \\mbox{for} & |z|>5 \\end{array}\\right"};
        String[] Text_Mode_Accents_and_Symbols1 = {"\\^{o}", "\\~{o}", "\\={o}", "\\d s", "\\.{o}", "\\u{o}", "\\H{o}", "\\t{oo}", "\\c{o}", "\\d{o}", "\\r s", "\\b{o}", "\\copyright", "\\AA", "\\aa",
            "\\ss", "\\i", "\\j", "\\H s", "\\o", "\\t s", "\\v s", "\\O", "\\ae", "\\AE", "\\dag", "\\ddag"};
        formula = new TeXFormula("\\Longleftarrow");
        icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
        icon.setInsets(new Insets(0, 0, 0, 0));
        image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        g2 = image.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
        jl = new JLabel();
        jl.setForeground(new Color(0, 0, 0));
        icon.paintIcon(jl, g2, 0, 0);
        btnBackSpace.setIcon(icon);
        setbuttoncommand(Greek_and_Hebrew_letters1, Greek_and_Hebrew_letters, 1);
        setbuttoncommand(LATEX_math_constructs1, LATEX_math_constructs, 2);
        setbuttoncommand(Delimiters1, Delimiters, 3);
        setbuttoncommand(Variable_sized_symbols1, Variable_sized_symbols, 4);
        setbuttoncommand(Standard_Function_Names1, Standard_Function_Names, 5);
        setbuttoncommand(Binary_Operation_Relation_Symbols1, Binary_Operation_Relation_Symbols, 6);
        setbuttoncommand(Arrow_symbols1, Arrow_symbols, 7);
        setbuttoncommand(Miscellaneous_symbols1, Miscellaneous_symbols, 8);
        setbuttoncommand(Math_mode_accents1, Math_mode_accents, 9);
//        setbuttoncommand(Array_environment1, Array_environment, 3);
        setbuttoncommand(Text_Mode_Accents_and_Symbols1, Text_Mode_Accents_and_Symbols, 10);
        setLocationRelativeTo(null);
    }

    void setbuttoncommand(String[] symbols, JPanel pnlAllSymbols, int id) {
        int DIV = 10;
        if (id == 6 || id == 7 || id == 8) {
            DIV = 15;
        }
        GridBagLayout layout = new GridBagLayout();
        pnlAllSymbols.setLayout(layout);
        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 1;
        cons.gridy = 1;
        cons.ipadx = 20;
        cons.ipady = 10;
        cons.anchor = GridBagConstraints.ABOVE_BASELINE;
        for (int i = 0; i < symbols.length; i++) {
            final String symbolName = symbols[i];
            final JButton btn = new JButton();
            if (i % DIV == 0) {
                cons.gridy++;
                cons.gridx = 1;
            }
            layout.setConstraints(pnlAllSymbols, cons);
            Dimension d = new Dimension(40, 40);
            if (id == 1 || id == 3 || id == 6 || id == 7 || id == 9 || id == 10) {
                d = new Dimension(30, 30);
            } else if (id == 2 || id == 4 || id == 8) {
                d = new Dimension(40, 40);
            } else if (id == 5) {
                d = new Dimension(55, 40);
            }
            btn.setSize(d);
            btn.setPreferredSize(d);
            pnlAllSymbols.add(btn, cons);
            cons.gridx++;
            setButtonIcon(btn, symbolName);
            btn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setEquation(btn, symbolName);
                    btnOKActionPerformed(e);
                }
            });
        }
    }

    void setButtonIcon(JButton b, String label) {
        formula = new TeXFormula(label);
        System.out.println(label);
        icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 18);
        icon.setInsets(new Insets(0, 0, 0, 0));
        image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        g2 = image.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
        jl = new JLabel();
        jl.setForeground(new Color(0, 0, 0));
        icon.paintIcon(jl, g2, 0, 0);
        b.setIcon(icon);
    }

    void setEquation(JButton b, String label) {
        label = "$ " + label + " $";
        lastStr.add(label);
        latex += label;
        txtEquation.setText(txtEquation.getText() + " " + label);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnOK = new javax.swing.JButton();
        btnBackSpace = new javax.swing.JButton();
        lblEquation = new javax.swing.JLabel();
        txtSymbol = new javax.swing.JTextField();
        btnAddSymbol = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnCopy = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox();
        jScrollPane10 = new javax.swing.JScrollPane();
        txtEquation = new javax.swing.JTextArea();
        JTabbedPane = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        Greek_and_Hebrew_letters = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        LATEX_math_constructs = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        Delimiters = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        Variable_sized_symbols = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        Standard_Function_Names = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        Binary_Operation_Relation_Symbols = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        Arrow_symbols = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        Miscellaneous_symbols = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        Math_mode_accents = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        Text_Mode_Accents_and_Symbols = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(102, 102, 102));

        btnOK.setText("OK");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });

        btnBackSpace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackSpaceActionPerformed(evt);
            }
        });

        btnAddSymbol.setText("Add Button");
        btnAddSymbol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSymbolActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Enter Symbol Text :");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Enter Equation :");

        btnCopy.setText("Copy");
        btnCopy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCopyActionPerformed(evt);
            }
        });

        jButton1.setText("MathType");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Greek & Hebrew Letters", "LATEX Math Constructs", "Delimiters", "Variable-Sized Symbols", "Standard Function Names", "Binary Operation/Relation Symbols", "Arrow Symbols", "Miscellaneous Symbols", "Math Mode Accents", "Text Mode" }));

        txtEquation.setColumns(20);
        txtEquation.setRows(5);
        jScrollPane10.setViewportView(txtEquation);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(txtSymbol, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblEquation, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(btnCopy, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(btnBackSpace)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnOK))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAddSymbol)
                        .addGap(0, 214, Short.MAX_VALUE))
                    .addComponent(jScrollPane10))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(txtSymbol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblEquation, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnBackSpace, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnOK))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton1))
                            .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCopy)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnAddSymbol))
                        .addGap(14, 14, 14)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        JTabbedPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        JTabbedPane.setAutoscrolls(true);

        jScrollPane1.setAutoscrolls(true);

        Greek_and_Hebrew_letters.setLayout(new java.awt.GridBagLayout());
        jScrollPane1.setViewportView(Greek_and_Hebrew_letters);

        JTabbedPane.addTab("Greek & Hebrew Letters", jScrollPane1);

        jScrollPane2.setAutoscrolls(true);

        javax.swing.GroupLayout LATEX_math_constructsLayout = new javax.swing.GroupLayout(LATEX_math_constructs);
        LATEX_math_constructs.setLayout(LATEX_math_constructsLayout);
        LATEX_math_constructsLayout.setHorizontalGroup(
            LATEX_math_constructsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        LATEX_math_constructsLayout.setVerticalGroup(
            LATEX_math_constructsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(LATEX_math_constructs);

        JTabbedPane.addTab("LATEX Math Constructs", jScrollPane2);

        jScrollPane3.setAutoscrolls(true);

        javax.swing.GroupLayout DelimitersLayout = new javax.swing.GroupLayout(Delimiters);
        Delimiters.setLayout(DelimitersLayout);
        DelimitersLayout.setHorizontalGroup(
            DelimitersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        DelimitersLayout.setVerticalGroup(
            DelimitersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(Delimiters);

        JTabbedPane.addTab("Delimiters", jScrollPane3);

        jScrollPane4.setAutoscrolls(true);

        javax.swing.GroupLayout Variable_sized_symbolsLayout = new javax.swing.GroupLayout(Variable_sized_symbols);
        Variable_sized_symbols.setLayout(Variable_sized_symbolsLayout);
        Variable_sized_symbolsLayout.setHorizontalGroup(
            Variable_sized_symbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        Variable_sized_symbolsLayout.setVerticalGroup(
            Variable_sized_symbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane4.setViewportView(Variable_sized_symbols);

        JTabbedPane.addTab("Variable-Sized Symbols", jScrollPane4);

        jScrollPane5.setAutoscrolls(true);

        javax.swing.GroupLayout Standard_Function_NamesLayout = new javax.swing.GroupLayout(Standard_Function_Names);
        Standard_Function_Names.setLayout(Standard_Function_NamesLayout);
        Standard_Function_NamesLayout.setHorizontalGroup(
            Standard_Function_NamesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        Standard_Function_NamesLayout.setVerticalGroup(
            Standard_Function_NamesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane5.setViewportView(Standard_Function_Names);

        JTabbedPane.addTab("Standard Function Names", jScrollPane5);

        jScrollPane6.setAutoscrolls(true);

        javax.swing.GroupLayout Binary_Operation_Relation_SymbolsLayout = new javax.swing.GroupLayout(Binary_Operation_Relation_Symbols);
        Binary_Operation_Relation_Symbols.setLayout(Binary_Operation_Relation_SymbolsLayout);
        Binary_Operation_Relation_SymbolsLayout.setHorizontalGroup(
            Binary_Operation_Relation_SymbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        Binary_Operation_Relation_SymbolsLayout.setVerticalGroup(
            Binary_Operation_Relation_SymbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane6.setViewportView(Binary_Operation_Relation_Symbols);

        JTabbedPane.addTab("Binary Operation/Relation Symbols", jScrollPane6);

        jScrollPane7.setAutoscrolls(true);

        javax.swing.GroupLayout Arrow_symbolsLayout = new javax.swing.GroupLayout(Arrow_symbols);
        Arrow_symbols.setLayout(Arrow_symbolsLayout);
        Arrow_symbolsLayout.setHorizontalGroup(
            Arrow_symbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        Arrow_symbolsLayout.setVerticalGroup(
            Arrow_symbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane7.setViewportView(Arrow_symbols);

        JTabbedPane.addTab("Arrow Symbols", jScrollPane7);

        jScrollPane8.setAutoscrolls(true);

        javax.swing.GroupLayout Miscellaneous_symbolsLayout = new javax.swing.GroupLayout(Miscellaneous_symbols);
        Miscellaneous_symbols.setLayout(Miscellaneous_symbolsLayout);
        Miscellaneous_symbolsLayout.setHorizontalGroup(
            Miscellaneous_symbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        Miscellaneous_symbolsLayout.setVerticalGroup(
            Miscellaneous_symbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane8.setViewportView(Miscellaneous_symbols);

        JTabbedPane.addTab("Miscellaneous Symbols", jScrollPane8);

        jScrollPane9.setAutoscrolls(true);

        javax.swing.GroupLayout Math_mode_accentsLayout = new javax.swing.GroupLayout(Math_mode_accents);
        Math_mode_accents.setLayout(Math_mode_accentsLayout);
        Math_mode_accentsLayout.setHorizontalGroup(
            Math_mode_accentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        Math_mode_accentsLayout.setVerticalGroup(
            Math_mode_accentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane9.setViewportView(Math_mode_accents);

        JTabbedPane.addTab("Math Mode Accents", jScrollPane9);

        jScrollPane11.setAutoscrolls(true);

        javax.swing.GroupLayout Text_Mode_Accents_and_SymbolsLayout = new javax.swing.GroupLayout(Text_Mode_Accents_and_Symbols);
        Text_Mode_Accents_and_Symbols.setLayout(Text_Mode_Accents_and_SymbolsLayout);
        Text_Mode_Accents_and_SymbolsLayout.setHorizontalGroup(
            Text_Mode_Accents_and_SymbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 973, Short.MAX_VALUE)
        );
        Text_Mode_Accents_and_SymbolsLayout.setVerticalGroup(
            Text_Mode_Accents_and_SymbolsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 215, Short.MAX_VALUE)
        );

        jScrollPane11.setViewportView(Text_Mode_Accents_and_Symbols);

        JTabbedPane.addTab("Text Mode", jScrollPane11);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addComponent(JTabbedPane)
                    .addGap(11, 11, 11)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(281, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(JTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addGap(172, 172, 172)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
    try {
        String finalStr = "";
        finalStr = start + txtEquation.getText() + end;
        TeXFormula formula = new TeXFormula(finalStr);

        TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 12, 1, Color.white);

        icon.setInsets(new Insets(5, 18, 5, 5));

        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = image.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
        JLabel jl = new JLabel();
//        jl.setForeground(new Color(0, 0, 0));
        jl.setForeground(Color.WHITE);
        icon.paintIcon(jl, g2, 0, 0);
        lblEquation.setIcon(icon);
    } catch (Exception e) {
        JOptionPane.showMessageDialog(rootPane, e.getMessage());
    }
}//GEN-LAST:event_btnOKActionPerformed

private void btnBackSpaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackSpaceActionPerformed
    if (latex.length() > 1) {
        int pos = lastStr.size();
        lastchar = lastStr.get(pos - 1);
        int pos1 = latex.lastIndexOf(lastchar);
        latex = latex.substring(0, pos1);
        lastStr.remove(pos - 1);
        txtEquation.setText(latex);
        btnOKActionPerformed(evt);
    }
}//GEN-LAST:event_btnBackSpaceActionPerformed

private void btnAddSymbolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSymbolActionPerformed
    final String symbolName = txtSymbol.getText();
    if (!symbolName.equals("")) {
        final JButton btn = new JButton();
        btn.setSize(10, 10);
        int index = jComboBox1.getSelectedIndex();
        if (index == 0) {
            Greek_and_Hebrew_letters.add(btn);
            Greek_and_Hebrew_letters.validate();
        } else if (index == 1) {
            LATEX_math_constructs.add(btn);
            LATEX_math_constructs.validate();
        } else if (index == 2) {
            Delimiters.add(btn);
            Delimiters.validate();
        } else if (index == 3) {
            Variable_sized_symbols.add(btn);
            Variable_sized_symbols.validate();
        } else if (index == 4) {
            Standard_Function_Names.add(btn);
            Standard_Function_Names.validate();
        } else if (index == 5) {
            Binary_Operation_Relation_Symbols.add(btn);
            Binary_Operation_Relation_Symbols.validate();
        } else if (index == 6) {
            Arrow_symbols.add(btn);
            Arrow_symbols.validate();
        } else if (index == 7) {
            Miscellaneous_symbols.add(btn);
            Miscellaneous_symbols.validate();
        } else if (index == 8) {
            Math_mode_accents.add(btn);
            Math_mode_accents.validate();
        } else if (index == 9) {
            Text_Mode_Accents_and_Symbols.add(btn);
            Text_Mode_Accents_and_Symbols.validate();
        }
        this.repaint();
        setButtonIcon(btn, symbolName);
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setEquation(btn, symbolName);
                btnOKActionPerformed(e);
            }
        });
    }
}//GEN-LAST:event_btnAddSymbolActionPerformed

private void btnCopyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCopyActionPerformed
    txtEquation.selectAll();
    txtEquation.copy();
}//GEN-LAST:event_btnCopyActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
boolean is64bit = false;
        if (System.getProperty("os.name").contains("Windows")) {
            is64bit = (System.getenv("ProgramFiles(x86)") != null);
        } else {
            is64bit = (System.getProperty("os.arch").indexOf("64") != -1);
        }
        
        ProcessBuilder pb = new ProcessBuilder(
                "C:/Program Files/MathType/MathType.exe",
                "x",
                "myjar.jar",
                "*.*",
                "new");
        if(is64bit){
            pb = new ProcessBuilder(
                "C:/Program Files (x86)/MathType/MathType.exe",
                "x",
                "myjar.jar",
                "*.*",
                "new");
        }
        pb.directory(new File("C:/"));
        pb.redirectError();
        try {
            Process p = pb.start();
        } catch (IOException ex) {
            Logger.getLogger(FormulaFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional)">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormulaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormulaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormulaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormulaFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormulaFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Arrow_symbols;
    private javax.swing.JPanel Binary_Operation_Relation_Symbols;
    private javax.swing.JPanel Delimiters;
    private javax.swing.JPanel Greek_and_Hebrew_letters;
    private javax.swing.JTabbedPane JTabbedPane;
    private javax.swing.JPanel LATEX_math_constructs;
    private javax.swing.JPanel Math_mode_accents;
    private javax.swing.JPanel Miscellaneous_symbols;
    private javax.swing.JPanel Standard_Function_Names;
    private javax.swing.JPanel Text_Mode_Accents_and_Symbols;
    private javax.swing.JPanel Variable_sized_symbols;
    private javax.swing.JButton btnAddSymbol;
    private javax.swing.JButton btnBackSpace;
    private javax.swing.JButton btnCopy;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblEquation;
    private javax.swing.JTextArea txtEquation;
    private javax.swing.JTextField txtSymbol;
    // End of variables declaration//GEN-END:variables
}