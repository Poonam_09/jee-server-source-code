/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;

import com.bean.NewAndOldBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Aniket
 */
public class NewAndOldOperations {
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    
    public ArrayList<NewAndOldBean> getNewAndOldList(Connection conn) {
        ArrayList<NewAndOldBean> returnList = null;
        NewAndOldBean newAndOldBean = null;
        try {
            String query = "SELECT * FROM NEWANDOLDQUESTION";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()) {
                newAndOldBean = new NewAndOldBean();
                newAndOldBean.setQuestionId(rs.getInt(1));
                newAndOldBean.setQuestionType(rs.getInt(2));

                if(returnList == null)
                    returnList = new ArrayList<NewAndOldBean>();

                returnList.add(newAndOldBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }
        
        return returnList;
    }
    
    public ArrayList<NewAndOldBean> getMasterNewAndOldList(Connection conn) {
        ArrayList<NewAndOldBean> returnList = null;
        NewAndOldBean newAndOldBean = null;
        try {
            String query = "SELECT * FROM MASTER_NEWANDOLDQUESTION";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()) {
                newAndOldBean = new NewAndOldBean();
                newAndOldBean.setQuestionId(rs.getInt(1));
                newAndOldBean.setQuestionType(rs.getInt(2));

                if(returnList == null)
                    returnList = new ArrayList<NewAndOldBean>();

                returnList.add(newAndOldBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }
        
        return returnList;
    }
    
    public ArrayList<NewAndOldBean> getNewAndOldPaperList(Connection conn) {
        ArrayList<NewAndOldBean> returnList = null;
        NewAndOldBean newAndOldBean = null;
        try {
            String query = "SELECT * FROM NEWANDOLDQUESTIONFORPAPER";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()) {
                newAndOldBean = new NewAndOldBean();
                newAndOldBean.setQuestionId(rs.getInt(1));
                newAndOldBean.setQuestionType(rs.getInt(2));

                if(returnList == null)
                    returnList = new ArrayList<NewAndOldBean>();

                returnList.add(newAndOldBean);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnList = null;
        }
        
        return returnList;
    }
}
